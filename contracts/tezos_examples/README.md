<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Tezos example contracts

The contracts in this directory are taken from the [tezos repository][tz:contracts].

Note: we modify some contracts; check the list below.

## Update instructions

These contracts are occasionally updated in the [tezos repository][tz:contracts] and we synchronize them from time to time.
Here are some things you should keep in mind:
1. Update the link to the contracts in this file.
2. List of our modifications that should persist unless they're obviated:
  + Whitespace is regularized according to CI
  + `ticket_wallet_fungible.tz` contains a workaround and a to-do
  + `view_op_toplevel_inconsistent_input_type.tz`: we modify the input type to nat, for the sake of uniformity
  + Several contracts testing sapling transactions have a note about typechecking but not implementing sapling functionality:
    + `ill_typed/pack_sapling_state.tz`
    + `ill_typed/sapling_build_empty_state_with_int_parameter.tz`
    + `ill_typed/unpack_sapling_state.tz`
    + `opcodes/sapling_empty_state.tz`
3. Carefully check the [Contracts.hs](/code/tests/test-common/Test/Util/Contracts.hs) module:
  + `wellTypedContractDirs` should list all folders with well-typed contracts; if folder structure of the [tezos repository][tz:contracts] changes, update the list there.
  + We have to distinguish ill-typed and unparseable contracts, and our parser is not the same as in Tezos, so there are ill-typed contracts that are not parseable by our code. `illTypedContractDirs` contains a list of directories with parseable ill-typed contracts; `unparsableContracts`, a list of unparseable contracts outside of `/contracts/unparsable` directory; `unsupportedContracts`, a list of unsupported contracts from the paragraph (3) above.
  + If you observe new `/ill-typed` of `/legacy` contracts that `morley` does not parse, feel free to simply add them to the `unparsableContracts` list. If some reference `/ill-typed` or `/legacy` contracts do typecheck by `morley`, or some well-typed reference contracts fail typechecking, the easiest workaround is putting it into `unsupportedContracts`, but it may warrant a fix to `morley` or a separate issue; please also document it here.

## License

[MIT][tz:license] © 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>

[tz:contracts]: https://gitlab.com/tezos/tezos/-/tree/cb9f439e58c761e76ade589d1cdbd2abb737dc68/tests_python/contracts_013
[tz:license]: ../../LICENSES/LicenseRef-MIT-Tezos.txt
