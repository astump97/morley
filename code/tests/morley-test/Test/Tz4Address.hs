-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for tz4 addresses
module Test.Tz4Address
  ( test_tz4keyhash
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Typed
import Morley.Tezos.Address
import Morley.Tezos.Crypto
import Test.Cleveland

test_tz4keyhash :: TestTree
test_tz4keyhash = testScenario "tz4 address is passed properly" $ scenario do
  handle <- originate "tz4_key_hash" Nothing $ storeContract @TxRollupL2Address
  let addr = TxRollupL2Address $ unsafe $ parseHash "tz4LVHYD4P4T5NHCuwJbxQvwVURF62seE3Qa"
  transfer handle $ calling def addr
  getStorage handle @@== Just addr

storeContract :: StorageScope (ToT s) => TypedContract s (Maybe s) ()
storeContract = TypedContract $ defaultContract $ CAR :# SOME :# NIL :# PAIR
