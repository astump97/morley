-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Tezos.Crypto.Secp256k1
  ( test_secp
  ) where

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Morley.Client hiding (revealKeyUnlessRevealed)
import Morley.Client.Action.Reveal
import Morley.Client.Init (mkLogAction)
import Morley.Client.TezosClient.Impl (importKey)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Tezos.Address
import Morley.Tezos.Core
import Morley.Tezos.Crypto
import Morley.Tezos.Crypto.Secp256k1 (detSecretKeyDo)
import Test.Cleveland (neMorleyClientEnv)
import Test.Cleveland.Internal.Abstract (Moneybag(Moneybag))
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Tasty

test_secp :: TestTree
test_secp =
  whenNetworkEnabled $ \withEnv ->
    testCase "Network accepts our Secp256k1 signatures (#849 regression test)" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env
      sk <- SecretKeySecp256k1 <$> detSecretKeyDo
      let pub = toPublic sk
      let addr = mkKeyAddress pub

      void $ runMorleyClientM (neMorleyClientEnv env) { mceLogAction = mkLogAction 1 } do
        importKey True "some-alias" sk
        transfer moneybag addr [tz|0.9|] U.DefEpName T.VUnit Nothing
        void . revealKeyUnlessRevealed addr $ RevealData pub Nothing
        -- in #849 roughly half of all signatures were invalid, so we're doing several tests
        replicateM_ 5 do
          -- the amount is tuned to minimize stuck funds based on current fees;
          -- may need adjusting if fees change.
          void $ transfer addr moneybag [tz|179 milli|] U.DefEpName T.VUnit Nothing
