-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Util.TH
  ( shouldCompileTo
  , shouldCompileIgnoringInstance
  ) where

import Prelude hiding (Type)

import Data.Generics qualified as SYB
import Fmt (Buildable(..), pretty)
import Language.Haskell.TH (pprint)
import Language.Haskell.TH.Syntax
  (Dec(..), Name, Q, Specificity, TyVarBndr(..), Type(..), mkName, nameBase, runQ)
import Test.Tasty.HUnit (Assertion, assertFailure)

import Morley.Michelson.Printer.Util
import Morley.Util.MismatchError

----------------------------------------------------------------------------
-- TemplateHaskell test helpers
----------------------------------------------------------------------------

compareDecs :: HasCallStack => [Dec] -> [Dec] -> Assertion
compareDecs (normalizeDecs -> actualDecs) (normalizeDecs -> expectedDecs) =
  when (actualDecs /= expectedDecs) $
    assertFailure $ printDocS False $ renderDocDiff doesntNeedParens MkMismatchError
      { meExpected = PrettyDecs expectedDecs
      , meActual = PrettyDecs actualDecs
      }

shouldCompileTo :: HasCallStack => [Dec] -> Q [Dec] -> Assertion
shouldCompileTo actualDecs expectedQ = do
  expectedDecs <- runQ expectedQ
  compareDecs actualDecs expectedDecs

-- | Same as 'shouldCompileTo', but ignores instance declarations of the given class.
shouldCompileIgnoringInstance :: HasCallStack => Name -> [Dec] -> Q [Dec] -> Assertion
shouldCompileIgnoringInstance className actualDecs expectedQ = do
  expectedDecs <- runQ expectedQ
  let actualDecs' = filter (not . isInstance) actualDecs
  compareDecs actualDecs' expectedDecs

  where
    isInstance :: Dec -> Bool
    isInstance = \case
      InstanceD _ _ (ConT t `AppT` _) _ | t == className -> True
      _ -> False

-- | Normalize ASTs to make them comparable.
--
-- By default, quoted ASTs and ASTs with names created using 'newName' will have
-- names with unique IDs.
-- For example:
--
-- > decs <- runQ [d|data D = D { f :: Int } |]
-- > putStrLn $ pprint decs
-- >
-- > -- Will generate this AST:
-- > data D_0 = D_1 { f_2 :: Int }
--
-- To be able to check if two ASTs are equivalent, we have to scrub the unique IDs off all names.
--
-- For convenience, to make the output easier to read, we also erase kind annotations when the kind is '*'.
normalizeDecs :: [Dec] -> [Dec]
normalizeDecs decs =
    SYB.everywhere
      ( SYB.mkT fixName
      . SYB.mkT simplifyType
      . SYB.mkT (simplifyTyVar @())
      . SYB.mkT (simplifyTyVar @Specificity)
      )
      decs
  where
    fixName :: Name -> Name
    fixName = mkName . nameBase

    simplifyType :: Type -> Type
    simplifyType = \case
      SigT t StarT -> t
      t -> t

    -- NB: `flag` is supposed to be either `Specificity` or `()`¹;
    -- if it is neither, such a TyVarBndr will not get simplified.
    -- ¹ https://gitlab.haskell.org/ghc/ghc/-/wikis/migration/9.0#template-haskell-217
    simplifyTyVar :: TyVarBndr flag -> TyVarBndr flag
    simplifyTyVar = \case
     KindedTV name flag StarT -> PlainTV name flag
     tv -> tv

newtype PrettyDecs = PrettyDecs [Dec]

instance Buildable PrettyDecs where
  build (PrettyDecs dec) = pretty $ pprint dec

instance RenderDoc PrettyDecs where
  renderDoc _ = renderAnyBuildable
  isRenderable _ = True
