-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for txr1 addresses
module Test.Txr1Address
  ( test_txr1keyhash
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Typed
import Morley.Tezos.Address
import Test.Cleveland

test_txr1keyhash :: TestTree
test_txr1keyhash = testScenario "txr1 address is passed properly" $ scenario do
  handle <- originate "txr1_key_hash" Nothing $ storeContract @Address
  let addr = toAddress [ta|txr1juGAfkUFU3Y3oEPfsBnruQxXaHgvsCnAB|]
  transfer handle $ calling def addr
  getStorage handle @@== Just addr

storeContract :: StorageScope (ToT s) => TypedContract s (Maybe s) ()
storeContract = TypedContract $ defaultContract $ CAR :# SOME :# NIL :# PAIR
