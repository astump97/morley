-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests of `Show` and `RenderDoc` instances of Michelson types.
module Test.Michelson.Show
  ( unit_AnnTagsShow
  , unit_AnnTagsRenderDoc
  , unit_AnnotationSetShow
  , unit_AnnotationSetRenderDoc
  , unit_NotesShow
  , unit_NotesRenderDoc
  , unit_InstrAbstractShow
  , unit_InstrAbstractRenderDoc
  , unit_HSTShow
  , unit_HSTRenderDoc
  , unit_SomeInstrOutShow
  , unit_SomeInstrOutPretty
  , unit_SomeInstrShow
  , unit_SomeInstrPretty
  , unit_TcErrorShow
  , unit_TcErrorRenderDoc
  , unit_ShowWithToplevel
  , unit_ShowWithSubexpression
  , unit_SomeParamTypeToplevel
  , unit_SomeParamTypeSubexpression
  , unit_StackRefToplevel
  , unit_StackRefSubexpression
  , unit_LabelToplevel
  , unit_LabelSubexpression
  , unit_WeirdLabel
  , unit_NamedFToplevel
  , unit_NamedFSubexpression
  , unit_WeirdNamedF
  , unit_NamedPrecedenceInfix9
  , unit_NamedPrecedenceJust
  ) where

import Debug qualified (show)

import Data.Constraint (Dict(..))
import Fmt (pretty)
import Test.HUnit (Assertion, assertEqual)

import Morley.Michelson.Printer.Util (RenderDoc(..), doesntNeedParens, printDoc)
import Morley.Michelson.TypeCheck
  (HST(..), SomeParamType(..), SomeTcInstr(..), SomeTcInstrOut(..), TcError, TcError'(..))
import Morley.Michelson.Typed (Notes(..), ParamNotes(..), T(..))
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Instr (Instr(..), StackRef(..))
import Morley.Michelson.Untyped qualified as U
import Morley.Util.Label (Label(..))
import Morley.Util.Named (NamedF, pattern (:!))
import Morley.Util.PeanoNatural (toPeanoNatural')
import Test.Cleveland.Util (ShowWith(..))

render :: RenderDoc a => a -> LText
render = printDoc True . renderDoc doesntNeedParens

-- * Annotation

typeAnn :: U.TypeAnn
typeAnn = U.UnsafeAnnotation @U.TypeTag "type"

fieldAnn :: U.FieldAnn
fieldAnn = U.UnsafeAnnotation @U.FieldTag "field"

varAnn :: U.VarAnn
varAnn = U.UnsafeAnnotation @U.VarTag "var"

unit_AnnTagsShow :: Assertion
unit_AnnTagsShow =
  zipWithM_ (assertEqual @Text "Annotations' `show` produces type tags")
    [ "UnsafeAnnotation @TypeTag \"type\""
    , "UnsafeAnnotation @FieldTag \"field\""
    , "UnsafeAnnotation @VarTag \"var\""
    ]
    [Debug.show typeAnn, Debug.show fieldAnn, Debug.show varAnn]

unit_AnnTagsRenderDoc :: Assertion
unit_AnnTagsRenderDoc =
  zipWithM_ (assertEqual "Annotations' `renderDoc` produces Michelson annotations")
    [":type", "%field", "@var"]
    [render typeAnn, render fieldAnn, render varAnn]

-- * AnnotationSet

annSet :: U.AnnotationSet
annSet = U.AnnotationSet [typeAnn] [fieldAnn] [varAnn]

unit_AnnotationSetShow :: Assertion
unit_AnnotationSetShow =
  assertEqual @Text "AnnotationSet's `show` is stock"
    "AnnotationSet {asTypes = [UnsafeAnnotation @TypeTag \"type\"], asFields = [UnsafeAnnotation @FieldTag \"field\"], asVars = [UnsafeAnnotation @VarTag \"var\"]}"
    (Debug.show annSet)

unit_AnnotationSetRenderDoc :: Assertion
unit_AnnotationSetRenderDoc =
  assertEqual "AnnotationSet's `renderDoc` produces a list of annotations"
    ":type %field @var"
    (render annSet)

-- * Notes

pair :: Notes ('TPair 'TUnit 'TUnit)
pair = NTPair typeAnn fieldAnn U.noAnn varAnn U.noAnn (NTUnit U.noAnn) (NTUnit U.noAnn)

unit_NotesShow :: Assertion
unit_NotesShow =
  assertEqual @Text "Notes' `show` is stock"
    "NTPair (UnsafeAnnotation @TypeTag \"type\") (UnsafeAnnotation @FieldTag \"field\") (UnsafeAnnotation @FieldTag \"\") (UnsafeAnnotation @VarTag \"var\") (UnsafeAnnotation @VarTag \"\") (NTUnit (UnsafeAnnotation @TypeTag \"\")) (NTUnit (UnsafeAnnotation @TypeTag \"\"))"
    (Debug.show pair)

unit_NotesRenderDoc :: Assertion
unit_NotesRenderDoc =
  assertEqual "Notes' `renderDoc` produces a Michelson annotated type"
    "pair :type (unit %field @var) unit"
    (render pair)

-- * InstrAbstract

instr :: U.InstrAbstract U.ExpandedOp
instr = U.PAIR typeAnn varAnn fieldAnn U.noAnn

unit_InstrAbstractShow :: Assertion
unit_InstrAbstractShow =
  assertEqual @Text "InstrAbstract's `show` is stock"
    "PAIR (UnsafeAnnotation @TypeTag \"type\") (UnsafeAnnotation @VarTag \"var\") (UnsafeAnnotation @FieldTag \"field\") (UnsafeAnnotation @FieldTag \"\")"
    (Debug.show instr)

unit_InstrAbstractRenderDoc :: Assertion
unit_InstrAbstractRenderDoc =
  assertEqual "InstrAbstract's `renderDoc` produces an annotated Michelson instruction"
    "PAIR :type %field @var"
    (render instr)

-- * HST

hst :: HST '[ 'TUnit ]
hst = (T.STUnit, Dict) ::& SNil

unit_HSTShow :: Assertion
unit_HSTShow =
  assertEqual @Text "HST's `show` is lawful"
    "(STUnit,Dict) ::& SNil"
    (Debug.show hst)

unit_HSTRenderDoc :: Assertion
unit_HSTRenderDoc =
  assertEqual "HST's `renderDoc` is pretty"
    "[ unit ]"
    (render hst)

-- * SomeTcInstrOut

someInstrOut :: SomeTcInstrOut '[]
someInstrOut = UNIT ::: hst

unit_SomeInstrOutShow :: Assertion
unit_SomeInstrOutShow =
  assertEqual @Text "SomeTcInstrOut's `show` is lawful"
    ("AnnUNIT (AnnsCons (UnsafeAnnotation @TypeTag \"\") (AnnsCons (UnsafeAnnotation @VarTag \"\") AnnsNil)) ::: ("
      <> Debug.show hst <> ")")
    (Debug.show someInstrOut)

unit_SomeInstrOutPretty :: Assertion
unit_SomeInstrOutPretty =
  assertEqual @Text "SomeTcInstrOut's `pretty` is human-readable"
    "[UNIT] :: [ unit ]"
    (pretty someInstrOut)

-- * SomeTcInstr

someInstr :: SomeTcInstr '[]
someInstr = SNil :/ someInstrOut

unit_SomeInstrShow :: Assertion
unit_SomeInstrShow =
  assertEqual @Text "SomeTcInstr's `show` is lawful"
    ("SNil :/ " <> Debug.show someInstrOut)
    (Debug.show someInstr)

unit_SomeInstrPretty :: Assertion
unit_SomeInstrPretty =
  assertEqual @Text "SomeTcInstr's `pretty` is human-readable"
    "[] -> [UNIT] :: [ unit ]"
    (pretty someInstr)

-- * TcError

tcError :: TcError
tcError = TcContractError "Example error" Nothing

unit_TcErrorShow :: Assertion
unit_TcErrorShow =
  assertEqual @Text "TcError's `show` is lawful"
    "TcContractError \"Example error\" Nothing"
    (Debug.show tcError)

unit_TcErrorRenderDoc :: Assertion
unit_TcErrorRenderDoc =
  assertEqual "TcError's `renderDoc` is human-readable"
    "Error occurred during contract typecheck:Example error"
    (render tcError)

-- * ShowWith

showWith :: ShowWith (Maybe ())
showWith = ShowWith Debug.show $ Just ()

unit_ShowWithToplevel :: Assertion
unit_ShowWithToplevel =
  assertEqual @Text "Top-level `ShowWith` is `show`n without parens"
    "Just ()"
    (Debug.show showWith)

unit_ShowWithSubexpression :: Assertion
unit_ShowWithSubexpression =
  assertEqual @Text "Subexpression `ShowWith` is parenthesized"
    "Just (Just ())"
    (Debug.show (Just showWith))


-- * SomeParamType

someParamType :: SomeParamType
someParamType = SomeParamType $ UnsafeParamNotes (NTUnit typeAnn) fieldAnn

unit_SomeParamTypeToplevel :: Assertion
unit_SomeParamTypeToplevel =
  assertEqual @Text "Top-level `SomeParamType` is `show`n without parens"
    "SomeParamType (UnsafeParamNotes {pnNotes = NTUnit (UnsafeAnnotation @TypeTag \"type\"), pnRootAnn = UnsafeAnnotation @FieldTag \"field\"})"
    (Debug.show someParamType)

unit_SomeParamTypeSubexpression :: Assertion
unit_SomeParamTypeSubexpression =
  assertEqual @Text "Subexpression `SomeParamType` is parenthesized"
    ("Just (" <> Debug.show someParamType <> ")")
    (Debug.show (Just someParamType))

-- * StackRef

stackRef :: StackRef '[ 'TUnit ]
stackRef = StackRef (toPeanoNatural' @0)

unit_StackRefToplevel :: Assertion
unit_StackRefToplevel =
  assertEqual @Text "Top-level `StackRef` is `show`n without parens"
    "StackRef (toPeanoNatural' @0)"
    (Debug.show stackRef)

unit_StackRefSubexpression :: Assertion
unit_StackRefSubexpression =
  assertEqual @Text "Subexpression `StackRef` is parenthesized"
    "Just (StackRef (toPeanoNatural' @0))"
    (Debug.show (Just stackRef))

-- * Label

label :: Label "label"
label = Label @"label"

unit_LabelToplevel :: Assertion
unit_LabelToplevel =
  assertEqual @Text "Top-level `Label` is `show`n without parens"
    "Label @\"label\""
    (Debug.show label)

unit_LabelSubexpression :: Assertion
unit_LabelSubexpression =
  assertEqual @Text "Subexpression `Label` is parenthesized"
    "Just (Label @\"label\")"
    (Debug.show (Just label))

unit_WeirdLabel :: Assertion
unit_WeirdLabel =
  assertEqual @Text "`Label`s with double-quotes are `show`n correctly"
    "Label @\"\\\"\""
    (Debug.show $ Label @"\"")

-- * NamedF Identity

named :: NamedF Identity () "name"
named = fromLabel @"name" :! ()

unit_NamedFToplevel :: Assertion
unit_NamedFToplevel =
  assertEqual @Text "Top-level `NamedF Identity` is `show`n without parens"
    "fromLabel @\"name\" :! ()"
    (Debug.show named)

unit_NamedFSubexpression :: Assertion
unit_NamedFSubexpression =
  assertEqual @Text "Subexpression `NamedF Identity` is parenthesized"
    "Just (fromLabel @\"name\" :! ())"
    (Debug.show (Just named))

unit_WeirdNamedF :: Assertion
unit_WeirdNamedF =
  assertEqual @Text "`Name`s with double-quotes are `show`n correctly"
    "fromLabel @\"\\\"\" :! ()"
    (Debug.show $ fromLabel @"\"" :! ())

infix 9 :++:
data Infix9 = () :++: ()
  deriving stock Show

unit_NamedPrecedenceInfix9 :: Assertion
unit_NamedPrecedenceInfix9 =
  assertEqual @Text "Precedence-9 infix constructor in fixity-9 (:!) is properly wrapped"
    "fromLabel @\"name\" :! (() :++: ())"
    (Debug.show $ #name :! (() :++: ()))

unit_NamedPrecedenceJust :: Assertion
unit_NamedPrecedenceJust =
  assertEqual @Text "Precedence-10 application of `Just` is unwrapped, as (:!) is of fixity 9"
    "fromLabel @\"name\" :! Just ()"
    (Debug.show $ #name :! Just ())
