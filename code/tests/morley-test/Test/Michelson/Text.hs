-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on 'MText'.
module Test.Michelson.Text
  ( test_Roundtrip
  , unit_mkMText
  , unit_parse
  , unit_mkMTextCut
  , unit_QuasiQuoter
  , unit_mt
  ) where

import Data.Default (def)
import Fmt (Buildable, pretty)
import Test.HUnit (Assertion, (@?), (@?=))
import Test.Tasty (TestTree)
import Text.Megaparsec qualified as P

import Hedgehog.Gen.Michelson (genMText)
import Morley.Michelson.Parser
import Morley.Michelson.Text
import Morley.Michelson.Untyped qualified as U
import Morley.Util.Text (dquotes)
import Test.Cleveland.Instances ()

import Test.Util.Hedgehog (roundtripTree)

-- | Parse string literal content to 'MText'.
parseMTextTest :: HasCallStack => Text -> Either () MText
parseMTextTest t =
  first (const ()) $
  expectString <$> parseNoEnv (stringLiteral <* P.eof) codeSrc (dquotes t)
  where
    expectString = \case
      U.ValueString t' -> t'
      o -> error $ "Expected string, but got " <> pretty o

-- | 'MText' rountrip conversions.
test_Roundtrip :: [TestTree]
test_Roundtrip =
  [ roundtripTree (genMText def) writeMText parseMTextTest ]

-- | Check value against the given predicate.
(@??) :: (Buildable a, HasCallStack) => a -> (a -> Bool) -> Assertion
(@??) val predicate =
  predicate val @?
  ("Predicate does not hold for value " <> pretty val)

unit_mkMText :: Assertion
unit_mkMText = do
  mkMText ""
    @?= Right (UnsafeMText "")
  mkMText "a ba"
    @?= Right (UnsafeMText "a ba")
  mkMText "a\nb"
    @?= Right (UnsafeMText "a\nb")
  mkMText "a\\nb"
    @?= Right (UnsafeMText "a\\nb")
  mkMText "\\\\"
    @?= Right (UnsafeMText "\\\\")
  mkMText "\""
    @?= Right (UnsafeMText "\"")
  mkMText "\r"
    @?? isLeft
  mkMText "\t"
    @?? isLeft
  mkMText (toText @String [toEnum 5])
    @?? isLeft
  mkMText (toText @String [toEnum 127])
    @?? isLeft
  mkMText (toText @String [toEnum 300])
    @?? isLeft

unit_parse :: Assertion
unit_parse = do
  parseMTextTest ""
    @?= Right (UnsafeMText "")
  parseMTextTest "a ba"
    @?= Right (UnsafeMText "a ba")
  parseMTextTest "a\nb"
    @?? isLeft
  parseMTextTest "a\\nb"
    @?= Right (UnsafeMText "a\nb")
  parseMTextTest "\\"
    @?? isLeft
  parseMTextTest "\\\\"
    @?= Right (UnsafeMText "\\")
  parseMTextTest "\""
    @?? isLeft
  parseMTextTest "\\\""
    @?= Right (UnsafeMText "\"")
  parseMTextTest "\r"
    @?? isLeft
  parseMTextTest "\t"
    @?? isLeft
  parseMTextTest (toText @String [toEnum 5])
    @?? isLeft
  parseMTextTest (toText @String [toEnum 127])
    @?? isLeft
  parseMTextTest (toText @String [toEnum 300])
    @?? isLeft

unit_mkMTextCut :: Assertion
unit_mkMTextCut = do
  mkMTextCut ""
    @?= UnsafeMText ""
  mkMTextCut "a ba"
    @?= UnsafeMText "a ba"
  mkMTextCut "a\nb"
    @?= UnsafeMText "a\nb"
  mkMTextCut "a\\nb"
    @?= UnsafeMText "a\\nb"
  mkMTextCut "\\"
    @?= UnsafeMText "\\"
  mkMTextCut "\\\\"
    @?= UnsafeMText "\\\\"
  mkMTextCut "\""
    @?= UnsafeMText "\""
  mkMTextCut "\\\""
    @?= UnsafeMText "\\\""
  mkMTextCut "a\rb"
    @?= UnsafeMText "ab"
  mkMTextCut "c\td\r"
    @?= UnsafeMText "cd"
  mkMTextCut (toText @String [toEnum 5])
    @?= UnsafeMText ""
  mkMTextCut (toText @String [toEnum 127])
    @?= UnsafeMText ""
  mkMTextCut (toText @String [toEnum 300, 'A'])
    @?= UnsafeMText "A"

unit_QuasiQuoter :: Assertion
unit_QuasiQuoter = do
  qqMText ""
    @?= Right ""
  qqMText "a ba"
    @?= Right "a ba"
  qqMText "a\nb"
    @?? isLeft
  qqMText "a\\nb"
    @?= Right "a\nb"
  qqMText "\\"
    @?? isLeft
  qqMText "\\\\"
    @?= Right "\\"
  qqMText "\""
    @?= Right "\""
  qqMText "\\\""
    @?? isLeft
  qqMText "\r"
    @?? isLeft
  qqMText "\t"
    @?? isLeft
  qqMText [toEnum 5]
    @?? isLeft
  qqMText [toEnum 127]
    @?? isLeft
  qqMText [toEnum 300]
    @?? isLeft

unit_mt :: Assertion
unit_mt = do
  [mt|aba|]
    @?= UnsafeMText "aba"
  [mt| a  |]
    @?= UnsafeMText " a  "
