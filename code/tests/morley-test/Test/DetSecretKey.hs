-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for 'detSecretKey'
module Test.DetSecretKey
  ( test_detSecretKey
  , hprop_detSecretKey
  ) where

import Debug qualified (show)

import Data.ByteString qualified as BS
import Hedgehog (Property, forAll, property, (===))
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (assertFailure, testCase)

import Morley.Tezos.Crypto

test_detSecretKey :: TestTree
test_detSecretKey = testCase "detSecretKey produces different key types for different seeds" do
  case detSecretKey "\x00" of
    SecretKeyEd25519{} -> pass
    x -> assertFailure $ "Expected \\x00 to produce SecretKeyEd25519, but got " <> Debug.show x
  case detSecretKey "\x01" of
    SecretKeySecp256k1{} -> pass
    x -> assertFailure $ "Expected \\x01 to produce SecretKeySecp256k1, but got " <> Debug.show x
  case detSecretKey "\x02" of
    SecretKeyP256{} -> pass
    x -> assertFailure $ "Expected \\x02 to produce SecretKeyP256, but got " <> Debug.show x
  case detSecretKey "\x03" of
    SecretKeyEd25519{} -> pass
    x -> assertFailure $ "Expected \\x03 to produce SecretKeyEd25519, but got " <> Debug.show x

hprop_detSecretKey :: Property
hprop_detSecretKey = property $ do
  char <- forAll $ Gen.word8 Range.linearBounded
  let key = detSecretKey (BS.pack [char])
      seedMod = fromIntegral char `mod` 3
      -- the reason this is explicitly "3" and not "fromEnum maxBound + 1" is to
      -- avoid making a self-cancelling mistake.
  fromEnum (keyType key) === seedMod
  where
    keyType = \case
      SecretKeyEd25519{}   -> KeyTypeEd25519
      SecretKeySecp256k1{} -> KeyTypeSecp256k1
      SecretKeyP256{}      -> KeyTypeP256
      -- NB: when adding other key types, remember to change the second argument
      -- of `mod` above, too.
