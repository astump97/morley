-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Interpreter.Annotations
  ( test_cast_notes
  , test_complex_instructions_stacks
  , test_interpret_stack
  , test_update_stacks
  , test_create_contract_stacks
  , unit_PAIRN_and_UNPAIRN_stacks
  , unit_GETN_stack
  , unit_GET_0_stack
  , unit_UPDATEN_stack
  , unit_UPDATEN_stack_2
  , unit_GET_AND_UPDATE_stack
  , test_dupn
  , test_nested_stacks
  , unit_CARk_CDRk_interact_with_stacks
  , unit_CAR_special_stacks
  , unit_CDR_special_stacks
  , unit_UNPAIR_special_stacks
  , test_special_stacks
  ) where

import Debug qualified (show)
import Prelude hiding (Const)
import Unsafe qualified

import Control.Lens (ix, makeLensesFor, (<<%=))
import Control.Monad.RWS.Strict (RWST, runRWST)
import Data.List qualified as List
import Data.Map.Strict qualified as Map
import Data.Vinyl (Rec(..))
import Data.Vinyl.Functor (Const(..))
import Data.Vinyl.Recursive (recordToList, rmap)
import Fmt (Buildable(..), pretty)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (Assertion, assertFailure, testCase)

import Morley.Michelson.ErrorPos (ErrorSrcPos(..), srcPos)
import Morley.Michelson.Interpret
  (ContractEnv, InstrRunner, InterpreterState(..), InterpreterStateMonad(..),
  MichelsonFailureWithStack, MorleyLogsBuilder, StkEl(..), initInterpreterState, mkInitStack,
  runInstrImpl)
import Morley.Michelson.Runtime.Dummy
  (dummyBigMapCounter, dummyContractEnv, dummyGlobalCounter, dummySelf)
import Morley.Michelson.Text (MText)
import Morley.Michelson.Typed (Operation, Value'(..), toVal)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.ClassifiedInstr
import Morley.Tezos.Address
import Test.Cleveland.Instances ()
import Test.Cleveland.Michelson (importContract)

import Test.Util.Contracts (contractsDir, (</>))
import Test.Util.HUnit (assertEqualBuild)

data SomeStackElem = forall t. SomeStackElem (StkEl t)

instance Eq SomeStackElem where
  SomeStackElem (StkEl v1) == SomeStackElem (StkEl v2) =
    v1 `T.eqValueExt` v2

data SomeStack where
  SomeStack :: forall s. Rec StkEl s -> SomeStack

instance Buildable SomeStack where
  build (SomeStack v) = case v of
    RNil -> "[]"
    x :& xs -> Debug.show x <> " :& " <> build (SomeStack xs)

instance Eq SomeStack where
  a == b = stackToElems a == stackToElems b
    where
      stackToElems :: SomeStack -> [SomeStackElem]
      stackToElems (SomeStack stack) = recordToList $ rmap (Const . SomeStackElem) stack

data TestInterpreterState = TestInterpreterState
  { _tisInterpreterState :: InterpreterState
  , _tisStacks :: [Maybe SomeStack]
    -- ^ A list of the stacks we expect to see after running each instruction.
    -- Use 'Nothing' when you don't care what the stack looks like at that step.
  }

makeLensesFor [("_tisStacks", "tisStacks")] ''TestInterpreterState

someStack :: Rec StkEl s -> Maybe SomeStack
someStack = Just . SomeStack

ignoreStack :: Maybe SomeStack
ignoreStack = Nothing

instance (Monad m, Monoid w) => InterpreterStateMonad (RWST r w TestInterpreterState m) where
  stateInterpreterState f = state
      (\TestInterpreterState{..} ->
        let (a, newSt) = f _tisInterpreterState in (a, TestInterpreterState{_tisInterpreterState=newSt,..}))

type TestInstrRunner = InstrRunner (ExceptT (MichelsonFailureWithStack Void) $ RWST ContractEnv MorleyLogsBuilder TestInterpreterState IO)

runInstrTest :: HasCallStack => TestInstrRunner
runInstrTest = runInstrTestImpl False

runInstrTestImpl :: HasCallStack => Bool -> TestInstrRunner
runInstrTestImpl skipInstr instr stack = do
  actualStack <- runInstrImpl (runInstrTestImpl $ isAnnotated instr) instr stack
  when (isRealInstr instr && not skipInstr) do
    (tisStacks <<%= Unsafe.tail) <&> Unsafe.head >>= \case
      Nothing ->
        -- We don't care what the stack looks like at this step, so we do nothing here.
        pass
      Just expectedStack ->
        -- We care what the stack looks like at this step,
        -- so we check whether it matches our expectation.
        liftIO $
          assertEqualBuild
            (List.unlines
              [ "Actual stack did not match the expected stack after running this instruction:"
              , "  " <> pretty (T.instrToOps instr)
              ]
            )
            expectedStack
            (SomeStack actualStack)
  pure actualStack

isAnnotated :: T.Instr a b -> Bool
isAnnotated = isJust . T.instrAnns

isRealInstr :: T.Instr a b -> Bool
isRealInstr = withClassifiedInstr \case
  SFromMichelson -> const True
  SStructural -> const False
  SAdditional -> \case
    C_Nop -> False
    C_Ext{} -> True
  SPhantom -> const False

stackAnnotationsTest
  :: HasCallStack
  => String
  -> Rec StkEl (T.ContractInp cp st)
  -> [Maybe SomeStack]
  -> T.Contract cp st
  -> TestTree
stackAnnotationsTest description initialStack expectedStacks contract =
  testCase description $
    stackAnnotationsAssertion initialStack expectedStacks contract

stackAnnotationsAssertion
  :: HasCallStack
  => Rec StkEl (T.ContractInp cp st)
  -> [Maybe SomeStack]
  -> T.Contract cp st
  -> Assertion
stackAnnotationsAssertion initialStack expectedStacks contract = do
  let initialState =
        TestInterpreterState
          (initInterpreterState dummyGlobalCounter dummyBigMapCounter dummyContractEnv)
          expectedStacks
  let action = runInstrImpl runInstrTest (T.unContractCode $ T.cCode contract) initialStack
  (result, finalState, _logs) <- runRWST (runExceptT action) dummyContractEnv initialState

  assertEqualBuild
    "Finished interpreting contract, but we still expected more instructions to be run."
    []
    (finalState ^. tisStacks)

  whenLeft result (assertFailure . pretty)

ops :: T.Value ('T.TList 'T.TOperation)
ops = T.VList []

test_complex_instructions_stacks :: IO [TestTree]
test_complex_instructions_stacks = concat <$> sequenceA
  [ importContract (contractsDir </> "if-none-annots.tz") <&> \contract ->
    [ stackAnnotationsTest
        "IF_NONE reports correct stack with None (morley-debugger#4)"
        (StkEl (T.VPair (T.VOption Nothing, T.VInt 0)) :& RNil)
        expectedStacksIF_NONE_None
        contract
    , stackAnnotationsTest
        "IF_NONE reports correct stack with Some (morley-debugger#4)"
        (StkEl (T.VPair (T.VOption $ Just $ T.VInt 6, T.VInt 0)) :& RNil)
        expectedStacksIF_NONE_Some
        contract
    ]
  , importContract (contractsDir </> "if-left-annots.tz") <&> \contract ->
    [ stackAnnotationsTest
        "IF_LEFT reports correct stack with Left (morley-debugger#4)"
        (StkEl (T.VPair (T.VOr $ Left $ T.VInt 6, T.VInt 0)) :& RNil)
        expectedStacksIF_LEFT_Left
        contract
    , stackAnnotationsTest
        "IF_LEFT reports correct stack with Right (morley-debugger#4)"
        (StkEl (T.VPair (T.VOr $ Right $ T.VInt 7, T.VInt 0)) :& RNil)
        expectedStacksIF_LEFT_Right
        contract
    ]
  , importContract (contractsDir </> "if-cons-annots.tz") <&> \contract ->
    [ stackAnnotationsTest
        "IF_CONS reports correct stack with {10} (morley-debugger#4)"
        (StkEl (T.VPair (T.VList [T.VInt 10], T.VInt 0)) :& RNil)
        expectedStacksIF_CONS_Cons
        contract
    , stackAnnotationsTest
        "IF_CONS reports correct stack with {} (morley-debugger#4)"
        (StkEl (T.VPair (T.VList [], T.VInt 0)) :& RNil)
        expectedStacksIF_CONS_Nil
        contract
    ]
  ]
  where
    expectedStacksIF_NONE_None :: [Maybe SomeStack]
    expectedStacksIF_NONE_None =
      [ -- CAR
        someStack $ StkEl (T.VOption @'T.TInt Nothing) :& RNil
        -- PUSH (int :y) 0
      , someStack $ StkEl (T.VInt 0) :& RNil
        -- IF_NONE
      , someStack $ StkEl (T.VInt 0) :& RNil
        -- NIL operation
        -- PAIR
      , ignoreStack, ignoreStack
      ]

    expectedStacksIF_NONE_Some :: [Maybe SomeStack]
    expectedStacksIF_NONE_Some =
      [ -- CAR
        someStack $ StkEl (T.VOption $ Just $ T.VInt 6) :& RNil
        -- PUSH (int :y) 1
      , someStack $ StkEl (T.VInt 1) :& StkEl (T.VInt 6) :& RNil
        -- ADD
      , someStack $ StkEl (T.VInt 7) :& RNil
        -- IF_NONE
      , someStack $ StkEl (T.VInt 7) :& RNil
        -- NIL operation
        -- PAIR
      , ignoreStack, ignoreStack
      ]

    expectedStacksIF_LEFT_Left :: [Maybe SomeStack]
    expectedStacksIF_LEFT_Left =
      [ -- CAR
        someStack $ StkEl (T.VOr @'T.TInt @'T.TInt $ Left $ T.VInt 6) :& RNil
        -- PUSH (int :y) 1
      , someStack $ StkEl (T.VInt 1) :& StkEl (T.VInt 6) :& RNil
        -- ADD
      , someStack $ StkEl (T.VInt 7) :& RNil
        -- IF_LEFT
      , someStack $ StkEl (T.VInt 7) :& RNil
        -- NIL operation
        -- PAIR
      , ignoreStack, ignoreStack
      ]

    expectedStacksIF_LEFT_Right :: [Maybe SomeStack]
    expectedStacksIF_LEFT_Right =
      [ -- CAR
        someStack $ StkEl (T.VOr @'T.TInt $ Right $ T.VInt 7) :& RNil
        -- PUSH (int :y) 2
      , someStack $ StkEl (T.VInt 2) :& StkEl (T.VInt 7) :& RNil
        -- ADD
      , someStack $ StkEl (T.VInt 9) :& RNil
        -- IF_LEFT
      , someStack $ StkEl (T.VInt 9) :& RNil
        -- NIL operation
        -- PAIR
      , ignoreStack, ignoreStack
      ]

    expectedStacksIF_CONS_Cons :: [Maybe SomeStack]
    expectedStacksIF_CONS_Cons =
      [ -- CAR
        someStack $ StkEl (T.VList [T.VInt 10]) :& RNil
        -- DROP
      , someStack $ RNil
        -- DIP
      , someStack $ StkEl (T.VInt 10) :& RNil
        -- IF_CONS
      , someStack $ StkEl (T.VInt 10) :& RNil
        -- NIL operation
        -- TODO [#507]: This param should not be here
      , someStack $ StkEl ops :& StkEl (T.VInt 10) :& RNil
        -- PAIR
      , ignoreStack
      ]

    expectedStacksIF_CONS_Nil :: [Maybe SomeStack]
    expectedStacksIF_CONS_Nil =
      [ -- CAR
        someStack $ StkEl (T.VList @'T.TInt []) :& RNil
        -- PUSH (int :y) 0
      , someStack $ StkEl (T.VInt 0) :& RNil
        -- IF_CONS
      , someStack $ StkEl (T.VInt 0) :& RNil
        -- NIL operation
        -- PAIR
      , ignoreStack, ignoreStack
      ]

test_cast_notes :: IO [TestTree]
test_cast_notes = sequenceA
  [ importContract (contractsDir </> "cast-annots.tz") <&>
    stackAnnotationsTest
      "CAST is no-op"
      (StkEl (T.VPair (T.VUnit, T.VPair (T.VInt 0, T.VInt 0))) :& RNil)
      expectedStacksCast
  ]
  where
    pair :: T.Value ('T.TPair 'T.TInt 'T.TInt)
    pair = T.VPair (T.VInt 1, T.VInt 2)

    expectedStacksCast :: [Maybe SomeStack]
    expectedStacksCast =
      [ -- DROP
        someStack RNil
        -- PUSH @ab (pair :p (int :a) (int :b)) (Pair 1 2)
      , someStack $ StkEl pair :& RNil
        -- CAST (pair int int)
      , someStack $ StkEl pair :& RNil
        -- CAST @xy (pair (int :x) (int :y))
      , someStack $ StkEl pair :& RNil
        -- NIL operation
      , ignoreStack
        -- PAIR
      , someStack $ StkEl (T.VPair (ops, pair)) :& RNil
      ]

test_interpret_stack :: IO [TestTree]
test_interpret_stack = sequenceA
  [ importContract (contractsDir </> "loop-annots.tz") <&>
    stackAnnotationsTest
      "Stacks are correct in LOOP (morley-debugger#4)"
      (StkEl (T.VPair (T.VUnit, T.VInt 0)) :& RNil)
      expectedStacksLoop
  , importContract (contractsDir </> "map-annots.tz") <&>
    stackAnnotationsTest
      "Stacks are correct in MAP (morley-debugger#4)"
      (StkEl (T.VPair (T.VUnit, T.VInt 0)) :& RNil)
      expectedStacksMap
  ]
  where
    expectedStacksLoop :: [Maybe SomeStack]
    expectedStacksLoop =
      [ -- DROP
        someStack RNil
        -- PUSH @q (int :q) 2
      , someStack $ StkEl (T.VInt 2) :& RNil
        -- PUSH bool True
      , someStack $ StkEl (T.VBool True) :& StkEl (T.VInt 2) :& RNil
        -- CAST int
      , someStack $ StkEl (T.VInt 2) :& RNil
        -- RENAME
      , someStack $ StkEl (T.VInt 2) :& RNil
        -- PUSH int -1
      , someStack $ StkEl (T.VInt (-1)) :& StkEl (T.VInt 2) :& RNil
        -- ADD
      , someStack $ StkEl (T.VInt 1) :& RNil
        -- DUP
      , someStack $ StkEl (T.VInt 1) :& StkEl (T.VInt 1) :& RNil
        -- GT
      , someStack $ StkEl (T.VBool True) :& StkEl (T.VInt 1) :& RNil
        -- CAST int
      , someStack $ StkEl (T.VInt 1) :& RNil
        -- RENAME
      , someStack $ StkEl (T.VInt 1) :& RNil
        -- PUSH int -1
      , someStack $ StkEl (T.VInt (-1)) :& StkEl (T.VInt 1) :& RNil
        -- ADD
      , someStack $ StkEl (T.VInt 0) :& RNil
        -- DUP
      , someStack $ StkEl (T.VInt 0) :& StkEl (T.VInt 0) :& RNil
        -- GT
      , someStack $ StkEl (T.VBool False) :& StkEl (T.VInt 0) :& RNil
        -- n.b.: these LOOP instructions are an implementation detail in Morley
        -- and do not appear in tezos-client while running an script with the
        -- --trace-stack flag.
        -- LOOP
      , someStack $ StkEl (T.VInt 0) :& RNil
        -- LOOP
      , someStack $ StkEl (T.VInt 0) :& RNil
        -- LOOP
      , someStack $ StkEl (T.VInt 0) :& RNil
        -- NIL operation
      , ignoreStack
        -- PAIR
      , someStack $ StkEl (T.VPair (ops, T.VInt 0)) :& RNil
      ]

    expectedStacksMap :: [Maybe SomeStack]
    expectedStacksMap =
      [ -- DROP
        someStack $ RNil
        -- PUSH @y (int :y) 42
      , someStack $ StkEl (T.VInt 42) :& RNil
        -- PUSH @list (list :l int) {1; 10}
      , someStack $ StkEl (T.VList [T.VInt 1, T.VInt 10]) :& StkEl (T.VInt 42) :& RNil
        -- CAST @q (int :q)
      , someStack $ StkEl (T.VInt 1) :& StkEl (T.VInt 42) :& RNil
        -- CAST int
      , someStack $ StkEl (T.VInt 42) :& RNil
        -- DIP
      , someStack $ StkEl (T.VInt 1) :& StkEl (T.VInt 42) :& RNil
        -- CAST @q (int :q)
      , someStack $ StkEl (T.VInt 10) :& StkEl (T.VInt 42) :& RNil
        -- CAST int
      , someStack $ StkEl (T.VInt 42) :& RNil
        -- DIP
      , someStack $ StkEl (T.VInt 10) :& StkEl (T.VInt 42) :& RNil
        -- MAP
      , someStack $ StkEl (T.VList [T.VInt 1, T.VInt 10]) :& StkEl (T.VInt 42) :& RNil
        -- DROP
      , someStack $ StkEl (T.VInt 42) :& RNil
        -- NIL operation
      , ignoreStack
        -- PAIR
      , someStack $ StkEl (T.VPair (ops, T.VInt 42)) :& RNil
      ]

test_update_stacks :: IO [TestTree]
test_update_stacks = sequenceA
  [ importContract (contractsDir </> "update-annots.tz") <&>
    stackAnnotationsTest
      "UPDATE reports a correct stack"
      (StkEl (T.VPair (T.VUnit, T.VMap @'T.TInt @'T.TInt Map.empty)) :& RNil)
      expectedStacksUpdate
  ]
  where
    intMap :: T.Value ('T.TMap 'T.TInt 'T.TInt)
    intMap = T.VMap $ Map.fromList [(T.VInt 0, T.VInt 1), (T.VInt 1, T.VInt 2), (T.VInt 2, T.VInt 0)]

    intMap' :: T.Value ('T.TMap 'T.TInt 'T.TInt)
    intMap' = T.VMap $ Map.fromList [(T.VInt 0, T.VInt 1), (T.VInt 1, T.VInt 2)]

    expectedStacksUpdate :: [Maybe SomeStack]
    expectedStacksUpdate =
      [ -- DROP
        someStack RNil
        -- PUSH @s (map :m int int) {Elt 0 1; Elt 1 2; Elt 2 0}
      , someStack $ StkEl intMap :& RNil
        -- NONE @v (int :v)
      , someStack $ StkEl (T.VOption @'T.TInt Nothing) :& StkEl intMap :& RNil
        -- PUSH @k (int :k) 2
      , someStack $ StkEl (T.VInt 2) :& StkEl (T.VOption @'T.TInt Nothing) :& StkEl intMap :& RNil
        -- UPDATE
      , someStack $ StkEl intMap' :& RNil
        -- NIL operation
      , ignoreStack
        -- PAIR
      , someStack $ StkEl (T.VPair (ops, intMap')) :& RNil
      ]

test_create_contract_stacks :: IO [TestTree]
test_create_contract_stacks = sequenceA
  [ importContract (contractsDir </> "create-contract-annots.tz") <&>
    stackAnnotationsTest
      "CREATE_CONTRACT produces a correct stack"
      (StkEl (T.VPair (T.VUnit, storage)) :& RNil)
      expectedStacksCreateContract
  ]
  where
    address :: ContractAddress
    address = [ta|KT1Cb7mVHmedj3Q1vfXvaiRqeNDMqpLbMKjD|]

    storage :: T.Value 'T.TAddress
    storage = T.VAddress $ T.EpAddress address T.DefEpName

    op :: T.Value ('T.TOption 'T.TKeyHash)
    op = T.VOption Nothing

    mutez :: T.Value 'T.TMutez
    mutez = T.VMutez 100

    contract :: T.Value 'T.TOperation
    contract = T.VOp $ T.OpCreateContract T.CreateContract
      { T.ccOriginator = Constrained dummySelf
      , T.ccDelegate = Nothing
      , T.ccBalance = 100
      , T.ccStorageVal = T.VUnit
      , T.ccContract = T.defaultContract $
        let loc c = T.WithLoc $ ErrorSrcPos $ srcPos 12 c in
        loc 19 T.CDR
        `T.Seq` loc 24 T.NIL
        `T.Seq` loc 39 T.PAIR :: T.Instr (T.ContractInp 'T.TUnit 'T.TUnit) (T.ContractOut 'T.TUnit)
      , T.ccCounter = 1
      }

    expectedStacksCreateContract :: [Maybe SomeStack]
    expectedStacksCreateContract =
      [ -- DROP
        someStack RNil
      , -- UNIT
        someStack $ StkEl T.VUnit :& RNil
      , -- AMOUNT
        someStack $ StkEl mutez :& StkEl T.VUnit :& RNil
      , -- NONE key_hash
        someStack $ StkEl op :& StkEl mutez :& StkEl T.VUnit :& RNil
      , -- CREATE_CONTRACT @op @addr
        someStack $ StkEl contract :& StkEl storage :& RNil
      , -- NIL operation
        someStack $ StkEl ops :& StkEl contract :& StkEl storage :& RNil
      , -- SWAP
        someStack $ StkEl contract :& StkEl ops :& StkEl storage :& RNil
      , -- CONS
        someStack $ StkEl (T.VList [contract]) :& StkEl storage :& RNil
      , -- PAIR
        someStack $ StkEl (T.VPair (T.VList [contract], storage)) :& RNil
      ]

unit_PAIRN_and_UNPAIRN_stacks :: Assertion
unit_PAIRN_and_UNPAIRN_stacks = do
  contract <- importContract (contractsDir </> "pair_n_unpair_n.tz")
  stackAnnotationsAssertion
    (StkEl contractInput :& RNil)
    expectedStacks
    contract
  where
    storage = (1, (2, 3)) :: (Integer, (Integer, Integer))
    param = (4, (5, 6)) :: (Integer, (Integer, Integer))
    contractInput = toVal (param, storage)

    expectedStacks :: [Maybe SomeStack]
    expectedStacks =
      [ -- CAR
        someStack $
          StkEl (toVal param) :&
          RNil
        -- UNPAIR 3
      , someStack $
          StkEl (toVal @Integer 4) :&
          StkEl (toVal @Integer 5) :&
          StkEl (toVal @Integer 6) :&
          RNil
        -- PAIR @newStorage 3;
      , someStack $
          StkEl (toVal param) :&
          RNil
        -- NIL operation;
      , someStack $
          StkEl ops :&
          StkEl (toVal param) :&
          RNil
        -- PAIR 2;
      , someStack $
          StkEl (toVal ([] :: [Operation], param)) :& RNil
      ]

unit_GETN_stack :: Assertion
unit_GETN_stack = do
  contract <- importContract (contractsDir </> "get_n.tz")
  stackAnnotationsAssertion
    (StkEl contractInput :& RNil)
    expectedStacks
    contract
  where
    storage = (5, 6) :: (Integer, Integer)
    param = (1, (2, 3)) :: (Integer, (Integer, Integer))
    contractInput = toVal (param, storage)

    expectedStacks :: [Maybe SomeStack]
    expectedStacks =
      [ -- CAR
       ignoreStack
        -- DUP
      , someStack $
          StkEl (toVal param) :&
          StkEl (toVal param) :&
          RNil
        -- GET 3;
      , someStack $
          StkEl (toVal @Integer 2) :&
          StkEl (toVal param) :&
          RNil
        -- SWAP;
      , someStack $
          StkEl (toVal param) :&
          StkEl (toVal @Integer 2) :&
          RNil
        -- GET @aa 1
      , someStack $
          StkEl (toVal @Integer 1) :&
          StkEl (toVal @Integer 2) :&
          RNil
        -- PAIR 2
        -- NIL operation;
        -- PAIR 2;
      , ignoreStack, ignoreStack, ignoreStack
      ]

unit_GET_0_stack :: Assertion
unit_GET_0_stack = do
  contract <- importContract (contractsDir </> "get_0.tz")
  stackAnnotationsAssertion
    (StkEl (toVal ((), 0 :: Integer)) :& RNil)
    expectedStacks
    contract
  where
    expectedStacks :: [Maybe SomeStack]
    expectedStacks =
      [ -- DROP
        ignoreStack
        -- PUSH @var (int :i) 1
      , someStack $
          StkEl (toVal @Integer 1) :&
          RNil
        -- GET 0;
      , someStack $
          StkEl (toVal @Integer 1) :&
          RNil
        -- NIL operation;
        -- PAIR 2;
      , ignoreStack, ignoreStack
      ]

unit_UPDATEN_stack :: Assertion
unit_UPDATEN_stack = do
  contract <- importContract (contractsDir </> "update_n_field_anns.tz")
  stackAnnotationsAssertion
    (StkEl (toVal ((), (0 :: Natural, 0 :: Natural))) :& RNil)
    expectedStacks
    contract
  where
    expectedStacks :: [Maybe SomeStack]
    expectedStacks =
      [ -- DROP
        ignoreStack
        -- PUSH (pair :t0 (int :t1 %f1) (string :t2 %f2) (unit :t3 %f3)) { 0; "a"; Unit };
      , someStack $ StkEl (toVal (0 :: Integer, ("a" :: MText, ()))) :& RNil
        -- PUSH (nat :t4) 0; UPDATE 1;
        -- PUSH (nat :t5) 0; UPDATE 3;
        -- PUSH (nat :t6) 0; UPDATE 4;
      , ignoreStack, ignoreStack
      , ignoreStack, ignoreStack
      , ignoreStack
      , someStack $ StkEl (toVal (0 :: Natural, (0 :: Natural, 0 :: Natural))) :& RNil

        -- PUSH (pair (nat :t7 %f7) (nat :t8 %f8)) (Pair 0 0);
        -- UPDATE 2;
      , ignoreStack
      , someStack $ StkEl (toVal (0 :: Natural, (0 :: Natural, 0 :: Natural))) :& RNil
        -- PUSH (nat :t9) 0;
        -- UPDATE 2;
      , ignoreStack
      , someStack $
          StkEl (toVal (0 :: Natural, 0 :: Natural)) :& RNil
        -- NIL operation; PAIR;
      , ignoreStack, ignoreStack
      ]

unit_UPDATEN_stack_2 :: Assertion
unit_UPDATEN_stack_2 = do
  contract <- importContract (contractsDir </> "update_n_var_anns.tz")
  stackAnnotationsAssertion
    (StkEl (toVal ((), (0 :: Integer, 0 :: Integer))) :& RNil)
    expectedStacks
    contract
  where
    expectedStacks :: [Maybe SomeStack]
    expectedStacks =
      [ -- DROP;
        -- PUSH @v1 (pair int int) (Pair 0 0);
        -- PUSH @v2 int 0;
        ignoreStack, ignoreStack
      , someStack $
          StkEl (toVal (0 :: Integer)) :&
          StkEl (toVal (0 :: Integer, 0 :: Integer)) :&
          RNil

        -- UPDATE 1
      , someStack $
          StkEl (toVal (0 :: Integer, 0 :: Integer)) :&
          RNil

        -- PUSH int 0;
        -- UPDATE @v3 1;
      , ignoreStack
      , someStack $
          StkEl (toVal (0 :: Integer, 0 :: Integer)) :&
          RNil

        -- NIL operation; PAIR;
      , ignoreStack, ignoreStack
      ]

unit_GET_AND_UPDATE_stack :: Assertion
unit_GET_AND_UPDATE_stack = do
  contract <- importContract (contractsDir </> "get_and_update_anns.tz")
  stackAnnotationsAssertion
    (StkEl (toVal ((), ())) :& RNil)
    expectedStacks
    contract
  where
    expectedStacks :: [Maybe SomeStack]
    expectedStacks =
      [ -- DROP;
        -- EMPTY_MAP :map @varMap (int :k) (nat :v);
        -- PUSH @varVal (option (nat :v)) (Some 1);
        -- PUSH @varKey (int :k) 1;
        ignoreStack, ignoreStack, ignoreStack, ignoreStack

        -- GET_AND_UPDATE
      , someStack $
          StkEl (toVal (Nothing :: Maybe Natural)) :&
          StkEl (toVal (one (1, 1) :: Map Integer Natural)) :&
          RNil

        -- PUSH @varKey (int :k) 1;
        -- GET_AND_UPDATE @newVarMap;
      , ignoreStack
      , someStack $
          StkEl (toVal (Just 1 :: Maybe Natural)) :&
          StkEl (toVal (mempty :: Map Integer Natural)) :&
          RNil

        -- DROP 2;
        -- UNIT; NIL operation; PAIR
      , ignoreStack, ignoreStack, ignoreStack, ignoreStack
      ]

test_dupn :: IO TestTree
test_dupn = importContract (contractsDir </> "dup-n.tz") <&>
  stackAnnotationsTest
    "Check that stacks behave as intended with DUP n (#471)"
    (StkEl (T.VPair (T.VUnit, store)) :& RNil)
    expectedStacks
  where
    store :: T.Value 'T.TInt
    store = T.VInt 42

    kek :: T.Value 'T.TString
    kek = T.VString "kek"

    expectedStacks :: [Maybe SomeStack]
    expectedStacks =
      [ -- CDR
        someStack $ StkEl store :& RNil
        -- RENAME @x
      , someStack $ StkEl store :& RNil
        -- PUSH string "kek"
      , someStack $ StkEl kek :& StkEl store :& RNil
        -- DUP 2
        -- n.b.: DUP n doesn't duplicate variable annotations. This is consistent with tezos-client.
      , someStack $ StkEl store :& StkEl kek :& StkEl store :& RNil
        -- SWAP
      , someStack $ StkEl kek :& StkEl store :& StkEl store :& RNil
        -- DROP 2
      , someStack $ StkEl store :& RNil
        -- DUP @z 1
      , someStack $ StkEl store :& StkEl store :& RNil
        -- DROP
      , someStack $ StkEl store :& RNil
        -- NIL operation
      , ignoreStack
        -- PAIR
      , someStack $ StkEl (VPair (ops, store)) :& RNil
      ]

test_nested_stacks :: IO [TestTree]
test_nested_stacks = sequenceA
  [ importContract (contractsDir </> "nested-pair-annots.tz") <&>
    stackAnnotationsTest
      "CAR interacts with stacks (morley-debugger#13)"
      (mkInitStack (VNat 1) (VNat 2))
      expectedStacks1
  , importContract (contractsDir </> "nested-pair-annots-2.tz") <&>
    stackAnnotationsTest
      "CAR @a overrides stacks (morley-debugger#13)"
      (mkInitStack (VNat 1) (VNat 2))
      expectedStacks2
  ]
  where
    parameterStk = StkEl (VNat 1)

    storageStk = StkEl (VNat 2)

    expectedStacks1 :: [Maybe SomeStack]
    expectedStacks1 =
      [ -- UNPAIR
        someStack $ parameterStk :& storageStk :& RNil
        -- UNIT @something
      , someStack $
          StkEl VUnit :&
          parameterStk :&
          storageStk :&
          RNil
        -- PAIR
      , someStack $
          StkEl (VPair (VUnit, VNat 1)) :&
          storageStk :&
          RNil
        -- CAR
      , someStack $ StkEl VUnit :& storageStk :& RNil
        -- DROP
      , ignoreStack
        -- NIL operation
      , ignoreStack
        -- PAIR
      , someStack $
          StkEl (VPair (ops, VNat 2)) :&
          RNil
      ]

    expectedStacks2 :: [Maybe SomeStack]
    expectedStacks2 = expectedStacks1 & ix 3 .~
      -- CAR @a
      ( someStack $ StkEl VUnit :& storageStk :& RNil
      )

unit_CARk_CDRk_interact_with_stacks :: Assertion
unit_CARk_CDRk_interact_with_stacks = do
  contract <- importContract (contractsDir </> "carn_and_cdrn.tz")
  stackAnnotationsAssertion
    (StkEl (toVal (param, ())) :& RNil)
    expectedStacks
    contract
  where
    param = (1, 2, ()) :: (Natural, Natural, ())

    expectedStacks :: [Maybe SomeStack]
    expectedStacks =
      [ -- CAR
        someStack $ StkEl (toVal param) :& RNil
        -- GET 0
      , someStack $ StkEl (toVal param) :& RNil
        -- DUP
      , ignoreStack
        -- CAR 0
      , someStack $
          StkEl (toVal @Natural 1) :&
          StkEl (toVal param) :&
          RNil
        -- DROP
      , ignoreStack
        -- DUP
      , ignoreStack
        -- CAR 1
      , someStack $
          StkEl (toVal @Natural 2) :&
          StkEl (toVal param) :&
          RNil
        -- DROP
      , ignoreStack
        -- GET @kek 4
      , someStack $ StkEl VUnit :& RNil
        -- NIL operation
      , ignoreStack
        -- PAIR
      , someStack $ StkEl (VPair (ops, VUnit)) :& RNil
      ]

unit_CAR_special_stacks :: Assertion
unit_CAR_special_stacks = do
  contract <- importContract (contractsDir </> "car_special_annots.tz")
  stackAnnotationsAssertion
    (StkEl (toVal ((), ())) :& RNil)
    expectedStacks
    contract
  where
    expectedStacks :: [Maybe SomeStack]
    expectedStacks =

      replicate 28 ignoreStack
        -- When the pair has no annotations
        & ix 2 .~ (someStack $ StkEl (toVal ()) :& RNil)
        & ix 5 .~ (someStack $ StkEl (toVal ()) :& RNil)

        -- When the pair has a var annotation
        & ix 8 .~ (someStack $ StkEl (toVal ()) :& RNil)
        & ix 11 .~ (someStack $ StkEl (toVal ()) :& RNil)

        -- When the pair has field annotations
        & ix 14 .~ (someStack $ StkEl (toVal ()) :& RNil)
        & ix 17 .~ (someStack $ StkEl (toVal ()) :& RNil)

        -- When the pair has var + field annotations
        & ix 20 .~ (someStack $ StkEl (toVal ()) :& RNil)
        & ix 23 .~ (someStack $ StkEl (toVal ()) :& RNil)

unit_CDR_special_stacks :: Assertion
unit_CDR_special_stacks = do
  contract <- importContract (contractsDir </> "cdr_special_annots.tz")
  stackAnnotationsAssertion
    (StkEl (toVal ((), ())) :& RNil)
    expectedStacks
    contract
  where
    expectedStacks :: [Maybe SomeStack]
    expectedStacks =

      replicate 28 ignoreStack
        -- When the pair has no annotations
        & ix 2 .~ (someStack $ StkEl (toVal ()) :& RNil)
        & ix 5 .~ (someStack $ StkEl (toVal ()) :& RNil)

        -- When the pair has a var annotation
        & ix 8 .~ (someStack $ StkEl (toVal ()) :& RNil)
        & ix 11 .~ (someStack $ StkEl (toVal ()) :& RNil)

        -- When the pair has field annotations
        & ix 14 .~ (someStack $ StkEl (toVal ()) :& RNil)
        & ix 17 .~ (someStack $ StkEl (toVal ()) :& RNil)

        -- When the pair has var + field annotations
        & ix 20 .~ (someStack $ StkEl (toVal ()) :& RNil)
        & ix 23 .~ (someStack $ StkEl (toVal ()) :& RNil)

unit_UNPAIR_special_stacks :: Assertion
unit_UNPAIR_special_stacks = do
  contract <- importContract (contractsDir </> "unpair_special_annots.tz")
  stackAnnotationsAssertion
    (StkEl (toVal ((), ())) :& RNil)
    expectedStacks
    contract
  where
    expectedStacks :: [Maybe SomeStack]
    expectedStacks =

      replicate 16 ignoreStack
        -- When the pair has no annotations
        & ix 2 .~ (someStack $ StkEl (toVal ()) :& StkEl (toVal ()) :& RNil)

        -- When the pair has a var annotation
        & ix 5 .~ (someStack $ StkEl (toVal ()) :& StkEl (toVal ()) :& RNil)

        -- When the pair has field annotations
        & ix 8 .~ (someStack $ StkEl (toVal ()) :& StkEl (toVal ())  :& RNil)

        -- When the pair has var + field annotations
        & ix 11 .~ (someStack $ StkEl (toVal ()) :& StkEl (toVal ()) :& RNil)

test_special_stacks :: IO TestTree
test_special_stacks = importContract (contractsDir </> "right_special_anns.tz") <&>
    stackAnnotationsTest
      "RIGHT reports correct stacks (#439)"
      (StkEl (VPair (VUnit, VUnit)) :& RNil)
      expectedStacksRIGHTSpecial
  where
    expectedStacksRIGHTSpecial :: [Maybe SomeStack]
    expectedStacksRIGHTSpecial =
      [ -- CAR
        someStack $ StkEl (VUnit) :& RNil
        -- RIGHT % %@ never
      , someStack $ StkEl (VOr @'T.TNever $ Right $ VUnit) :& RNil
        -- DROP
        -- PUSH unit Unit
        -- NUL operation
        -- PAIR
      , ignoreStack, ignoreStack, ignoreStack, ignoreStack
      ]
