-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Interpreter tests involving 'StackRef'.

module Test.Interpreter.StackRef
  ( test_mkStackRef
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Typed
import Test.Cleveland

test_mkStackRef :: TestTree
test_mkStackRef =
  testScenarioOnEmulator "does not segfault" $ scenario do
    handle <- originate "test_contract" () $ TypedContract @() @() @() contract
    transfer handle
  where
    stackRef = PrintComment . one . Right $ mkStackRef @1

    contract :: Contract 'TUnit 'TUnit
    contract = Contract
      { cCode = contractCode
      , cStoreNotes = starNotes
      , cParamNotes = starParamNotes
      , cEntriesOrder = def
      , cViews = def
      }

    contractCode :: ContractCode 'TUnit 'TUnit
    contractCode = mkContractCode $
      CAR `Seq` DUP `Seq` Ext (PRINT stackRef) `Seq`
      DROP `Seq` NIL `Seq` PAIR
