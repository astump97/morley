-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Module, containing spec to test conditionals.tz contract.
module Test.Interpreter.Conditionals
  ( test_conditionals
  ) where

import Hedgehog (forAll, property)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)

import Hedgehog.Gen.Michelson (genMText)
import Morley.Michelson.Text
import Morley.Michelson.Typed (ToT, convertContract)
import Morley.Michelson.Untyped
import Test.Cleveland
import Test.Cleveland.Michelson.Import (embedContract)

import Test.Util.Contracts

type Param = Either MText (Maybe Integer)

-- | Spec to test conditionals.tz contract.
test_conditionals :: [TestTree]
test_conditionals =
  [ testScenarioOnEmulator "single test" $ scenario do
      handle <- originate "conditionals" (ValueString "storage") $ convertContract contract
      transfer handle $ unsafeCalling def @Param $ Left "abc"
      getStorage @MText handle @@== "abc"
  , testProperty "randomized check" $ property do
      inputParam :: Param <- forAll $ Gen.either
        (genMText def)
        (Gen.maybe (Gen.integral (Range.linearFrom 0 -1000 1000)))
      testScenarioProps $ scenario do
        handle <- originate "conditionals" (ValueString "storage") $ convertContract contract
        transfer handle (unsafeCalling def inputParam)
          & case inputParam of
              Left str -> (>> (getStorage @MText handle @@== str))
              Right Nothing -> expectFailedWith ()
              Right (Just a)
                | a < 0 -> expectFailedWith ()
                | otherwise -> (>> (getStorage @MText handle @@== ""))
  ]
  where
    contract = $$(embedContract @(ToT Param) @(ToT MText) (inContractsDir "tezos_examples/attic/conditionals.tz"))
