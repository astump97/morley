-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Utilities for testing entrypoints documentation.
module Test.Lorentz.Entrypoints.Doc.Utils
  ( hasNot
  , hasEP
  , countEPSections
  ) where

import Data.Text (count, isInfixOf)
import Test.Tasty.HUnit (Assertion, assertBool, (@?=))

has :: Text -> Text -> Assertion
has docs s = assertBool ("Contains \"" <> toString s <> "\"") $ s `isInfixOf` docs

hasNot :: Text -> Text -> Assertion
hasNot docs s = assertBool ("Does not contain \"" <> toString s <> "\"") $ not $ s `isInfixOf` docs

hasEP :: Text -> Text -> IO ()
hasEP docs n = do
  has docs $ "\n## `"<>n<>"`\n"
  has docs $ "\n1. Call contract's `"<>n<>"` entrypoint"

countEPSections :: Text -> Int -> Assertion
countEPSections docs = (count "# Entrypoints\n" docs @?=)
