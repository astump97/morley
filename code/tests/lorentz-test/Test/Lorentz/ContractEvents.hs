-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE QualifiedDo, NoApplicativeDo #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Test.Lorentz.ContractEvents
  ( test_LorentzEmit
  , test_LorentzEmitAnnotated
  , test_LorentzEmitManual
  ) where

import Fmt (pretty)
import Test.Tasty (TestTree)

import Lorentz
  (Contract, HasAnnotation, IsoValue, MText, car, cons, defaultContract, dip, emit, emit', emitAuto, mt, nil,
  pair, push, right, (>>))
import Morley.Michelson.Parser (notes)
import Test.Cleveland

test_LorentzEmit :: TestTree
test_LorentzEmit =
  testScenario "emit works in a lorentz contract" $ scenario do
    lorentzEmit <- originate "lorentz_emit" () contract
    [ContractEvent{..}] <- transfer lorentzEmit WithContractEvents $ calling def ()
    pretty cePayload @== ("Right \"right\" :: or nat string" :: Text)
    toAddress ceSource @== toAddress lorentzEmit
    ceTag @== "notify_client"
  where
    contract :: Contract () () ()
    contract = defaultContract Lorentz.do
      car
      push [mt|right|]
      right @Natural
      emitAuto "notify_client"
      dip nil
      cons
      pair

data AnnotatedValue = Int Natural | Str MText
  deriving stock (Generic)
  deriving anyclass (IsoValue, HasAnnotation)

test_LorentzEmitAnnotated :: TestTree
test_LorentzEmitAnnotated =
  testScenario "Annotated emit works on lorentz contract" $ scenario do
    lorentzEmit <- originate "lorentz_emit" () contract
    [ContractEvent{..}] <- transfer lorentzEmit WithContractEvents $ calling def ()
    pretty cePayload @== ("Right \"right\" :: or (nat %int) (string %str)" :: Text)
    toAddress ceSource @== toAddress lorentzEmit
    ceTag @== "notify_client"
  where
    contract :: Contract () () ()
    contract = defaultContract Lorentz.do
      car
      push $ Str [mt|right|]
      emit "notify_client"
      dip nil
      cons
      pair

test_LorentzEmitManual :: TestTree
test_LorentzEmitManual =
  testScenario "Manually annotated emit works on lorentz contract" $ scenario do
    lorentzEmit <- originate "lorentz_emit" () contract
    [ContractEvent{..}] <- transfer lorentzEmit WithContractEvents $ calling def ()
    pretty cePayload @== ("Right \"right\" :: or (nat %nat) (string %str)" :: Text)
    toAddress ceSource @== toAddress lorentzEmit
    ceTag @== "notify_client"
  where
    contract :: Contract () () ()
    contract = defaultContract Lorentz.do
      car
      push [mt|right|]
      right @Natural
      emit' "notify_client" $ Just [notes|or (nat %nat) (string %str)|]
      dip nil
      cons
      pair
