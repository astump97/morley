-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-unused-do-bind #-}
{-# LANGUAGE QualifiedDo, NoApplicativeDo #-}

-- | Tests for txr1 addresses
module Test.Lorentz.Txr1Address
  ( test_txr1keyhash
  ) where

import Prelude (unsafe, pass)
import Lorentz

import Test.Tasty (TestTree)

import Morley.Tezos.Address
import Morley.Tezos.Crypto
import Test.Cleveland

test_txr1keyhash :: TestTree
test_txr1keyhash = testScenario "txr1 address is passed properly" $ scenario do
  handle <- originate "txr1_test" () txr1Contract
  let caddr = toTAddress [ta|txr1juGAfkUFU3Y3oEPfsBnruQxXaHgvsCnAB|]
      l2addr = TxRollupL2Address $ unsafe $ parseHash "tz4LVHYD4P4T5NHCuwJbxQvwVURF62seE3Qa"
  -- TODO [#835]: support transaction rollups in morley-client/cleveland
  --
  -- TODO [#838]: support transaction rollups on the emulator
  --
  -- Until we support transaction rollups, the following call would fail, but we
  -- still want to typecheck it.
  if False
    then transfer handle $ calling def (caddr, l2addr)
    else pass

txr1Contract :: Contract (TAddress (Txr1CallParam ()) (), TxRollupL2Address) () ()
txr1Contract = compileLorentzContract $ defaultContractData Lorentz.do
  car

  unpair
  contractCalling (Call @"Deposit")

  assertSome @MText "Expected txr1 address"

  swap

  -- amount for transferring
  push @Mutez 0
  swap

  -- create a ticket
  push @Natural 10
  unit
  ticket

  pair

  -- deposit
  transferTokens

  dip nil
  cons
  dip unit
  pair
