-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for gas limit calculation implementation in 'morley-client'.
module Test.GasLimit
  ( test_GasLimitCalculation
  ) where

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Morley.Client (runMorleyClientM, runMorleyOnlyRpcM)
import Morley.Client.Action.Batched
import Morley.Client.Action.Common (TD(..), TransactionData(..))
import Morley.Client.TezosClient.Impl (getSecretKey)
import Morley.Michelson.Typed (IsoValue(..))
import Morley.Michelson.Untyped (pattern DefEpName)
import Morley.Tezos.Address
import Morley.Tezos.Address.Alias
import Morley.Tezos.Core (tz)
import Morley.Tezos.Crypto
import Morley.Tezos.Crypto.Ed25519 (randomSecretKey)

import Test.Cleveland (mkMorleyOnlyRpcEnvNetwork, neMorleyClientEnv)
import Test.Cleveland.Internal.Abstract (Moneybag(..))
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Tasty (whenNetworkEnabled)

test_GasLimitCalculation :: IO TestTree
test_GasLimitCalculation = pure $ whenNetworkEnabled $ \withEnv ->
  testCase "Attempt a batch with 100 transfers" $ withEnv \env -> do
    Moneybag moneybagAddr <- setupMoneybagAddress env
    moneybagSecretKey <- runMorleyClientM (neMorleyClientEnv env) $ do
      getSecretKey (AddressResolved moneybagAddr)

    destSecretKey <- SecretKeyEd25519 <$> randomSecretKey
    let dest = mkKeyAddress . toPublic $ destSecretKey

    -- NB: using MorleyOnlyRpcM because it's _a lot_ faster; MorleyClientM
    -- spends an unholy amount of time looking up aliases.
    void $ runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [destSecretKey, moneybagSecretKey]) do
      runOperationsBatch (AddressResolved moneybagAddr) $ replicateM 100 $ do
        runTransactionM $ TransactionData TD
          { tdReceiver = Constrained dest
          , tdAmount = [tz|100u|]
          , tdEpName = DefEpName
          , tdParam = toVal ()
          , tdMbFee = Nothing
          }
    pass
