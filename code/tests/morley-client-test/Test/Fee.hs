-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests for fee calculation implementation in 'morley-client'.
module Test.Fee
  ( test_FeeCalculation
  ) where

import Data.List.NonEmpty qualified as NE
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (Assertion, testCase, (@?=))

import Morley.Client
import Morley.Client.Action.Common (RevealData(..), computeStorageLimit)
import Morley.Client.TezosClient.Impl
  (calcOriginationFee, calcRevealFee, calcTransferFee, importKey)
import Morley.Client.TezosClient.Types (CalcOriginationFeeData(..), CalcTransferFeeData(..))
import Morley.Micheline.Json (TezosMutez(..))
import Morley.Michelson.Runtime.GState (genesisAddress)
import Morley.Michelson.Typed (IsoValue(..))
import Morley.Michelson.Untyped (pattern DefEpName)
import Morley.Tezos.Address
import Morley.Tezos.Address.Alias (AddressOrAlias(..))
import Morley.Tezos.Core (tz, zeroMutez)
import Morley.Tezos.Crypto
import Morley.Tezos.Crypto.Ed25519 qualified as Ed25519
import Test.Cleveland (NetworkEnv(neMorleyClientEnv), mkMorleyOnlyRpcEnvNetwork)
import Test.Cleveland.Internal.Abstract (Moneybag(..))
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Michelson.Import (importContract)
import Test.Cleveland.Tasty (whenNetworkEnabled)

import Test.Util.Contracts (contractsDir, (</>))

test_FeeCalculation :: IO TestTree
test_FeeCalculation = pure $ whenNetworkEnabled $ \withEnv ->
  testGroup "Compare morley-client fee calulation with tezos-client"
    [ testCase "single transfer in a batch has the same fee" $ withEnv \env -> do
      compareTransferFeeCalculation env $ one $ trivialTransfer
    , testCase "multiple transfers in a batch have the same fee" $ withEnv \env -> do
      compareTransferFeeCalculation env $ NE.fromList $ replicate 5 trivialTransfer
    , testCase "origination has the same fee" $ withEnv \env -> do
      soBigContract <- importContract @(ToT ()) @(ToT ()) $ contractsDir </> "so_big.tz"
      compareOriginationFeeCalculation env $ OriginationData
        { odAliasBehavior = OverwriteDuplicateAlias
        , odName = "so_big"
        , odBalance = zeroMutez
        , odContract = soBigContract
        , odStorage = toVal ()
        , odMbFee = Nothing
        , odDelegate = Nothing
        }
    , testCase "reveal has the same fee" $ withEnv \env -> do
      compareRevealFeeCalculation env
    , testCase "call to non-implicit contract has the same fee" $ withEnv \env -> do
      compareContractCallFeeCalculation env
    ]
  where
    trivialTransfer = TransactionData $ TD
      { tdReceiver = Constrained genesisAddress
      , tdAmount = 100
      , tdEpName = DefEpName
      , tdParam = toVal ()
      , tdMbFee = Nothing
      }

compareTransferFeeCalculation
  :: NetworkEnv -> NonEmpty TransactionData -> Assertion
compareTransferFeeCalculation env transferBatch = do
  Moneybag moneybagAddr <- setupMoneybagAddress env
  runMorleyClientM (neMorleyClientEnv env) $ revealKeyUnlessRevealed moneybagAddr Nothing
  (appliedResults, feesMorleyClient) <- fmap (unzip . toList) $ runMorleyClientM (neMorleyClientEnv env) $
    dryRunOperationsNonEmpty (AddressResolved moneybagAddr) (map OpTransfer transferBatch)
  pp <- runMorleyClientM (neMorleyClientEnv env) getProtocolParameters
  feesTezosClient <- runMorleyClientM (neMorleyClientEnv env) $ calcTransferFee
    (AddressResolved moneybagAddr) Nothing (computeStorageLimit appliedResults pp)
    (map transactionDataToCalcTransferFeeData $ toList transferBatch)
  feesMorleyClient @?= feesTezosClient
  where
    transactionDataToCalcTransferFeeData :: TransactionData -> CalcTransferFeeData
    transactionDataToCalcTransferFeeData
      (TransactionData TD{tdReceiver = Constrained tdReceiver, ..})
      = CalcTransferFeeData
          { ctfdTo = AddressResolved $ tdReceiver
          , ctfdParam = tdParam
          , ctfdEp = tdEpName
          , ctfdAmount = TezosMutez tdAmount
          }

compareOriginationFeeCalculation
  :: NetworkEnv -> OriginationData -> Assertion
compareOriginationFeeCalculation env od@OriginationData{..} = do
  Moneybag moneybagAddr <- setupMoneybagAddress env
  runMorleyClientM (neMorleyClientEnv env) $ revealKeyUnlessRevealed moneybagAddr Nothing
  (appliedResults, feesMorleyClient) <- fmap (unzip . toList) $ runMorleyClientM (neMorleyClientEnv env) $
    dryRunOperationsNonEmpty (AddressResolved moneybagAddr) (one $ OpOriginate od)
  pp <- runMorleyClientM (neMorleyClientEnv env) getProtocolParameters
  feeTezosClient <- runMorleyClientM (neMorleyClientEnv env) $ calcOriginationFee
    CalcOriginationFeeData
      { cofdFrom = AddressResolved moneybagAddr
      , cofdBalance = TezosMutez odBalance
      , cofdMbFromPassword = Nothing
      , cofdContract = odContract
      , cofdStorage = odStorage
      , cofdBurnCap = computeStorageLimit appliedResults pp
      }
  feesMorleyClient @?= [feeTezosClient]

compareRevealFeeCalculation
  :: NetworkEnv -> Assertion
compareRevealFeeCalculation env = do
  sk <- SecretKeyEd25519 <$> liftIO Ed25519.randomSecretKey
  let pub = toPublic sk
  let addr = mkKeyAddress pub

  alias <- runMorleyClientM (neMorleyClientEnv env) do
    alias <- importKey True "rpc-revealed-key" sk
    Moneybag moneybag <- liftIO $ setupMoneybagAddress env
    void $ transfer moneybag addr [tz|1 milli|] DefEpName (toVal ()) Nothing
    return alias

  (appliedResults, feesRPC) :| [] <- runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [sk]) do
    let rd = RevealData{ rdPublicKey = pub, rdMbFee = Nothing }
    dryRunOperationsNonEmpty (AddressResolved addr) (one $ OpReveal rd)

  pp <- runMorleyClientM (neMorleyClientEnv env) getProtocolParameters
  feesTezosClient <- runMorleyClientM (neMorleyClientEnv env) $ calcRevealFee
    alias Nothing (computeStorageLimit [appliedResults] pp)

  feesRPC @?= feesTezosClient

compareContractCallFeeCalculation
  :: NetworkEnv -> Assertion
compareContractCallFeeCalculation env = do
  Moneybag moneybagAddr <- setupMoneybagAddress env
  contractAddr <- runMorleyClientM (neMorleyClientEnv env) $ do
    add1Contract <- liftIO $ importContract @(ToT Integer) @(ToT Integer) $

      contractsDir </> "tezos_examples" </> "attic" </> "add1.tz"
    snd <$> originateContract OverwriteDuplicateAlias "add1"
      (AddressResolved moneybagAddr)
      zeroMutez add1Contract (toVal @Integer 1) Nothing Nothing
  compareTransferFeeCalculation env $ one $ TransactionData $ TD
      { tdReceiver = Constrained contractAddr
      , tdAmount = 0
      , tdEpName = DefEpName
      , tdParam = toVal @Integer 1
      , tdMbFee = Nothing
      }
