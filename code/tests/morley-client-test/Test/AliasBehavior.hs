-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.AliasBehavior
  ( test_originateContract
  , test_rememberContract
  ) where

import Debug qualified
import Lorentz qualified as L

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (assertFailure, testCase, (@?=))

import Morley.Client
import Morley.Tezos.Address.Alias (AddressOrAlias(..), Alias(..))
import Test.Cleveland.Internal.Abstract (Moneybag(..), neMorleyClientEnv)
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Tasty (whenNetworkEnabled)

test_originateContract :: TestTree
test_originateContract = whenNetworkEnabled $ \withEnv ->
  testCase "originateContract" $ withEnv \env -> do
    Moneybag moneybagAddr <- setupMoneybagAddress env
    let doOriginate b = lOriginateContract b alias (AddressResolved moneybagAddr) L.zeroMutez
          ctr () Nothing Nothing
    runMorleyClientM (neMorleyClientEnv env) do
      (_, addr1) <- doOriginate DontSaveAlias
      alias1 <- getAliasMaybe (AddressResolved addr1)
      liftIO $ alias1 @?= Nothing

      (_, addr2) <- doOriginate KeepDuplicateAlias
      alias2 <- getAliasMaybe (AddressResolved addr2)
      liftIO $ alias2 @?= Just alias

      (_, addr3) <- doOriginate OverwriteDuplicateAlias
      alias3 <- getAliasMaybe (AddressResolved addr3)
      liftIO $ alias3 @?= Just alias

      (_, addr4) <- doOriginate KeepDuplicateAlias
      alias4 <- getAliasMaybe (AddressResolved addr4)
      liftIO $ alias4 @?= Nothing
      addr3' <- resolveAddressMaybe (AddressAlias alias)
      liftIO $ addr3' @?= Just addr3

      void (doOriginate ForbidDuplicateAlias)
        `catch` \case
          DuplicateAlias "some-originated-contract" -> pass
          x -> liftIO $ assertFailure $
            "Expected DuplicateAlias \"some-originated-contract\", but got " <> Debug.show x
    where
      ctr :: L.Contract () () ()
      ctr = L.defaultContract $ L.drop L.# L.unit L.# L.nil @L.Operation L.# L.pair
      alias = ContractAlias "some-originated-contract"

test_rememberContract :: TestTree
test_rememberContract = whenNetworkEnabled $ \withEnv ->
  testCase "rememberContract" $ withEnv \env -> do
    Moneybag moneybagAddr <- setupMoneybagAddress env
    runMorleyClientM (neMorleyClientEnv env) do
      (_, addr) <- lOriginateContract DontSaveAlias
        alias (AddressResolved moneybagAddr) L.zeroMutez
        ctr () Nothing Nothing

      rememberContract DontSaveAlias addr alias
      alias1 <- getAliasMaybe (AddressResolved addr)
      liftIO $ alias1 @?= Nothing

      rememberContract KeepDuplicateAlias addr alias
      alias2 <- getAliasMaybe (AddressResolved addr)
      liftIO $ alias2 @?= Just alias

      (_, addr2) <- lOriginateContract DontSaveAlias
        alias (AddressResolved moneybagAddr) L.zeroMutez
        ctr () Nothing Nothing

      rememberContract OverwriteDuplicateAlias addr2 alias
      alias3 <- getAliasMaybe (AddressResolved addr2)
      liftIO $ alias3 @?= Just alias

      rememberContract KeepDuplicateAlias addr alias
      alias4 <- getAliasMaybe (AddressResolved addr)
      liftIO $ alias4 @?= Nothing
      addr3' <- resolveAddressMaybe (AddressAlias alias)
      liftIO $ addr3' @?= Just addr2

      void (rememberContract ForbidDuplicateAlias addr alias)
        `catch` \case
          DuplicateAlias "some-remembered-contract" -> pass
          x -> liftIO $ assertFailure $
            "Expected DuplicateAlias \"some-remembered-contract\", but got " <> Debug.show x
    where
      ctr :: L.Contract () () ()
      ctr = L.defaultContract $ L.drop L.# L.unit L.# L.nil @L.Operation L.# L.pair
      alias = ContractAlias "some-remembered-contract"
