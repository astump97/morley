-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.AddressOrAlias.ResolveAlias
  ( test_parse_and_resolve_ContractAddressOrAlias
  , test_parse_and_resolve_ImplicitAddressOrAlias
  , test_parse_and_resolve_SomeAddressOrAlias
  ) where

import Fmt (Buildable, pretty)
import Options.Applicative qualified as Opt
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (assertFailure, testCase, (@?=))

import Lorentz ((#))
import Lorentz qualified as L
import Morley.CLI (addressOrAliasOption, someAddressOrAliasOption)
import Morley.Client
import Morley.Tezos.Address
import Morley.Tezos.Address.Alias
import Morley.Tezos.Address.Kinds (AddressKind(..))
import Morley.Util.Interpolate
import Morley.Util.Named
import Test.Cleveland (NetworkEnv(..))
import Test.Cleveland.Internal.Abstract qualified as Internal
import Test.Cleveland.Internal.Client qualified as Internal
import Test.Cleveland.Tasty (whenNetworkEnabled)

txr1Address :: KindedAddress 'AddressKindTxRollup
txr1Address = [ta|txr1YNMEtkj5Vkqsbdmt7xaxBTMRZjzS96UAi|]

mkParser
  :: HasCallStack
  => Opt.Parser a
  -> String
  -> Either String a
mkParser optionParser input =
  case Opt.execParserPure (Opt.prefs mempty) (Opt.info optionParser mempty) ["--aoa", input] of
    Opt.Success aoa -> Right aoa
    Opt.Failure failure ->
      -- Mimic the behavior of `Opt.handleParseResult`, but without exiting.
      let (errorMsg, _exit) = Opt.renderFailure failure "program-name"
      in  Left errorMsg
    Opt.CompletionInvoked _ -> error "unexpected CompletionInvoked"

tryDisplay :: MonadCatch m => m a -> m (Either String a)
tryDisplay action =
  first displayException <$> tryAny action

someOption :: Opt.Parser SomeAddressOrAlias
someOption =
  someAddressOrAliasOption
    Nothing
    (#name :! "aoa")
    (#help :! "Help text.")

contractOption :: Opt.Parser ContractAddressOrAlias
contractOption =
  addressOrAliasOption
    Nothing
    (#name :! "aoa")
    (#help :! "Help text.")

implicitOption :: Opt.Parser ImplicitAddressOrAlias
implicitOption =
  addressOrAliasOption
    Nothing
    (#name :! "aoa")
    (#help :! "Help text.")

contractAlias, contractAmbiguousAlias :: ContractAlias
implicitAlias, implicitAmbiguousAlias :: ImplicitAlias
contractAlias = "contract-alias"
implicitAlias = "implicit-alias"
implicitAmbiguousAlias = "ambiguous-alias"
contractAmbiguousAlias = "ambiguous-alias"

test_parse_and_resolve_ContractAddressOrAlias :: TestTree
test_parse_and_resolve_ContractAddressOrAlias =
  whenNetworkEnabled \withEnv -> do
    testCase "parse and resolve ContractAddressOrAlias" do
      withEnv \env -> do
        runMorleyClientM (neMorleyClientEnv env) do
          (implicitAddr, contractAddr, _, _) <- setup env

          -- Parse and resolve addresses
          pretty contractAddr `shouldSucceed` contractAlias
          pretty implicitAddr `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: '#{implicitAddr}'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]
          pretty txr1Address `parserShouldFail`
            [itu|
            option --aoa: Unexpected transaction rollup address: #{txr1Address}

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]

          -- Parse and resolve aliases
          --   * without prefix
          "implicit-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'implicit-alias' to be assigned to an address of kind 'contract',
            but it's assigned to an address of kind 'implicit': #{implicitAddr}.
            |]
          "contract-alias" `shouldSucceed` contractAlias

          --   * with prefix
          "implicit:implicit-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: 'implicit:implicit-alias'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]
          "contract:contract-alias" `shouldSucceed` contractAlias

          --   * with the wrong prefix
          "contract:implicit-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'implicit-alias' to be assigned to an address of kind 'contract',
            but it's assigned to an address of kind 'implicit': #{implicitAddr}.
            |]
          "implicit:contract-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: 'implicit:contract-alias'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]

          -- Parse and resolve ambiguous aliases
          --   * without prefix
          "ambiguous-alias" `shouldSucceed` contractAmbiguousAlias

          --   * with prefix
          "implicit:ambiguous-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: 'implicit:ambiguous-alias'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]
          "contract:ambiguous-alias" `shouldSucceed` contractAmbiguousAlias

          -- Parse and resolve invalid aliases
          --   * without prefix
          "invalid-alias" `resolveShouldThrow`
            [itu|Could not find the alias 'contract:invalid-alias'.|]

          --   * with prefix
          "implicit:invalid-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected contract address or alias, but got: 'implicit:invalid-alias'

            Usage: program-name --aoa CONTRACT ADDRESS OR ALIAS
            |]
          "contract:invalid-alias" `resolveShouldThrow`
            [itu|Could not find the alias 'contract:invalid-alias'.|]

          -- Parse and resolve invalid addresses (addresses for which there isn't an alias)
          "KT1BRd2ka5q2cPRdXALtXD1QZ38CPam2j1ye" `resolveShouldThrow`
            [itu|Could not find an alias for the address 'KT1BRd2ka5q2cPRdXALtXD1QZ38CPam2j1ye'.|]
  where
    shouldSucceed :: HasCallStack => String -> ContractAlias -> MorleyClientM ()
    shouldSucceed = mkShouldSucceed contractOption

    parserShouldFail :: HasCallStack => String -> String -> MorleyClientM ()
    parserShouldFail = mkParserShouldFail contractOption

    resolveShouldThrow :: HasCallStack => String -> String -> MorleyClientM ()
    resolveShouldThrow = mkResolveShouldThrow contractOption

test_parse_and_resolve_ImplicitAddressOrAlias :: TestTree
test_parse_and_resolve_ImplicitAddressOrAlias =
  whenNetworkEnabled \withEnv -> do
    testCase "parse and resolve ImplicitAddressOrAlias" do
      withEnv \env -> do
        runMorleyClientM (neMorleyClientEnv env) do
          (implicitAddr, contractAddr, _, _) <- setup env

          -- Parse and resolve addresses
          pretty implicitAddr `shouldSucceed` implicitAlias
          pretty contractAddr `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: '#{contractAddr}'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]
          pretty txr1Address `parserShouldFail`
            [itu|
            option --aoa: Unexpected transaction rollup address: #{txr1Address}

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]

          -- Parse and resolve aliases
          --   * without prefix
          "implicit-alias" `shouldSucceed` implicitAlias
          "contract-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'contract-alias' to be assigned to an address of kind 'implicit',
            but it's assigned to an address of kind 'contract': #{contractAddr}.
            |]

          --   * with prefix
          "implicit:implicit-alias" `shouldSucceed` implicitAlias
          "contract:contract-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: 'contract:contract-alias'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]

          --   * with the wrong prefix
          "contract:implicit-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: 'contract:implicit-alias'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]
          "implicit:contract-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'contract-alias' to be assigned to an address of kind 'implicit',
            but it's assigned to an address of kind 'contract': #{contractAddr}.
            |]

          -- Parse and resolve ambiguous aliases
          --   * without prefix
          "ambiguous-alias" `shouldSucceed` implicitAmbiguousAlias

          --   * with prefix
          "implicit:ambiguous-alias" `shouldSucceed` implicitAmbiguousAlias
          "contract:ambiguous-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: 'contract:ambiguous-alias'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]

          -- Parse and resolve invalid aliases
          --   * without prefix
          "invalid-alias" `resolveShouldThrow`
            [itu|Could not find the alias 'implicit:invalid-alias'.|]

          --   * with prefix
          "implicit:invalid-alias" `resolveShouldThrow`
            [itu|Could not find the alias 'implicit:invalid-alias'.|]
          "contract:invalid-alias" `parserShouldFail`
            [itu|
            option --aoa: Unexpected address kind: expected implicit address or alias, but got: 'contract:invalid-alias'

            Usage: program-name --aoa IMPLICIT ADDRESS OR ALIAS
            |]

          -- Parse and resolve invalid addresses (addresses for which there isn't an alias)
          "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU" `resolveShouldThrow`
            [itu|Could not find an alias for the address 'tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU'.|]
  where
    shouldSucceed :: HasCallStack => String -> ImplicitAlias -> MorleyClientM ()
    shouldSucceed = mkShouldSucceed implicitOption

    parserShouldFail :: HasCallStack => String -> String -> MorleyClientM ()
    parserShouldFail = mkParserShouldFail implicitOption

    resolveShouldThrow :: HasCallStack => String -> String -> MorleyClientM ()
    resolveShouldThrow = mkResolveShouldThrow implicitOption

test_parse_and_resolve_SomeAddressOrAlias :: TestTree
test_parse_and_resolve_SomeAddressOrAlias =
  whenNetworkEnabled \withEnv -> do
    testCase "parse and resolve SomeAddressOrAlias" do
      withEnv \env -> do
        runMorleyClientM (neMorleyClientEnv env) do
          (implicitAddr, contractAddr, implicitAmbiguousAddr, contractAmbiguousAddr) <- setup env

          -- Parse and resolve addresses
          pretty implicitAddr `shouldSucceed` SomeAlias implicitAlias
          pretty contractAddr `shouldSucceed` SomeAlias contractAlias
          pretty txr1Address `parserShouldFail`
            [itu|
            option --aoa: Unexpected transaction rollup address: #{txr1Address}

            Usage: program-name --aoa CONTRACT OR IMPLICIT ADDRESS OR ALIAS
            |]

          -- Parse and resolve aliases
          --   * without prefix
          "implicit-alias" `shouldSucceed` SomeAlias implicitAlias
          "contract-alias" `shouldSucceed` SomeAlias contractAlias

          --   * with prefix
          "implicit:implicit-alias" `shouldSucceed` SomeAlias implicitAlias
          "contract:contract-alias" `shouldSucceed` SomeAlias contractAlias

          --   * with the wrong prefix
          "contract:implicit-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'implicit-alias' to be assigned to an address of kind 'contract',
            but it's assigned to an address of kind 'implicit': #{implicitAddr}.
            |]
          "implicit:contract-alias" `resolveShouldThrow`
            [itu|
            Expected the alias 'contract-alias' to be assigned to an address of kind 'implicit',
            but it's assigned to an address of kind 'contract': #{contractAddr}.
            |]

          -- Parse and resolve ambiguous aliases
          --   * without prefix
          "ambiguous-alias" `resolveBothShouldThrow`
            [itu|
            The alias 'ambiguous-alias' is assigned to both:
              * a contract address: #{contractAmbiguousAddr}
              * and an implicit address: #{implicitAmbiguousAddr}
            Use 'contract:ambiguous-alias' or 'implicit:ambiguous-alias' to disambiguate.
            |]

          --   * with prefix
          "implicit:ambiguous-alias" `shouldSucceed` SomeAlias implicitAmbiguousAlias
          "contract:ambiguous-alias" `shouldSucceed` SomeAlias contractAmbiguousAlias

          -- Parse and resolve invalid aliases
          --   * without prefix
          "invalid-alias" `resolveShouldThrow` [itu|Could not find the alias 'invalid-alias'.|]

          --   * with prefix
          "implicit:invalid-alias" `resolveShouldThrow` [itu|Could not find the alias 'implicit:invalid-alias'.|]
          "contract:invalid-alias" `resolveShouldThrow` [itu|Could not find the alias 'contract:invalid-alias'.|]

          -- Parse and resolve invalid addresses (addresses for which there isn't an alias)
          "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU" `resolveShouldThrow`
            [itu|Could not find an alias for the address 'tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU'.|]
          "KT1BRd2ka5q2cPRdXALtXD1QZ38CPam2j1ye" `resolveShouldThrow`
            [itu|Could not find an alias for the address 'KT1BRd2ka5q2cPRdXALtXD1QZ38CPam2j1ye'.|]
  where
    shouldSucceed :: HasCallStack => String -> SomeAlias -> MorleyClientM ()
    shouldSucceed = mkShouldSucceed someOption

    parserShouldFail :: HasCallStack => String -> String -> MorleyClientM ()
    parserShouldFail = mkParserShouldFail someOption

    resolveShouldThrow :: HasCallStack => String -> String -> MorleyClientM ()
    resolveShouldThrow = mkResolveShouldThrow someOption

    resolveBothShouldThrow :: HasCallStack => String -> String -> MorleyClientM ()
    resolveBothShouldThrow = mkResolveBothShouldThrow someOption

mkShouldSucceed
  :: (HasCallStack, Resolve addressOrAlias, Eq (ResolvedAlias addressOrAlias), Show (ResolvedAlias addressOrAlias))
  => Opt.Parser addressOrAlias
  -> String
  -> ResolvedAlias addressOrAlias
  -> MorleyClientM ()
mkShouldSucceed optionParser input expectedAddr = do
  case mkParser optionParser input of
    Left err -> liftIO $ assertFailure [itu|Expected parser to succeed, but it failed with: #{err} |]
    Right aoa -> do
      result1 <- tryDisplay $ getAliasMaybe aoa
      liftIO $ result1 @?= Right (Just expectedAddr)

      result2 <- tryDisplay $ getAlias aoa
      liftIO $ result2 @?= Right expectedAddr

-- | Expect the address/alias parser to fail.
mkParserShouldFail
  :: (HasCallStack, Buildable addressOrAlias)
  => Opt.Parser addressOrAlias
  -> String
  -> String
  -> MorleyClientM ()
mkParserShouldFail optionParser input expectedErrorMsg =
  liftIO $
    case mkParser optionParser input of
      Right aoa -> assertFailure [itu|Expected parser to fail, but it succeeded with: #{aoa}|]
      Left err -> err @?= expectedErrorMsg

-- | Expect `getAlias` to throw, and `getAliasMaybe` to return `Nothing`.
mkResolveShouldThrow
  :: (HasCallStack, Resolve addressOrAlias, Eq (ResolvedAlias addressOrAlias), Show (ResolvedAlias addressOrAlias))
  => Opt.Parser addressOrAlias
  -> String
  -> String
  -> MorleyClientM ()
mkResolveShouldThrow optionParser input expectedErrorMsg =
  case mkParser optionParser input of
    Left err -> liftIO $ assertFailure [itu|Expected parser to succeed, but it failed with: #{err} |]
    Right aoa -> do
      result1 <- tryDisplay $ getAliasMaybe aoa
      liftIO $ result1 @?= Right Nothing

      result2 <- tryDisplay $ getAlias aoa
      liftIO $ result2 @?= Left expectedErrorMsg

-- | Expect both `getAlias` and `getAliasMaybe` to throw.
mkResolveBothShouldThrow
  :: (HasCallStack, Resolve addressOrAlias, Eq (ResolvedAlias addressOrAlias), Show (ResolvedAlias addressOrAlias))
  => Opt.Parser addressOrAlias
  -> String
  -> String
  -> MorleyClientM ()
mkResolveBothShouldThrow optionParser input expectedErrorMsg =
  case mkParser optionParser input of
    Left err -> liftIO $ assertFailure [itu|Expected parser to succeed, but it failed with: #{err} |]
    Right aoa -> do
      result1 <- tryDisplay $ getAliasMaybe aoa
      liftIO $ result1 @?= Left expectedErrorMsg

      result2 <- tryDisplay $ getAlias aoa
      liftIO $ result2 @?= Left expectedErrorMsg

-- | Creates 3 aliases:
--
-- * `implicit-alias`, associated with an implicit address.
-- * `contract-alias`, associated with an contract address.
-- * `ambiguous-alias`, associated with both an implicit address and a contract address.
setup :: NetworkEnv -> MorleyClientM (ImplicitAddress, ContractAddress, ImplicitAddress, ContractAddress)
setup env = do
  Internal.Moneybag moneyBag <- liftIO $ Internal.setupMoneybagAddress env

  implicitAddr <- genKey "implicit-alias"
  (_, contractAddr) <- originateContract
    OverwriteDuplicateAlias
    "contract-alias"
    (AddressResolved moneyBag)
    0
    (L.toMichelsonContract $ idContract @() @())
    (L.toVal ())
    Nothing
    Nothing

  implicitAmbiguousAddr <- genKey "ambiguous-alias"
  (_, contractAmbiguousAddr) <- originateContract
    OverwriteDuplicateAlias
    "ambiguous-alias"
    (AddressResolved moneyBag)
    0
    (L.toMichelsonContract $ idContract @() @())
    (L.toVal ())
    Nothing
    Nothing

  pure (implicitAddr, contractAddr, implicitAmbiguousAddr, contractAmbiguousAddr)
  where
    -- Simple contract that does nothing when called.
    idContract :: (L.NiceParameterFull cp, L.NiceStorageFull st) => L.Contract cp st ()
    idContract = L.defaultContract $
      L.cdr #
      L.nil @L.Operation #
      L.pair
