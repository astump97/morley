-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests on key revealing performed via RPC.
module Test.RPCKeyRevealing
  ( test_rpcKeyRevealing
  ) where

import Crypto.Random (getRandomBytes)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Client.Action qualified as WithTezosClient
import Morley.Client.Action.Reveal
import Morley.Client.Full qualified as WithTezosClient
import Morley.Client.OnlyRPC
import Morley.Client.RPC.Getters as WithTezosClient
import Morley.Client.TezosClient.Impl qualified as WithTezosClientImpl
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Tezos.Address
import Morley.Tezos.Core
import Morley.Tezos.Crypto
import Test.Cleveland
import Test.Cleveland.Internal.Abstract (Moneybag(Moneybag))
import Test.Cleveland.Internal.Client (setupMoneybagAddress)
import Test.Cleveland.Tasty

test_rpcKeyRevealing :: TestTree
test_rpcKeyRevealing =
  whenNetworkEnabled $ \withEnv ->
  testGroup "Key revealing via RPC"
  [ testCase "Can reveal a new key" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env
      sk <- detSecretKey <$> liftIO (getRandomBytes 16)
      let pub = toPublic sk
      let addr = mkKeyAddress pub

      WithTezosClient.runMorleyClientM (neMorleyClientEnv env) $ do
        void $ WithTezosClientImpl.importKey True "rpc-revealed-key" sk
        void $ WithTezosClient.transfer moneybag addr [tz|1 milli|] U.DefEpName T.VUnit Nothing

      mManager <- runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [sk]) $ do
        void $ revealKey addr $ RevealData pub Nothing
        getManagerKey addr

      mManager @?= Just pub

  , testCase "revealKeyUnlessRevealed works" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env
      sk <- detSecretKey <$> liftIO (getRandomBytes 16)
      let pub = toPublic sk
      let addr = mkKeyAddress pub

      WithTezosClient.runMorleyClientM (neMorleyClientEnv env) $ do
        void $ WithTezosClientImpl.importKey True "rpc-revealed-key" sk
        void $ WithTezosClient.transfer moneybag addr [tz|1 milli|] U.DefEpName T.VUnit Nothing

      runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [sk]) $ do
        void . revealKey addr $
          RevealData pub Nothing
        void . revealKeyUnlessRevealed addr $
          RevealData pub Nothing
  ]
