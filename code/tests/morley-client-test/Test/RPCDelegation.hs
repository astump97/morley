-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Tests register delegate performed via RPC.
module Test.RPCDelegation
  ( test_rpcRegisterDelegate
  ) where

import Crypto.Random (getRandomBytes)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Client (getDelegate, runMorleyOnlyRpcM)
import Morley.Client.Action as WithMorleyClient
import Morley.Client.Action.Reveal
import Morley.Client.Full qualified as WithTezosClient
import Morley.Client.TezosClient.Impl qualified as WithTezosClientImpl
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Tezos.Address
import Morley.Tezos.Address.Alias
import Morley.Tezos.Core
import Morley.Tezos.Crypto
import Test.Cleveland hiding (getDelegate)
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Internal.Client
import Test.Cleveland.Tasty

test_rpcRegisterDelegate :: TestTree
test_rpcRegisterDelegate = whenNetworkEnabled $ \withEnv ->
  testGroup "Delegations via RPC"
  [ testCase "Can (self) register as delegate" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env
      sk <- detSecretKey <$> liftIO (getRandomBytes 16)
      let pub = toPublic sk
          addr@(ImplicitAddress keyHash) = mkKeyAddress pub

      WithTezosClient.runMorleyClientM (neMorleyClientEnv env) $ do
        void $ WithTezosClientImpl.importKey True "rpc-delegate-key" sk
        void $ WithMorleyClient.transfer moneybag addr [tz|1 milli|] U.DefEpName T.VUnit Nothing

      runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [sk]) $ do
        void $ revealKey addr $ RevealData pub Nothing
        void $ WithMorleyClient.registerDelegateOp keyHash
        delegate <- getDelegate (toL1Address addr)
        liftIO $ ImplicitAddress <$> delegate @?= Just addr

  , testCase "Can set/unset a delegate" $ withEnv \env -> do
      Moneybag moneybag <- setupMoneybagAddress env

      skAlice <- detSecretKey <$> liftIO (getRandomBytes 16)
      let pubAlice = toPublic skAlice
          addrAlice@(ImplicitAddress keyHashAlice) = mkKeyAddress pubAlice

      skBob <- detSecretKey <$> liftIO (getRandomBytes 16)
      let pubBob = toPublic skBob
          addrBob = mkKeyAddress pubBob

      WithTezosClient.runMorleyClientM (neMorleyClientEnv env) $ do
        void $ WithTezosClientImpl.importKey True "alice-key" skAlice
        void $ WithMorleyClient.transfer moneybag addrAlice [tz|1 milli|] U.DefEpName T.VUnit Nothing
        void $ WithTezosClientImpl.importKey True "bob-key" skBob
        void $ WithMorleyClient.transfer moneybag addrBob [tz|5 milli|] U.DefEpName T.VUnit Nothing

      runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [skAlice]) $ do
        void $ revealKey addrAlice $ RevealData pubAlice Nothing
        void $ WithMorleyClient.registerDelegateOp keyHashAlice

      runMorleyOnlyRpcM (mkMorleyOnlyRpcEnvNetwork env [skBob, skAlice]) $ do
        void $ revealKey addrBob $ RevealData pubBob Nothing
        void $ WithMorleyClient.setDelegateOp (AddressResolved addrBob) (Just keyHashAlice)
        bobsDelegate1 <- getDelegate (toL1Address addrBob)
        liftIO $ ImplicitAddress <$> bobsDelegate1 @?= Just addrAlice
        void $ WithMorleyClient.setDelegateOp (AddressResolved addrBob) Nothing
        bobsDelegate2 <- getDelegate (toL1Address addrBob)
        liftIO $ ImplicitAddress <$> bobsDelegate2 @?= Nothing
  ]
