-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Utility functions to read sample contracts (for testing).

module Test.Util.Contracts
  ( contractsDir
  , inContractsDir
  , (</>)

  , getIllTypedContracts
  , getWellTypedContracts
  , getUnparsableContracts
  , getWellTypedMichelsonContracts
  , getWellTypedMorleyContracts
  , getContractsWithReferences
  ) where

import Data.List (isSuffixOf)
import System.Directory (listDirectory)
import System.FilePath (addExtension, (</>))

-- | Directory with sample contracts.
contractsDir :: FilePath
contractsDir = "../../contracts/"

inContractsDir :: FilePath -> FilePath
inContractsDir = (contractsDir </>)

getIllTypedContracts :: IO [FilePath]
getIllTypedContracts = do
  illTyped <- concatMapM (\ext ->
                            concatMapM (getContractsWithExtension ext)
                            illTypedContractDirs
                         ) [".tz"]
  unparsable <- getUnparsableContracts
  return $ filter (not . (flip elem (unparsable <> unsupportedContracts))) illTyped

getWellTypedContracts :: IO [FilePath]
getWellTypedContracts = getWellTypedMichelsonContracts <> getWellTypedMorleyContracts

getUnparsableContracts :: IO [FilePath]
getUnparsableContracts = do
  unparsable <- concatMapM (flip getContractsWithExtension (contractsDir </> "unparsable"))
    [".tz"]
  return $ unparsable ++ unparsableContracts

getWellTypedMichelsonContracts :: IO [FilePath]
getWellTypedMichelsonContracts = do
  wellTyped <- concatMapM (getContractsWithExtension ".tz") wellTypedContractDirs
  return $ filter (not . flip elem unsupportedContracts) wellTyped

getWellTypedMorleyContracts :: IO [FilePath]
getWellTypedMorleyContracts = do
  wellTyped <- concatMapM (getContractsWithExtension ".tz") wellTypedContractDirs
  return $ filter (not . flip elem unsupportedContracts) wellTyped

getContractsWithExtension :: String -> FilePath -> IO [FilePath]
getContractsWithExtension ext dir = mapMaybe convertPath <$> listDirectory dir
  where
    convertPath :: FilePath -> Maybe FilePath
    convertPath fileName
      | (ext `isSuffixOf` fileName) =
        Just (dir </> fileName)
      | otherwise = Nothing

wellTypedContractDirs :: [FilePath]
wellTypedContractDirs =
  contractsDir :
  (contractsDir </> "annotation-mismatch") :
  map ((contractsDir </> "tezos_examples") </>)
    [ "attic"
    , "entrypoints"
    , "macros"
    , "mini_scenarios"
    , "non_regression"
    , "opcodes"
    ]

illTypedContractDirs :: [FilePath]
illTypedContractDirs =
  [ contractsDir </> "ill-typed"
  , contractsDir </> "tezos_examples" </> "ill_typed"
  , contractsDir </> "tezos_examples" </> "legacy"
  ]

unsupportedContracts :: [FilePath]
unsupportedContracts =
  [ contractsDir </> "tezos_examples" </> "ill_typed" </> name
  | name <-
      [ "badly_indented.tz" -- we are a little more permissive about indenttion that the reference.
      ]
  ] <>
  [ contractsDir </> "tezos_examples" </> "opcodes" </> name
  | name <-
      [ "comb_literals.tz" -- TODO [#832] fails the "print-parse is idempotent" test
      ]
  ] <>
  -- it's a contract with a `never` parameter, we cannot run it on-chain.
  [ contractsDir </> "tezos_examples" </> "non_regression" </> "bug_843.tz"]

unparsableContracts :: [FilePath]
unparsableContracts =
  [ contractsDir </> "tezos_examples" </> "ill_typed" </> name
  | name <-
      [ "big_map_arity.tz"
      , "chain_id_arity.tz"
      , "create_contract_rootname.tz"
      , "missing_only_code_field.tz"
      , "missing_only_parameter_field.tz"
      , "missing_only_storage_field.tz"
      , "missing_parameter_and_storage_fields.tz"
      , "multiple_code_field.tz"
      , "multiple_parameter_field.tz"
      , "multiple_storage_and_code_fields.tz"
      , "multiple_storage_field.tz"
      , "sapling_build_empty_state_with_int_parameter.tz"
      , "view_op_invalid_arity.tz"
      , "view_toplevel_invalid_arity.tz"
      ]
      ++
      [ "view_" <> kind <> "_bad_name_" <> name <> ".tz"
      | kind <- [ "op", "toplevel" ]
      , name <- [ "invalid_type", "invalid_char_set", "non_printable_char"
                , "too_long"
                ]
      ]
  ] ++
  [ contractsDir </> "tezos_examples" </> "legacy" </> name
  | name <-
      [ "create_account.tz"
      , "originator.tz"
      , "steps_to_quota.tz"
      ]
  ]

getContractsWithReferences :: String -> FilePath -> String -> IO [(FilePath, FilePath)]
getContractsWithReferences ext fp refExt =
  fmap attachPrettyPath <$> getContractsWithExtension ext fp
  where
    attachPrettyPath :: FilePath -> (FilePath, FilePath)
    attachPrettyPath src = (src, addExtension src  refExt)
