-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Helpers for HUnit.

module Test.Util.HUnit
  ( ShowThroughBuild(..)
  , assertEqualBuild
  , genShowThroughBuild
  ) where

import Fmt (Buildable, pretty)
import Text.Show qualified (show)

import Hedgehog (Gen)
import Test.HUnit (Assertion, assertEqual)

----------------------------------------------------------------------------
-- 'Show'ing a value though 'Buildable' type class.
-- Useful because Hedgehog and HUnit use 'Show'.
----------------------------------------------------------------------------

newtype ShowThroughBuild a = STB
  { unSTB :: a
  } deriving newtype (Eq, Ord)

instance Buildable a => Show (ShowThroughBuild a) where
  show = pretty . unSTB


assertEqualBuild
  :: (HasCallStack, Eq a, Show (ShowThroughBuild a))
  => String -> a -> a -> Assertion
assertEqualBuild desc a b =
  assertEqual desc (STB a) (STB b)

genShowThroughBuild :: Gen a -> Gen (ShowThroughBuild a)
genShowThroughBuild genA = STB <$> genA
