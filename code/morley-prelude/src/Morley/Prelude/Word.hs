-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE NoImplicitPrelude #-}

module Morley.Prelude.Word
  ( module Data.Word.Odd
  )
  where

import Data.IntCast (IntBaseType, IntBaseTypeK(..))
import Data.Word.Odd (Word62, Word63)

type instance IntBaseType Word63 = 'FixedWordTag 63
type instance IntBaseType Word62 = 'FixedWordTag 62
