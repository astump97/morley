<!-- Unreleased: append new entries here -->

* [!1266](https://gitlab.com/morley-framework/morley/-/merge_requests/1266)
  Overload `getAlias`
  + Overloaded `getAlias` and `getAliasMaybe`
    to work with `SomeAddressOrAlias`.
  + `getAlias` and `getAliasMaybe` now check that the alias actually exists.
  + Replaced the `findAddress` and `getAliasEither` methods from
    the `HasTezosClient` typeclass with `getAliasesAndAddresses`.
* [!1259](https://gitlab.com/morley-framework/morley/-/merge_requests/1259)
  More options for handling duplicate alias in morley-client
  + Added new datatype `Morley.Client.AliasBehavior`.
  + Various origination functions and datatypes now use this datatype instead of
    a boolean flag.
  + `DuplicateAlias` error is now part of `TezosClientError`.
* [!1258](https://gitlab.com/morley-framework/morley/-/merge_requests/1258)
  Support implicit contract delegates and setting delegates during origination
  + Fix a small potential bug in RPC error code decoders.
  + Parse `deleagate.already_active` error
  + Support getting delegates for implicit addresses
  + Support setting contract delegate during origination
* [!1260](https://gitlab.com/morley-framework/morley/-/merge_requests/1260)
  Add `SomeAddressOrAlias`
  + Overloaded `resolveAddress` and `resolveAddressMaybe` to work with
    `SomeAddressOrAlias`.
  + Replaced the `transfer --to-implicit` and `--to-contract` options with a
    single `--to` option.
  + Replaced the `resolveAddressMaybe` method from the `HasTezosClient`
    typeclass with `findAddress`.
* [!1226](https://gitlab.com/morley-framework/morley/-/merge_requests/1226)
  Implement delegation operation via RPC
* [!1257](https://gitlab.com/morley-framework/morley/-/merge_requests/1257)
  Export `Morley.Client.Action.Common.runErrorsToClientError` utility for
  converting `[RunError]` to `ClientRpcError`
* [!1244](https://gitlab.com/morley-framework/morley/-/merge_requests/1244)
  Smarter initial gas limit estimation
* [!1249](https://gitlab.com/morley-framework/morley/-/merge_requests/1249)
  Add getAliasMaybe and getAliasEither to morley-client
* [!1227](https://gitlab.com/morley-framework/morley/-/merge_requests/1227)
  Add timestamp to morley-client logs
  + Omit source locations from logs when verbosity is `<=2`.
* [!1242](https://gitlab.com/morley-framework/morley/-/merge_requests/1242)
  Use `Constrained` utility existential
* [!1247](https://gitlab.com/morley-framework/morley/-/merge_requests/1247)
  Refactor MorleyClientEnv
  + Removed `MorleyClientEnv'`, constructor now lives in `MorleyClientEnv`
  + Removed `Morley.Client.Env` module
  + `MorleyClientEnv`, `mkMorleyClientEnv` and `MorleyClientEnv` lenses moved to `Morley.Client.Full`
* [!1237](https://gitlab.com/morley-framework/morley/-/merge_requests/1237)
  Handle unregistered_delegate error in morley-client
* [!1215](https://gitlab.com/morley-framework/morley/-/merge_requests/1215)
  Output contract events in morley-client binary

0.2.1
=====
* [!1199](https://gitlab.com/morley-framework/morley/-/merge_requests/1199)
  Remove HasTezosClient getTezosClientConfig, importKey, get[Public/Secret]Key
* [!1212](https://gitlab.com/morley-framework/morley/-/merge_requests/1212)
  Get contract events
  + Operation result handlers now also return internal operation data.
  + Injecting the operation fetches contract events from internal operation
    data (received from preapply).
  + `OpTransfer` now has a list of contract events as the result.
* [!1210](https://gitlab.com/morley-framework/morley/-/merge_requests/1210)
  Use `consumed_milligas` instead of `consumed_gas`, as the latter is removed in Kathmandu protocol.
* [!1201](https://gitlab.com/morley-framework/morley/-/merge_requests/1201)
  Enforce reveal operation to use key aliases when doing `tezos-client reveal`
* [!1177](https://gitlab.com/morley-framework/morley/-/merge_requests/1177)
  Distinguish implicit/contract aliases and addresses on the type level
* [!1183](https://gitlab.com/morley-framework/morley/-/merge_requests/1183)
  Avoid origination when an alias already exists

0.2.0
=====
* [!1161](https://gitlab.com/morley-framework/morley/-/merge_requests/1161)
  Remove support for `AliasHint`
* [!1164](https://gitlab.com/morley-framework/morley/-/merge_requests/1164)
  Add `now` and `level` params to the `/run_code`
* [!1150](https://gitlab.com/morley-framework/morley/-/merge_requests/1150)
  Implement wait-for-operation via RPC
  + Block injection is now checked for errors and injection is retried.
* [!1159](https://gitlab.com/morley-framework/morley/-/merge_requests/1159)
  Add script size calculation to morley-client
* [!1088](https://gitlab.com/morley-framework/morley/-/merge_requests/1088)
  Move data types related to address aliases to `morley`.
* [!1155](https://gitlab.com/morley-framework/morley/-/merge_requests/1155)
  Update operations limits estimation to match the v13.0 `tezos-client` implementation.
* [!1147](https://gitlab.com/morley-framework/morley/-/merge_requests/1147)
  Fix error handling in morley-client
  + `handleOperationResult` is now exported from `Morley.Client.Action.Common`
* [!1114](https://gitlab.com/morley-framework/morley/-/merge_requests/1114)
  Update to ghc-9.0.2
* [!1108](https://gitlab.com/morley-framework/morley/-/merge_requests/1108)
  Remove support for the deprecated morley extensions
* [!1133](https://gitlab.com/morley-framework/morley/-/merge_requests/1133)
  Add missing fields to `TransactionOpResp` constructor.
* [!1127](https://gitlab.com/morley-framework/morley/-/merge_requests/1127)
  Add method to get secret key from tezos-client
* [!1140](https://gitlab.com/morley-framework/morley/-/merge_requests/1140)
  Derive `newtype` `Eq`, `Ord`, `Show`, `Buildable` instances for `BlockHash`

0.1.2
=====
* [!1017](https://gitlab.com/morley-framework/morley/-/merge_requests/1017)
  Resolve some TODOs and link TODOs without issue id to the corresponding gitlab tickets.
* [!1082](https://gitlab.com/morley-framework/morley/-/merge_requests/1082)
  Fix/drop/comment noncanonical Show instances

0.1.1
=====
* [!1094](https://gitlab.com/morley-framework/morley/-/merge_requests/1094)
  Deprecate morley language extensions
  + Morley language extensions now require `--deprecated-morley-extensions` flag to parse.
* [!1077](https://gitlab.com/morley-framework/morley/-/merge_requests/1077)
  Ithaca changes: Use `head~2` block in the `branch` field of RPC operations.
* [!1034](https://gitlab.com/morley-framework/morley/-/merge_requests/1034)
  Add key revealing that uses only RPC.
* [!965](https://gitlab.com/morley-framework/morley/-/merge_requests/965)
  Use Morley's fixed-size lists
  + Use `SizedList` for `feeOutputParser`
* [!1072](https://gitlab.com/morley-framework/morley/-/merge_requests/1072)
  Add `runCode` to Cleveland
  + `runContract` now supports parameter/storage values in their RPC
    representation (i.e. with bigmap IDs).
* [!1070](https://gitlab.com/morley-framework/morley/-/merge_requests/1070)
  Simplify cleveland's internals & public api
  + Removed `runContractSimple`, added `runContractParameters` and lenses.
* [!1060](https://gitlab.com/morley-framework/morley/-/merge_requests/1060)
  Move `AsRPC` type family to `morley`
* [!978](https://gitlab.com/morley-framework/morley/-/merge_requests/978)
  Make it difficult to misuse 'Show'
  + Use `Buildable` and `pretty` preferentially.
  + Add `Buildable` instances to that effect for `FeeParserException`, `SecretKeyEncryptionParserException`.
  + Use `displayException` instead of `show` where appropriate.

0.1.0
=====
Initial release.
A client to interact with the Tezos blockchain, by use of the `tezos-node` RPC
and/or of the `tezos-client` binary.
