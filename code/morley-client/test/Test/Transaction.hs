-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Transaction
  ( test_lRunTransactionsUnit
  ) where

import Test.HUnit (Assertion, assertFailure)
import Test.Hspec.Expectations (shouldThrow)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Morley.Client (ResolveError(..), TezosClientError(..))
import Morley.Client.Action.Transaction
import Morley.Michelson.Runtime.GState (genesisAddress1, genesisAddress3)
import Morley.Michelson.Untyped.Entrypoints
import Morley.Tezos.Core (tz)
import Test.Addresses
import Test.Util
import TestM

fakeState
  :: FakeState
fakeState = defaultFakeState
  { fsContracts = fromList $ one (contractAddress2, dumbContractState)
  , fsImplicits = fromList $ one (genesisAddress1, dumbImplicitState)
  }

test_lRunTransactionsUnit :: TestTree
test_lRunTransactionsUnit = testGroup "Fake test transaction sending"
  [ testCase "Successful transaction" $ handleSuccess $
    runFakeTest chainOperationHandlers fakeState $
      lTransfer genesisAddress1 contractAddress2
        [tz|10u|] DefEpName () Nothing
  , testCase "Sender doesn't exist" $ handleUnknownContract $
    runFakeTest chainOperationHandlers fakeState $
      lTransfer genesisAddress3 contractAddress2
        [tz|10u|] DefEpName () Nothing
  , testCase "Destination doesn't exist" $ handleUnknownContract $
    runFakeTest chainOperationHandlers fakeState $
      lTransfer genesisAddress1 genesisAddress3
        [tz|10u|] DefEpName () Nothing
  ]
  where
    handleSuccess :: Either SomeException a -> Assertion
    handleSuccess (Right _) = pass
    handleSuccess (Left e) = assertFailure $ displayException e

    handleUnknownContract :: Either SomeException a -> Assertion
    handleUnknownContract (Right _) =
      assertFailure "Transaction sending unexpectedly didn't fail."
    handleUnknownContract (Left e) =
      shouldThrow (throwM e) $ \case
        ResolveError REAddressNotFound{} -> True
        _ -> False
