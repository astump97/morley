-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Origination
  ( test_lRunTransactionsUnit
  ) where

import Test.HUnit (Assertion, assertFailure)
import Test.Hspec.Expectations (shouldThrow)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Lorentz qualified as L
import Morley.Client
import Morley.Michelson.Runtime.GState (genesisAddress1, genesisAddress3)
import Morley.Tezos.Address.Alias (AddressOrAlias(..))
import Morley.Tezos.Core
import Test.Addresses
import Test.Util
import TestM

fakeState
  :: FakeState
fakeState = defaultFakeState
  { fsContracts = fromList $ one $ (contractAddress2, dumbContractState)
  , fsImplicits = fromList $ one $ (genesisAddress1, dumbImplicitState)
  }

dumbLorentzContract :: L.Contract Integer () ()
dumbLorentzContract = L.defaultContract $
  L.drop L.# L.unit L.# L.nil L.# L.pair

test_lRunTransactionsUnit :: TestTree
test_lRunTransactionsUnit = testGroup "Mock test transaction sending"
  [ testCase "Successful origination" $ handleSuccess $
    runFakeTest chainOperationHandlers fakeState $
      lOriginateContract OverwriteDuplicateAlias "dummy" (AddressResolved genesisAddress1) [tz|100500u|]
      dumbLorentzContract () Nothing Nothing
  , testCase "Originator doesn't exist" $ handleUnknownContract $
    runFakeTest chainOperationHandlers fakeState $
      lOriginateContract OverwriteDuplicateAlias "dummy" (AddressResolved genesisAddress3) [tz|100500u|]
      dumbLorentzContract () Nothing Nothing
  ]
  where
    handleSuccess :: Either SomeException a -> Assertion
    handleSuccess (Right _) = pass
    handleSuccess (Left e) = assertFailure $ displayException e

    handleUnknownContract :: Either SomeException a -> Assertion
    handleUnknownContract (Right _) =
      assertFailure "Origination unexpectedly didn't fail."
    handleUnknownContract (Left e) =
      shouldThrow (throwM e) $ \case
        ResolveError REAddressNotFound{} -> True
        _ -> False
