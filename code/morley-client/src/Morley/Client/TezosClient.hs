-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Interface to @tezos-client@ (and its implementation).

module Morley.Client.TezosClient
  ( module Morley.Client.TezosClient.Class
  , module Morley.Client.TezosClient.Types
  , TezosClientError (..)
  , AliasBehavior (..)
  , Resolve(ResolvedAddress, ResolvedAlias)
  , ResolveError(..)
  , resolveAddress
  , resolveAddressMaybe
  , getAlias
  , getAliasMaybe
  ) where

import Morley.Client.TezosClient.Class
import Morley.Client.TezosClient.Impl
import Morley.Client.TezosClient.Types
