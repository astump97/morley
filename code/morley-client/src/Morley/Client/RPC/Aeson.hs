-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Helpers for the @aeson@ package.
--
-- Currently we need this module due to "GHC stage restriction".

module Morley.Client.RPC.Aeson
  ( morleyClientAesonOptions
  ) where

import Data.Aeson.Casing (aesonPrefix, snakeCase)
import Data.Aeson.TH (Options)

-- | We use these 'Options' to produce JSON encoding in the format
-- that RPC expects. We are not using defaults from @morley@ because
-- we need a specific format.
morleyClientAesonOptions :: Options
morleyClientAesonOptions = aesonPrefix snakeCase
