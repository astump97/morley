-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Abstraction layer for @tezos-client@ functionality.
-- We use it to fake @tezos-client@ in tests.

module Morley.Client.TezosClient.Class
  ( HasTezosClient (..)
  , AliasBehavior (..)
  ) where

import Data.ByteArray (ScrubbedBytes)

import Morley.Tezos.Address
import Morley.Tezos.Address.Alias
import Morley.Tezos.Crypto

-- | How to save the originated contract address.
data AliasBehavior
  = DontSaveAlias
  -- ^ Don't save the newly originated contract address.
  | KeepDuplicateAlias
  -- ^ If an alias already exists, keep it, don't save the newly originated
  -- contract address.
  | OverwriteDuplicateAlias
  -- ^ If an alias already exists, replace it with the address of the newly
  -- originated contract.
  | ForbidDuplicateAlias
  -- ^ If an alias already exists, throw an exception without doing the
  -- origination
  deriving stock (Eq, Ord, Enum)

-- | Type class that provides interaction with @tezos-client@ binary
class (Monad m) => HasTezosClient m where
  signBytes :: ImplicitAddressOrAlias -> Maybe ScrubbedBytes -> ByteString -> m Signature
  -- ^ Sign an operation with @tezos-client@.
  genKey :: ImplicitAlias -> m ImplicitAddress
  -- ^ Generate a secret key and store it with given alias.
  -- If a key with this alias already exists, the corresponding address
  -- will be returned and no state will be changed.
  genFreshKey :: ImplicitAlias -> m ImplicitAddress
  -- ^ Generate a secret key and store it with given alias.
  -- Unlike 'genKey' this function overwrites
  -- the existing key when given alias is already stored.
  revealKey :: ImplicitAlias -> Maybe ScrubbedBytes -> m ()
  -- ^ Reveal public key associated with given implicit account.
  rememberContract :: AliasBehavior -> ContractAddress -> ContractAlias -> m ()
  -- ^ Associate the given contract with alias.
  -- The 'Bool' variable indicates whether or not we should replace already
  -- existing contract alias or not.
  getAliasesAndAddresses :: m [(Text, Text)]
  -- ^ Retrieves a list with all known aliases and respective addresses.
  --
  -- Note that an alias can be ambiguous: it can refer to BOTH a contract and an implicit account.
  -- When an alias "abc" is ambiguous, the list will contain two entries:
  --
  -- > ("abc", "KT1...")
  -- > ("key:abc", "tz1...")
  --
  -- TODO [#910]: Cache this and turn it into a 'Morley.Util.Bimap'.
  getKeyPassword :: ImplicitAddress -> m (Maybe ScrubbedBytes)
  -- ^ Get password for secret key associated with given address
  -- in case this key is password-protected. Obtained password is used
  -- in two places:
  --   * 1) In @signBytes@ call.
  --   * 2) in @revealKey@ call.
