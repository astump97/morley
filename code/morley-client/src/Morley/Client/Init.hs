-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Morley client initialization.
module Morley.Client.Init
  ( MorleyClientConfig(..)
  , mkLogAction

    -- * Lens
  , mccEndpointUrlL
  , mccTezosClientPathL
  , mccMbTezosClientDataDirL
  , mccVerbosityL
  , mccSecretKeyL
  ) where

import Colog
  (Msg(..), Severity(..), cmapM, defaultFieldMap, filterBySeverity, fmtRichMessageCustomDefault,
  logTextStderr, msgSeverity, showSeverity, showSourceLoc, showTime, upgradeMessageAction)
import Morley.Util.Lens
import Servant.Client (BaseUrl(..))

import Morley.Client.Logging (ClientLogAction, logFlush)
import Morley.Tezos.Crypto.Ed25519 qualified as Ed25519

-- | Data necessary for morley client initialization.
data MorleyClientConfig = MorleyClientConfig
  { mccEndpointUrl :: Maybe BaseUrl
  -- ^ URL of tezos endpoint on which operations are performed
  , mccTezosClientPath :: FilePath
  -- ^ Path to @tezos-client@ binary through which operations are
  -- performed
  , mccMbTezosClientDataDir :: Maybe FilePath
  -- ^ Path to @tezos-client@ data directory.
  , mccVerbosity :: Word
  -- ^ Verbosity level. @0@ means that only important messages will be
  -- printed. The greater this value is, the more messages will be
  -- printed during execution. After some small unspecified limit
  -- increasing this value does not change anything.
  , mccSecretKey :: Maybe Ed25519.SecretKey
  -- ^ Custom secret key to use for signing.
  } deriving stock Show


makeLensesWith postfixLFields ''MorleyClientConfig

-- | Make appropriate 'ClientLogAction' based on verbosity specified by the user.
mkLogAction :: MonadIO m => Word -> ClientLogAction m
mkLogAction verbosity =
  filterBySeverity severity msgSeverity $
    upgradeMessageAction defaultFieldMap $
      flip fmtRichMessageCustomDefault fmt `cmapM` logTextStderrFlush
  where
    severity = case verbosity of
      0 -> Warning
      1 -> Info
      _ -> Debug
    logTextStderrFlush = logTextStderr <> logFlush stderr
    -- NB: ignoring thread id because it's not informative here
    fmt _ (maybe "" showTime -> time) Msg{..} =
        showSeverity msgSeverity
     <> time
     <> (if verbosity > 2 then showSourceLoc msgStack else mempty)
     <> msgText
