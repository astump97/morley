-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA
-- | Functions to submit transactions via @tezos-client@ and node RPC.

module Morley.Client.Action.Transaction
  ( runTransactions
  , lRunTransactions

  -- * Transfer
  , transfer
  , lTransfer

  -- Datatypes for batch transactions
  , TD (..)
  , LTransactionData (..)
  , TransactionData (..)
  ) where

import Lorentz.Constraints
import Morley.Client.Action.Common
import Morley.Client.Action.Operation
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types
import Morley.Client.TezosClient.Class
import Morley.Client.Types
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Scope
import Morley.Michelson.Untyped.Entrypoints
import Morley.Tezos.Address
import Morley.Tezos.Address.Alias (AddressOrAlias(..))
import Morley.Tezos.Core (Mutez)

-- | Perform sequence of transactions to the contract, each transactions should be
-- defined via @EntrypointParam t@. Returns operation hash and block in which
-- this operation appears or @Nothing@ in case empty list was provided.
runTransactions
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => ImplicitAddress -> [TransactionData]
  -> m (Maybe (OperationHash, [OperationInfo Result]))
runTransactions sender transactions = do
  (h, i) <- runOperations (AddressResolved sender) (OpTransfer <$> transactions)
  pure $ (, i) <$> h

-- | Lorentz version of 'TransactionData'.
data LTransactionData where
  LTransactionData ::
    forall (t :: Type). NiceParameter t =>
    TD t -> LTransactionData

-- | Lorentz version of 'runTransactions'
lRunTransactions
  :: forall m env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    )
  => ImplicitAddress
  -> [LTransactionData]
  -> m (Maybe (OperationHash, [OperationInfo Result]))
lRunTransactions sender transactions =
  runTransactions sender $ map convertLTransactionData transactions
  where
    convertLTransactionData :: LTransactionData -> TransactionData
    convertLTransactionData (LTransactionData (TD {..} :: TD t))=
      TransactionData TD
      { tdReceiver = tdReceiver
      , tdEpName = tdEpName
      , tdAmount = tdAmount
      , tdParam = T.toVal tdParam
      , tdMbFee = tdMbFee
      }

transfer
  :: forall m t env kind.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    , ParameterScope t
    , L1AddressKind kind
    )
  => ImplicitAddress
  -> KindedAddress kind
  -> Mutez
  -> EpName
  -> T.Value t
  -> Maybe Mutez
  -> m (OperationHash, [IntOpEvent])
transfer from to amount epName param mbFee = do
  res <- runTransactions from $
    [TransactionData TD
      { tdReceiver = Constrained to
      , tdAmount = amount
      , tdEpName = epName
      , tdParam = param
      , tdMbFee = mbFee
      }
    ]
  case res of
    Just res' -> pure $ second (concat . filterTransfers) res'
    _ -> throwM RpcNoOperationsRun
  where
    filterTransfers = mapMaybe \case
      OpTransfer i -> Just i
      _ -> Nothing

lTransfer
  :: forall m t env kind.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    , NiceParameter t
    , L1AddressKind kind
    )
  => ImplicitAddress
  -> KindedAddress kind
  -> Mutez
  -> EpName
  -> t
  -> Maybe Mutez
  -> m (OperationHash, [IntOpEvent])
lTransfer from to amount epName param mbFee =
  transfer from to amount epName (T.toVal param) mbFee
