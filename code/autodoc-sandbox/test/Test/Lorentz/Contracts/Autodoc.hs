-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.Contracts.Autodoc
  ( unit_DocumentationIsTotal
  , test_documentation
  ) where

import Test.HUnit (Assertion)
import Test.Tasty (TestTree)

import Morley.Michelson.Doc
import Test.Cleveland.Doc

import Lorentz.Contracts.Autodoc

unit_DocumentationIsTotal :: Assertion
unit_DocumentationIsTotal =
  evaluateNF_ $ buildMarkdownDoc $ finalizedAsIs autodocSandboxContract

test_documentation :: [TestTree]
test_documentation =
  runDocTests testLorentzDoc autodocSandboxContract
