-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Definitions for calling @txr1@ addresses
module Lorentz.Txr1Call
  ( Txr1CallParam
  ) where

import Lorentz.Annotation
import Lorentz.Constraints.Scopes
import Lorentz.Entrypoints
import Lorentz.Value
import Morley.Tezos.Address

-- | Call parameter for @txr1@ addresses. Default entrypoint doesn't exist, but
-- here we simply represent it as having @never@ parameter, thus uncallable.
data Txr1CallParam a = Deposit (Ticket a, TxRollupL2Address) | Default Never
  deriving stock Generic
  deriving anyclass (HasAnnotation)

instance NiceComparable a => IsoValue (Txr1CallParam a)

instance (HasAnnotation a, NiceComparable a) => ParameterHasEntrypoints (Txr1CallParam a) where
  type ParameterEntrypointsDerivation (Txr1CallParam a) = EpdPlain

-- NB: This is an orphan, because otherwise we have nasty circular dependencies between modules
instance (cp ~ Txr1CallParam a, vd ~ ()) => ToTAddress cp vd TxRollupAddress where
  toTAddress = TAddress . MkAddress
