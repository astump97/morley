-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE FunctionalDependencies #-}

-- | Referenced-by-type versions of some instructions.
--
-- They allow to "dip" into stack or copy elements of stack referring them
-- by type. Their use is justified, because in most cases there is only
-- one element of each type of stack, and in cases when this does not hold
-- (e.g. entrypoint with multiple parameters of the same type), it might be
-- a good idea to wrap those types into a newtype or to use primitives from
-- @named@ package.
--
-- This module is experimental, i.e. everything here should work but may be
-- removed in favor of better development practices.
--
-- Each instruction is followed with usage example.
module Lorentz.Referenced
  ( dupT
  , dipT
  , dropT
  ) where

import Prelude hiding (drop, swap)

import Data.Eq.Singletons (DefaultEq, DefaultEqSym1)
import Data.List.Singletons qualified as LS
import Data.Type.Bool (If)
import Data.Vinyl.TypeLevel qualified as Peano
import GHC.TypeLits (ErrorMessage(..), TypeError)

import Lorentz.Base
import Lorentz.Constraints
import Lorentz.Instr
import Lorentz.Value
import Morley.Util.Peano
import Morley.Util.Type (IsElem)

-- Errors
----------------------------------------------------------------------------

type family StackElemNotFound st a :: ErrorMessage where
  StackElemNotFound st a =
    'Text "Element of type `" ':<>: 'ShowType a ':<>:
    'Text "` is not present on stack" ':$$: 'ShowType st

type family StackElemAmbiguous st a :: ErrorMessage where
  StackElemAmbiguous st a =
    'Text "Ambigous reference to element of type `" ':<>: 'ShowType a ':<>:
    'Text "` for stack" ':$$: 'ShowType st

-- Dup
----------------------------------------------------------------------------

-- | Allows duplicating stack elements referring them by type.
class st ~ (Head st ': Tail st) => DupT (a :: Type) (st :: [Type]) where
  -- | Duplicate an element of stack referring it by type.
  --
  -- If stack contains multiple entries of this type, compile error is raised.
  dupT :: st :-> a : st

instance ( EnsureElem a st
         , TheOnlyC (StackElemNotFound st a)
                    (StackElemAmbiguous st a)
                    (LS.FindIndices (DefaultEqSym1 a) st)
                    indexGHC
         , succ_index ~ 'Peano.S (ToPeano indexGHC)
         , ConstraintDUPNLorentz succ_index st (a ': st) a
         , Dupable a
         ) =>
  DupT a st where
  dupT = dupNPeano @succ_index

_dupSample1 :: [Integer, MText, ()] :-> [MText, Integer, MText, ()]
_dupSample1 = dupT @MText

_dupSample2 :: [Integer, MText, ()] :-> [MText, Integer, MText, ()]
_dupSample2 = dupT

{-
-- Erroneous samples that should give useful error messages.

_dupSampleErr1 :: '[] :-> a
_dupSampleErr1 = dupT @Bool

-- Should fully infer both wildcards
_dupSampleErr2 :: {- [Integer, _, ()] -} _ :-> [Bool, Integer, _, ()]
_dupSampleErr2 = dupT

-- Should fully infer both wildcards
_dupSampleErr3 :: [Integer, _, ()] :-> (Bool ': _)
_dupSampleErr3 = dupT
-}

-- Dip
----------------------------------------------------------------------------

-- | Allows diving into stack referring expected new tip by type.
class dipInp ~ (a ': Tail dipInp) =>
      DipT (a :: Type) (inp :: [Type]) (dipInp :: [Type]) (dipOut :: [Type])
           (out :: [Type])
           | inp a -> dipInp, dipOut inp a -> out, inp out a -> dipOut
           where
  -- | Dip down until an element of the given type is on top of the stack.
  --
  -- If the stack does not contain an element of this type, or contains more
  -- than one, then a compile-time error is raised.
  dipT :: (dipInp :-> dipOut) -> (inp :-> out)


{-
-- We'd like to be able to write something like this. Unfortunately, due to GHC
-- issue #22126, this causes numerous copies of the type error to be sprayed to
-- the screen.

instance ( dipInp ~ (a ': tlDI)
         , EnsureElem a inp
         , RequireNonEmpty
             ('Text "dipT requires a Lorentz instruction that takes input on the stack.")
             dipInp
         , index ~ ToPeano (TheOnly (StackElemNotFound inp a)
                                    (StackElemAmbiguous inp a)
                                    (LS.FindIndices (DefaultEqSym1 a) inp))
         , ConstraintDIPNLorentz index inp out dipInp dipOut
         ) =>
  DipT a inp dipInp dipOut out where
  dipT = dipNPeano @index

type family TheOnly (empty_err :: ErrorMessage) (many_err :: ErrorMessage) (xs :: [k]) :: k where
  TheOnly e_err _ '[] = TypeError e_err
  TheOnly _ _ '[x] = x
  TheOnly _ m_err _ = TypeError m_err
-}

instance ( dipInp ~ (a ': tail_dipInp)
         , EnsureElem a inp
         , RequireNonEmpty
             ('Text "dipT requires a Lorentz instruction that takes input on the stack.")
             dipInp
         , TheOnlyC (StackElemNotFound inp a)
                   (StackElemAmbiguous inp a)
                   (LS.FindIndices (DefaultEqSym1 a) inp)
                   indexGHC
         , index ~ ToPeano indexGHC
         , ConstraintDIPNLorentz index inp out dipInp dipOut
         ) =>
  DipT a inp dipInp dipOut out where
  dipT = dipNPeano @index

-- | @EnsureElem x xs@ constrains @x@ to be an element of @xs@. Unlike
-- @'IsElem' x xs ~ 'True@, @EnsureElem x xs@ can help infer @xs@. For
-- example, given @EnsureElem Int '[Char, b, Bool]@, where @b@ is otherwise
-- unknown, GHC will infer that @b ~ Int@. @EnsureElem@ can also increase
-- information about the length of @xs@. For example, given
-- @EnsureElem Int (Char ': more)@, GHC will infer that @more@ has at least
-- one element.
type EnsureElem :: forall k. k -> [k] -> Constraint
type EnsureElem x xs = (xs ~ (Head xs ': Tail xs), EnsureElem' x xs)
type family EnsureElem' x xs where
  EnsureElem' x (y ': ys) =
    ( If (x `DefaultEq` y) (() :: Constraint) (EnsureElem x ys)
    , If (IsElem x ys) (() :: Constraint) (x ~ y))

-- | @TheOnlyC empty_err many_err xs x@ constrains @x@ to be the only
-- element of @xs@. It produces the type error @empty_err@ if @xs@ is empty,
-- and the type error @many_err@ if @xs@ has more than one element.
class TheOnlyC (empty_err :: ErrorMessage) (many_err :: ErrorMessage) (xs :: [k]) (x :: k)
  | xs -> x
instance (TypeError e_err, y ~ Determined) => TheOnlyC e_err m_err '[] y
instance x ~ y => TheOnlyC e_err m_err '[x] y
instance (TypeError m_err, y ~ Determined) => TheOnlyC e_err m_err (x1 ': x2 ': xs) y

type family RequireNonEmpty (e :: ErrorMessage) (xs :: [k]) :: Constraint where
  RequireNonEmpty e '[] = TypeError e
  RequireNonEmpty _ _ = ()

-- We just use this to satisfy fundeps in error cases.
type family Determined where {}

_dipSample1
  :: [Natural, ()]
      :-> '[ByteString]
  -> [Integer, Text, Natural, ()]
      :-> [Integer, Text, ByteString]
_dipSample1 = dipT @Natural

_dipSample2
  :: [Natural, ()]
      :-> '[ByteString]
  -> [Integer, Text, Natural, ()]
      :-> [Integer, Text, ByteString]
_dipSample2 = dipT -- No type application needed

-- An implementation of dropT that demands a bit more from inference.
_dipSample3
  :: forall a inp dinp dout out.
     ( DipT a inp dinp dout out
     , dinp ~ (a ': dout)
     )
  => inp :-> out
_dipSample3 = dipT (drop @a)

{-
-- Erroneous samples that should give useful error messages.

_dipSampleErr1
  :: [Natural, ()]
      :-> '[ByteString]
  -> [Integer, Text, ()]
      :-> [Integer, Text, ByteString]
_dipSampleErr1 = dipT @Natural

_dipSampleErr2
  :: [Natural, ()]
      :-> '[ByteString]
  -> [Integer, Text, Natural, (), Natural]
      :-> [Integer, Text, ByteString]
_dipSampleErr2 = dipT @Natural

_dipSampleErr3
  :: '[]
      :-> '[ByteString]
  -> [Integer, Text, Natural, ()]
      :-> [Integer, Text, ByteString]
_dipSampleErr3 = dipT @Natural
-}

-- Drop
----------------------------------------------------------------------------

-- | Remove element with the given type from the stack.
dropT
  :: forall a inp dinp dout out.
     ( DipT a inp dinp dout out
     , dinp ~ (a ': dout)
     )
  => inp :-> out
dropT = dipT @a drop

_dropSample1 :: [Integer, (), Natural] :-> [Integer, Natural]
_dropSample1 = dropT @()

-- Framing
----------------------------------------------------------------------------

{- Note that there instructions are only usable for concrete stacks.

When you know your stack only partially, and you try to refer to element of
type "X", then with the current approach compiler will require the unknown
part of stack to contain no elements of type "X", and this is annoying
at least because it ruins modularity.

This issue can be resolved with using 'framed' instruction wrapper and family.

-}
