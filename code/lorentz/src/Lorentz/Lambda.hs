-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Lorentz.Lambda
  ( WrappedLambda(..)
  , Lambda
  , mkLambda
  ) where

import Lorentz.Annotation
import Lorentz.Base
import Morley.AsRPC (HasRPCRepr(..))
import Morley.Michelson.Typed
import Morley.Michelson.Typed.Contract (giveNotInView)

-- | A helper type to construct Lorentz lambda values; Use this for lambda
-- values outside of Lorentz contracts or with @push@.
--
-- The primary reason this is a newtype and not a type synonym is to avoid
-- accidentally splicing the output of 'mkLambda' in-line.
newtype WrappedLambda i o = WrappedLambda {unWrappedLambda :: i :-> o}
  deriving stock (Show, Eq, Generic)
  deriving newtype MapLorentzInstr

deriving newtype instance IsoValue (a :-> b) => IsoValue (WrappedLambda a b)
deriving newtype instance HasAnnotation (i :-> o) => HasAnnotation (WrappedLambda i o)
deriving newtype instance HasRPCRepr (i :-> o) => HasRPCRepr (WrappedLambda i o)

-- | A constructor providing the required constraint for 'WrappedLambda'. This is
-- the only way to construct a lambda that uses operations forbidden in views.
mkLambda :: (IsNotInView => i :-> o) -> WrappedLambda i o
mkLambda i = WrappedLambda $ giveNotInView i

-- | A type synonym representing Michelson lambdas.
type Lambda i o = WrappedLambda '[i] '[o]
