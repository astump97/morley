-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Definition of the Label type and utilities
module Morley.Util.Label
  ( -- * Definitions
    Label (..)

  -- * Utilities
  , labelToText

  -- * Re-exports
  , IsLabel (..)
  ) where

import Fmt (Buildable(..), pretty)
import Text.Show (Show(..), showParen, showString, shows)

import Morley.Util.TypeLits

--------------------------------------------------------------------------------
-- Definitions
--------------------------------------------------------------------------------

-- | Proxy for a label type that includes the 'KnownSymbol' constraint
data Label (name :: Symbol) where
  Label :: KnownSymbol name => Label name

deriving stock instance Eq (Label name)

instance Show (Label name) where
  showsPrec d Label = showParen (d > app_prec) $ showString "Label @" . shows (symbolVal (Proxy @name))
    where
      app_prec = 10

instance (KnownSymbol name, s ~ name) => IsLabel s (Label name) where
  fromLabel = Label

instance Buildable (Label name) where
  build Label = build $ symbolVal (Proxy @name)

--------------------------------------------------------------------------------
-- Utilities
--------------------------------------------------------------------------------

-- | Utility function to get the t'Text' representation of a 'Label'
labelToText :: Label name -> Text
labelToText = pretty
