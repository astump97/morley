-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Util.TypeTuple.Class
  ( RecFromTuple (..)
  ) where

-- | Building a record from tuple.
--
-- It differs from similar typeclass in "Data.Vinyl.FromTuple" module in that
-- it allows type inference outside-in - knowing desired 'Type' you know which
-- tuple should be provided - this improves error messages when constructing
-- concrete 'Type' objects.
class RecFromTuple r where
  type IsoRecTuple r :: Type
  recFromTuple :: IsoRecTuple r -> r
  tupleFromRec :: r -> IsoRecTuple r
