-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{- | Module, carrying logic of @UNPACK@ instruction.

This is nearly symmetric to adjacent Pack.hs module.

When implementing this the following sources were used:

* https://pastebin.com/8gfXaRvp

* https://gitlab.com/tezos/tezos/-/blob/767de2b6665ec2cc21e41e6348f8a0b369d26450/src/proto_alpha/lib_protocol/script_ir_translator.ml#L2501

* https://github.com/tezbridge/tezbridge-crypto/blob/f7d93d8d04201557972e839967758cff5bbe5345/PsddFKi3/codec.js#L513

-}
module Morley.Michelson.Interpret.Unpack
  ( UnpackError (..)
  , unpackInstr'
  , unpackUValue'
  , unpackValue'
  ) where

import Prelude hiding (EQ, Ordering(..), get)

import Data.ByteString qualified as BS
import Data.Constraint (Dict(..))
import Fmt (pretty)

import Morley.Micheline.Binary (eitherDecodeExpression)
import Morley.Micheline.Class (FromExpression, fromExpression)
import Morley.Michelson.Typed (UnpackedValScope)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped
import Morley.Michelson.Untyped qualified as U
import Morley.Util.Binary

{- Implementation notes:

* We need to know which exact type we unpack to.
For instance, serialized signatures are indistinguishable from
plain serialized bytes, so if we want to return "Value" (typed or untyped),
we need to know currently expected type. The reference implementation does
the same.

* It occurred to be easier to decode to typed values and untyped instructions.
When decoding lambda, we type check given instruction, and when decoding
@PUSH@ call we untype decoded value.
One may say that this gives unreasonable performance overhead, but with the
current definition of "Value" types (typed and untyped) we cannot avoid it
anyway, because when deserializing bytearray-like data (keys, signatures, ...),
we have to convert raw bytes to human-readable 'Text' and later parse them
to bytes back at type check stage.
We console ourselves that lambdas are rarely packed.

-}

-- | Deserialize bytes into the given value.
-- Suitable for @UNPACK@ operation only.
unpackValue'
  :: forall t. (UnpackedValScope t)
  => ByteString -> Either UnpackError (T.Value t)
unpackValue' = unpackImpl @(T.Value t)
  where
    _reallyNeedThisConstraint = Dict @(UnpackedValScope t)

-- | Deserialize an instruction into the given value.
unpackInstr' :: ByteString -> Either UnpackError [ExpandedOp]
unpackInstr' = unpackImpl @([ExpandedOp])

-- | Deserialize bytes into 'U.Value'.
unpackUValue' :: ByteString -> Either UnpackError U.Value
unpackUValue' = unpackImpl @U.Value

unpackImpl :: forall t. (FromExpression t)
           => ByteString
           -> Either UnpackError t
unpackImpl bs = do
  (tag, bs') <- maybeToRight (UnpackError "Empty bytes") (BS.uncons bs)
  when (tag /= 0x05) . Left . UnpackError $
    "Unexpected tag: '" <> (show tag) <> "'. '0x05' tag expected."
  expr <- eitherDecodeExpression bs'
  first (UnpackError . pretty) $ fromExpression @t expr
