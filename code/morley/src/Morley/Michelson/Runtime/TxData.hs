-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | 'TxData' type and associated functionality.

module Morley.Michelson.Runtime.TxData
       ( TxData (..)
       , TxParam(..)
       , tdSenderAddressL
       , tdParameterL
       , tdEntrypointL
       , tdAmountL
       ) where

import Control.Lens (makeLensesWith)

import Morley.Michelson.Typed (ParameterScope)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped (EpName)
import Morley.Michelson.Untyped qualified as U
import Morley.Tezos.Address
import Morley.Tezos.Core (Mutez)
import Morley.Util.Lens (postfixLFields)

-- | A parameter associated with a particular transaction.
data TxParam where
  TxTypedParam :: forall t. ParameterScope t => T.Value t -> TxParam
  TxUntypedParam :: U.Value -> TxParam

deriving stock instance Show TxParam

-- | Data associated with a particular transaction.
data TxData = TxData
  { tdSenderAddress :: L1Address
  , tdParameter :: TxParam
  , tdEntrypoint :: EpName
  , tdAmount :: Mutez
  } deriving stock Show

makeLensesWith postfixLFields ''TxData
