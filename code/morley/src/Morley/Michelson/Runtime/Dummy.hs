-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Dummy data to be used in tests or demos where it's not essential.

module Morley.Michelson.Runtime.Dummy
  ( dummyNow
  , dummyLevel
  , dummyMaxSteps
  , dummyVotingPowers
  , dummyBigMapCounter
  , dummyContractEnv
  , dummyGlobalCounter
  , dummyOrigination
  , dummyMinBlockTime
  , dummySelf
  , dummyContractState
  ) where

import Data.Default (def)

import Morley.Michelson.Interpret (ContractEnv(..), RemainingSteps)
import Morley.Michelson.Runtime.GState
  (BigMapCounter, ContractState(..), dummyVotingPowers, genesisAddress)
import Morley.Michelson.Typed (ParameterScope, StorageScope)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Typed.Operation (OriginationOperation(..))
import Morley.Tezos.Address
import Morley.Tezos.Core (Timestamp(..), dummyChainId, tz)

-- | Dummy timestamp, can be used to specify current @NOW@ value or
-- maybe something else.
dummyNow :: Timestamp
dummyNow = Timestamp 100

dummyLevel :: Natural
dummyLevel = 4000

dummyMinBlockTime :: Natural
dummyMinBlockTime = 1

-- | Dummy value for maximal number of steps a contract can
-- make. Intentionally quite large, because most likely if you use
-- dummy value you don't want the interpreter to stop due to gas
-- exhaustion. On the other hand, it probably still prevents the
-- interpreter from working for eternity.
dummyMaxSteps :: RemainingSteps
dummyMaxSteps = 100500

dummyBigMapCounter :: BigMapCounter
dummyBigMapCounter = 0

dummyGlobalCounter :: GlobalCounter
dummyGlobalCounter = 0

dummySelf :: ContractAddress
dummySelf = [ta|KT1AEseqMV6fk2vtvQCVyA7ZCaxv7cpxtXdB|]

-- | Dummy 'ContractEnv' with some reasonable hardcoded values. You
-- can override values you are interested in using record update
-- syntax.
dummyContractEnv :: ContractEnv
dummyContractEnv = ContractEnv
  { ceNow = dummyNow
  , ceMaxSteps = dummyMaxSteps
  , ceBalance = [tz|100u|]
  , ceContracts = mempty
  , ceSelf = dummySelf
  , ceSource = Constrained genesisAddress
  , ceSender = Constrained genesisAddress
  , ceAmount = [tz|100u|]
  , ceVotingPowers = dummyVotingPowers
  , ceChainId = dummyChainId
  , ceOperationHash = Nothing
  , ceLevel = dummyLevel
  , ceErrorSrcPos = def
  , ceMinBlockTime = dummyMinBlockTime
  }

-- | 'OriginationOperation' with most data hardcoded to some
-- reasonable values. Contract and initial values must be passed
-- explicitly, because otherwise it hardly makes sense.
dummyOrigination
  :: (ParameterScope cp, StorageScope st)
  => T.Value st
  -> T.Contract cp st
  -> GlobalCounter
  -> OriginationOperation
dummyOrigination storage contract counter = OriginationOperation
  { ooOriginator = genesisAddress
  , ooDelegate = Nothing
  , ooBalance = [tz|100u|]
  , ooStorage = storage
  , ooContract = contract
  , ooCounter = counter
  , ooAlias = Nothing
  }

-- | Construct dummy 'ContractState' for the given parameter type. This can be
-- useful with @runCode@. The state constructed doesn't have any views, and uses
-- @unit@ for storage.
dummyContractState :: T.ParameterScope t => T.ParamNotes t -> ContractState
dummyContractState notes = ContractState
  { csContract = T.Contract
      { cCode = T.mkContractCode $ T.CDR T.:# T.NIL T.:# T.PAIR
      , cParamNotes = notes
      , cStoreNotes = T.starNotes
      , cEntriesOrder = def
      , cViews = def
      }
  , csDelegate = Nothing
  , csBalance = [tz|100u|]
  , csStorage = T.VUnit
  }
