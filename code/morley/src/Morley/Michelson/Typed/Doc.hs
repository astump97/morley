-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Extracting documentation from instructions set.
module Morley.Michelson.Typed.Doc
  ( cutInstrNonDoc
  , docInstr
  ) where

import Control.Lens (at)
import Control.Monad.Writer.Strict (Writer, runWriter, writer)
import Data.Default (def)
import Prelude hiding (Ordering(..))

import Morley.Michelson.Doc
import Morley.Michelson.Typed.Aliases
import Morley.Michelson.Typed.Contract
import Morley.Michelson.Typed.Instr
import Morley.Michelson.Typed.Util

someDefinitionDocItemToContractDoc :: SomeDocDefinitionItem -> State ContractDoc ()
someDefinitionDocItemToContractDoc sdi@(SomeDocDefinitionItem di) =
  modify $ flip mappend
    mempty
    { cdContents = mempty
    , cdDefinitions = docItemToBlock di
    , cdDefinitionsSet = one sdi
    , cdDefinitionIds = one $ case docItemRef di of
        DocItemRef docItemId -> docItemId
    }

someDocItemToContractDoc :: SomeDocItem -> State ContractDoc ()
someDocItemToContractDoc (SomeDocItem di) = do
  () <- case docItemRef di of
    DocItemNoRef ->
      modify (<> mempty{ cdContents = docItemToBlock di })
    DocItemRefInlined{} ->
      modify (<> mempty{ cdContents = docItemToBlock di })
    DocItemRef{} ->
      someDefinitionDocItemToContractDoc (SomeDocDefinitionItem di)
  forM_ @_ @_ @() (docItemDependencies di) $ \(SomeDocDefinitionItem dep) ->
    case docItemRef dep of
      DocItemRef{} -> do
        -- Taking special treatment for possible cyclic dependencies.
        isPresent <- use $ cdDefinitionsSetL . at (SomeDocDefinitionItem dep)
        case isPresent of
          Just () -> pass
          Nothing -> someDocItemToContractDoc (SomeDocItem dep)

-- | Put a document item.
docInstr :: DocItem di => di -> Instr s s
docInstr = Ext . DOC_ITEM . SomeDocItem

instance ContainsDoc (Instr inp out) where
  buildDocUnfinalized = dfsFoldInstr dfsSettings \case
    Ext ext -> case ext of
      DOC_ITEM sdi ->
        execState (someDocItemToContractDoc sdi) mempty
      _ -> mempty
    _ -> mempty
    where
    dfsSettings :: DfsSettings (Writer ContractDoc)
    dfsSettings = def
      { dsCtorEffectsApp = CtorEffectsApp
          { ceaName = "Building DocGroup"
          , ceaPostStep = \_old -> \case
              (runWriter -> (i@(DocGroup grouping _), resChildren)) ->
                writer (i, docGroupContent grouping resChildren)
              other -> other
          }
      }

instance ContainsUpdateableDoc (Instr inp out) where
  modifyDocEntirely mapper = dfsModifyInstr def $ \case
    Ext ext -> Ext $
      case ext of
        DOC_ITEM sdi -> DOC_ITEM (mapper sdi)
        i -> i
    i -> i

instance ContainsDoc (ContractCode inp out) where
  buildDocUnfinalized = buildDocUnfinalized . unContractCode

instance ContainsUpdateableDoc (ContractCode inp out) where
  modifyDocEntirely how (ContractCode x) = ContractCode $ modifyDocEntirely how x

instance ContainsDoc (Contract cp st) where
  buildDocUnfinalized = buildDocUnfinalized . cCode

instance ContainsUpdateableDoc (Contract cp st) where
  modifyDocEntirely how contract =
    contract{ cCode = modifyDocEntirely how (cCode contract) }

-- | Leave only instructions related to documentation.
--
-- Generated documentation for resulting instruction remains the same, but
-- semantics of instruction itself gets lost.
-- We have to pass optimizer here as an argument to avoid cyclic dependencies.
cutInstrNonDoc :: (forall i o. Instr i o -> Instr i o) -> Instr inp out -> Instr s s
cutInstrNonDoc optimize = optimize . dfsFoldInstr dfsSettings step
  where
  dfsSettings :: DfsSettings $ Writer (Instr s s)
  dfsSettings = def
    { dsCtorEffectsApp = CtorEffectsApp
        { ceaName = "Wrap into DocGroup"
        , ceaPostStep = \_old -> \case
            (runWriter -> (i@(DocGroup g _), resChildren)) ->
              writer (i, DocGroup g resChildren)
            other -> other
        }
    }
  step :: Instr inp out -> Instr s s
  step = \case
    Ext ext -> case ext of
      DOC_ITEM di -> Ext $ DOC_ITEM di
      _ -> Nop
    _ -> Nop
