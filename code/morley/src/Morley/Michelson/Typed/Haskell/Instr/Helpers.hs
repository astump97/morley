-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Helpers used for product and sum type representations
module Morley.Michelson.Typed.Haskell.Instr.Helpers
  ( Branch (..)
  , Path
  ) where

-- | Which branch to choose in generic tree representation: left,
-- straight or right. 'S' is used when there is one constructor with
-- one field (something newtype-like).
--
-- The reason why we need 'S' can be explained by this example:
-- data A = A1 B | A2 Integer
-- data B = B Bool
-- Now we may search for A1 constructor or B constructor. Without 'S' in
-- both cases path will be the same ([L]).
data Branch = L | S | R

-- | Path to a leaf (some field or constructor) in generic tree representation.
type Path = [Branch]
