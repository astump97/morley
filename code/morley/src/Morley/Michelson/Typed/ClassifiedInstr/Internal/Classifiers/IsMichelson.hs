-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | Actual decision implementation for 'IsMichelson'.
module Morley.Michelson.Typed.ClassifiedInstr.Internal.Classifiers.IsMichelson
  ( module Morley.Michelson.Typed.ClassifiedInstr.Internal.Classifiers.IsMichelson
  ) where

import Morley.Michelson.Typed.ClassifiedInstr.Internal.InstrEnum
import Morley.Michelson.Typed.ClassifiedInstr.Internal.Types

-- | Decide whether an instruction is coming from Michelson and whether it affects execution.
isMichelson :: InstrEnum -> IsMichelson
isMichelson = \case
  WithLoc -> Phantom
  Meta -> Phantom
  FrameInstr -> Phantom
  DocGroup -> Phantom
  Nop -> Additional
  Ext -> Additional
  Seq -> Structural
  Nested -> Structural
  --------------------
  IF_NONE -> FromMichelson
  IF_LEFT -> FromMichelson
  IF_CONS -> FromMichelson
  AnnMAP -> FromMichelson
  ITER -> FromMichelson
  IF -> FromMichelson
  LOOP -> FromMichelson
  LOOP_LEFT -> FromMichelson
  DIP  -> FromMichelson
  DIPN  -> FromMichelson
  FAILWITH  -> FromMichelson
  NEVER  -> FromMichelson
  DROP -> FromMichelson
  DROPN -> FromMichelson
  SWAP -> FromMichelson
  DIG -> FromMichelson
  DUG -> FromMichelson
  UNPAIRN -> FromMichelson
  AnnCAR -> FromMichelson
  AnnCDR -> FromMichelson
  AnnDUP -> FromMichelson
  AnnDUPN -> FromMichelson
  AnnPUSH -> FromMichelson
  AnnSOME -> FromMichelson
  AnnNONE -> FromMichelson
  AnnUNIT -> FromMichelson
  AnnPAIR -> FromMichelson
  AnnUNPAIR -> FromMichelson
  AnnPAIRN -> FromMichelson
  AnnLEFT -> FromMichelson
  AnnRIGHT -> FromMichelson
  AnnNIL -> FromMichelson
  AnnCONS -> FromMichelson
  AnnSIZE -> FromMichelson
  AnnEMPTY_SET -> FromMichelson
  AnnEMPTY_MAP -> FromMichelson
  AnnEMPTY_BIG_MAP -> FromMichelson
  AnnMEM -> FromMichelson
  AnnGET -> FromMichelson
  AnnGETN -> FromMichelson
  AnnUPDATE -> FromMichelson
  AnnUPDATEN -> FromMichelson
  AnnGET_AND_UPDATE -> FromMichelson
  AnnLAMBDA -> FromMichelson
  AnnEXEC -> FromMichelson
  AnnAPPLY -> FromMichelson
  AnnCAST -> FromMichelson
  AnnRENAME -> FromMichelson
  AnnPACK -> FromMichelson
  AnnUNPACK -> FromMichelson
  AnnCONCAT -> FromMichelson
  AnnCONCAT' -> FromMichelson
  AnnSLICE -> FromMichelson
  AnnISNAT -> FromMichelson
  AnnADD -> FromMichelson
  AnnSUB -> FromMichelson
  AnnSUB_MUTEZ -> FromMichelson
  AnnMUL -> FromMichelson
  AnnEDIV -> FromMichelson
  AnnABS -> FromMichelson
  AnnNEG -> FromMichelson
  AnnLSL -> FromMichelson
  AnnLSR -> FromMichelson
  AnnOR -> FromMichelson
  AnnAND -> FromMichelson
  AnnXOR -> FromMichelson
  AnnNOT -> FromMichelson
  AnnCOMPARE -> FromMichelson
  AnnEQ -> FromMichelson
  AnnNEQ -> FromMichelson
  AnnLT -> FromMichelson
  AnnGT -> FromMichelson
  AnnLE -> FromMichelson
  AnnGE -> FromMichelson
  AnnINT -> FromMichelson
  AnnVIEW -> FromMichelson
  AnnSELF -> FromMichelson
  AnnCONTRACT -> FromMichelson
  AnnTRANSFER_TOKENS -> FromMichelson
  AnnSET_DELEGATE -> FromMichelson
  AnnCREATE_CONTRACT -> FromMichelson
  AnnIMPLICIT_ACCOUNT -> FromMichelson
  AnnNOW -> FromMichelson
  AnnAMOUNT -> FromMichelson
  AnnBALANCE -> FromMichelson
  AnnVOTING_POWER -> FromMichelson
  AnnTOTAL_VOTING_POWER -> FromMichelson
  AnnCHECK_SIGNATURE -> FromMichelson
  AnnSHA256 -> FromMichelson
  AnnSHA512 -> FromMichelson
  AnnBLAKE2B -> FromMichelson
  AnnSHA3 -> FromMichelson
  AnnKECCAK -> FromMichelson
  AnnHASH_KEY -> FromMichelson
  AnnPAIRING_CHECK -> FromMichelson
  AnnSOURCE -> FromMichelson
  AnnSENDER -> FromMichelson
  AnnADDRESS -> FromMichelson
  AnnCHAIN_ID -> FromMichelson
  AnnLEVEL -> FromMichelson
  AnnSELF_ADDRESS -> FromMichelson
  AnnTICKET -> FromMichelson
  AnnREAD_TICKET -> FromMichelson
  AnnSPLIT_TICKET -> FromMichelson
  AnnJOIN_TICKETS -> FromMichelson
  AnnOPEN_CHEST -> FromMichelson
  AnnSAPLING_EMPTY_STATE -> FromMichelson
  AnnSAPLING_VERIFY_UPDATE -> FromMichelson
  AnnMIN_BLOCK_TIME -> FromMichelson
  AnnEMIT -> FromMichelson
