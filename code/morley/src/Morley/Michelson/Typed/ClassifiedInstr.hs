-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Machinery for typed instruction classification.
module Morley.Michelson.Typed.ClassifiedInstr
  ( withClassifiedInstr
  , ClassifiedInstr(..)

    -- * Classifications
  , NumChildren (..)
  , FailureType (..)
  , IsMichelson (..)
  , HasAnns (..)

    -- * Singletons
  , SingNumChildren (..)
  , SingFailureType (..)
  , SingIsMichelson (..)
  , SingHasAnns (..)
  ) where

import Morley.Michelson.Typed.ClassifiedInstr.Internal.MainImpl
import Morley.Michelson.Typed.ClassifiedInstr.Internal.Types
import Morley.Michelson.Typed.ClassifiedInstr.Internal.WithClassifiedInstr
