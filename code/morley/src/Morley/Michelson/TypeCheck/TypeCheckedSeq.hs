-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | This module provides a data type for representing a partially typed
-- sequence of instructions.
--
-- It is needed to represent the fact that there can only be one well-typed node
-- in a sequence and it is the first one. Also, it serves its role to remove
-- @TcError@ usage from @TypeCheckedOp@.
module Morley.Michelson.TypeCheck.TypeCheckedSeq
  ( TypeCheckedInstr
  , TypeCheckedOp(..)
  , IllTypedInstr(..)
  , TypeCheckedSeq(..)
  , tcsEither
  , seqToOps
  , someInstrToOp
  , someViewToOp
  ) where

import Morley.Michelson.TypeCheck.Error (TcError')
import Morley.Michelson.TypeCheck.TypeCheckedOp
  (IllTypedInstr(..), TypeCheckedInstr, TypeCheckedOp(..), someInstrToOp, someViewToOp)
import Morley.Michelson.TypeCheck.Types (SomeTcInstr(..))

-- | Represents a partiall typed sequence of instructions.
data TypeCheckedSeq op inp
  -- | A fully well-typed sequence.
  = WellTypedSeq (SomeTcInstr inp)
  -- | A well-typed prefix followed by some error and semi-typed instructions.
  | MixedSeq (SomeTcInstr inp) (TcError' op) [IllTypedInstr op]
  -- | There is no well-typed prefix, only an error and semi-typed instructions.
  | IllTypedSeq (TcError' op) [IllTypedInstr op]

seqToOps :: TypeCheckedSeq op inp -> [TypeCheckedOp op]
seqToOps = \case
  WellTypedSeq instr -> [someInstrToOp instr]
  MixedSeq instr _ tail' -> someInstrToOp instr : map IllTypedOp tail'
  IllTypedSeq _ tail' -> map IllTypedOp tail'

-- | Case analysis for @TypeCheckedSeq@.
tcsEither
  :: ([TypeCheckedOp op] -> TcError' op -> a)
     -- ^ On error, with all already typechecked operations
  -> (SomeTcInstr inp -> a) -- ^ On well-typed instruction
  -> TypeCheckedSeq op inp -- ^ The sequence to dispatch on
  -> a
tcsEither onErr onInstr v = case v of
  WellTypedSeq instr -> onInstr instr
  MixedSeq _ err _ -> onErr (seqToOps v) err
  IllTypedSeq err _ -> onErr (seqToOps v) err
