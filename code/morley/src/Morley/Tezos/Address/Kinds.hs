-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- NB: 'genSingletonsType' creates some unused synonyms and GHC complains. To
-- minimize the impact of this option, this is in a separate module.
{-# OPTIONS_GHC -Wno-unused-top-binds #-}

-- | Address kinds.
module Morley.Tezos.Address.Kinds
  ( AddressKind(..)
  , SingAddressKind(..)
  ) where

import Fmt (Buildable(..))

import Morley.Util.Sing

-- | Address "kind"
data AddressKind
  = AddressKindImplicit
  -- ^ an implicit address, @tz1@-@tz3@
  | AddressKindContract
  -- ^ a contract address, @KT1@
  | AddressKindTxRollup
  -- ^ a transaction rollup address, @txr1@
  deriving stock (Eq, Bounded, Enum, Ord, Show, Generic)

instance NFData AddressKind

instance Buildable AddressKind where
  build = \case
    AddressKindImplicit -> "implicit"
    AddressKindContract -> "contract"
    AddressKindTxRollup -> "transaction rollup"

genSingletonsType ''AddressKind
