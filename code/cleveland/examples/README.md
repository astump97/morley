# EDSL examples

This directory contains tests that we use in EDSL tutorial.
Please see [testing EDSL documentation](https://gitlab.com/morley-framework/morley/-/blob/master/code/cleveland/testingEDSL.md) for more details.
