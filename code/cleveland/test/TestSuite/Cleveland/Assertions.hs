-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.Assertions
  ( test_failure_fails
  , test_assert_succeeds
  , test_assert_fails
  , test_eq_succeeds
  , test_eq_fails
  , test_neq_succeeds
  , test_neq_fails
  , test_checkCompares_succeeds
  , test_checkCompares_fails
  , test_checkComparesWith_succeeds
  , test_checkComparesWith_fails
  ) where

import Fmt (pretty)
import Test.Tasty (TestTree)

import Test.Cleveland

import Morley.Util.Interpolate (i)
import TestSuite.Util (shouldFailWithMessage)

{-# ANN module ("HLint: ignore Reduce duplication" :: Text) #-}

test_failure_fails :: TestTree
test_failure_fails =
  testScenario "`failure` fails unconditionally" $ scenario do
    failure @() "<error msg>"
      & shouldFailWithMessage "<error msg>"

test_assert_succeeds :: TestTree
test_assert_succeeds =
  testScenario "`assert` succeeds when condition is true" $ scenario $
    assert True "<error msg>"

test_assert_fails :: TestTree
test_assert_fails =
  testScenario "`assert` fails when condition is false" $ scenario do
    assert False "<error msg>"
      & shouldFailWithMessage "<error msg>"

test_eq_succeeds :: TestTree
test_eq_succeeds =
  testScenario "`@==` succeeds when the values are equal" $ scenario $
    True @== True

test_eq_fails :: TestTree
test_eq_fails =
  testScenario "`@==` fails when the values are not equal" $ scenario do
    (True @== False)
      & shouldFailWithMessage [i|
━━ Expected (rhs) ━━
False
━━ Got (lhs) ━━
True|]

test_neq_succeeds :: TestTree
test_neq_succeeds =
  testScenario "`@/=` succeeds when the values are not equal" $ scenario $
    True @/= False

test_neq_fails :: TestTree
test_neq_fails =
  testScenario "`@/=` fails when the values are equal" $ scenario do
    (True @/= True)
      & shouldFailWithMessage "The two values are equal:"

test_checkCompares_succeeds :: TestTree
test_checkCompares_succeeds =
  testScenario "`checkCompares` succeeds when the comparison succeeds" $ scenario $
    checkCompares @Int 2 elem [1, 2, 3]

test_checkCompares_fails :: TestTree
test_checkCompares_fails =
  testScenario "`checkCompares` fails when the comparison fails" $ scenario do
    checkCompares @Int 1 elem []
      & shouldFailWithMessage [i|
━━ lhs ━━
1
━━ rhs ━━
[]|]

test_checkComparesWith_succeeds :: TestTree
test_checkComparesWith_succeeds =
  testScenario "`checkComparesWith` succeeds when the comparison succeeds" $ scenario $
    checkComparesWith @Int pretty 2 elem pretty [1, 2, 3]

test_checkComparesWith_fails :: TestTree
test_checkComparesWith_fails =
  testScenario "`checkComparesWith` fails when the comparison fails and prints values" $ scenario do
    checkComparesWith @Int toCardinal 1 elem (pretty . fmap toOrdinal) [2, 3]
      & shouldFailWithMessage [i|
━━ lhs ━━
One
━━ rhs ━━
[Second,Third]|]
  where
    toCardinal = \case
      1 -> "One"
      2 -> "Two"
      3 -> "Three"
      n -> show n
    toOrdinal = \case
      1 -> "First"
      2 -> "Second"
      3 -> "Third"
      n -> show @String n
