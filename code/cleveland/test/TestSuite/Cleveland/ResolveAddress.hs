-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.ResolveAddress
  ( test_resolves_address_of_contract
  , test_resolves_address_of_implicit_account
  ) where

import Test.Tasty (TestTree)

import Test.Cleveland
import Test.Cleveland.Internal.Abstract (SpecificOrDefaultAlias(SpecificAlias))
import Test.Cleveland.Lorentz.Types (toContractAddress)
import TestSuite.Util (idContract)

test_resolves_address_of_contract :: TestTree
test_resolves_address_of_contract =
  testScenario "Resolves address of contract" $ scenario do
    let alias = "contract-test-1"
    expectedAddress <- toContractAddress <$> originate alias () (idContract @() @())
    resolveAddress alias @@== expectedAddress

test_resolves_address_of_implicit_account :: TestTree
test_resolves_address_of_implicit_account =
  testScenario "Resolves address of implicit account" $ scenario do
    let alias = "contract-test-1"
    expectedAddress <- newAddress $ SpecificAlias alias
    resolveAddress alias @@== expectedAddress
