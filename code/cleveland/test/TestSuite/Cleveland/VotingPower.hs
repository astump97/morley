-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Module testing voting power instructions and their support from
-- cleveland.
module TestSuite.Cleveland.VotingPower
  ( test_VotingPowers
  ) where

import Test.Tasty (TestTree)

import Lorentz.Value
import Morley.Tezos.Crypto
import Test.Cleveland

test_VotingPowers :: [TestTree]
test_VotingPowers =
  [ testScenario "Can pick voting power of some address" $ scenario do
      contract
        <- originate "vp contract" 0
        =<< importContract @KeyHash @Natural @() ("../../contracts/voting_power.tz")

      let keyHash = unsafe $ parseHash "tz1Yq4Hj9u2f9473wAKiFEm36Sp2GfB5aTGa"
      transfer contract $ calling def keyHash
      getStorage contract @@== 0

  , testScenarioOnEmulator "Can set voting powers in test scenario" $ scenarioEmulated do
      contract
        <- originate "vp contract" (0, 0)
        =<< importContract @KeyHash @(Natural, Natural) @() ("../../contracts/voting_powers.tz")

      comment "Setting custom voting powers to access them later"
      let keyHash = unsafe $ parseHash "tz1Yq4Hj9u2f9473wAKiFEm36Sp2GfB5aTGa"
      let keyHash2 = unsafe $ parseHash "tz1hvYBbHRJhT3dQ8bEjc34xm3rjJJmTcuqs"
      setVotingPowers (mkVotingPowers [(keyHash, 123), (keyHash2, 57)])

      transfer contract $ calling def keyHash
      getStorage contract @@== (123, 180)
  ]
