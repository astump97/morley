-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.BadAddressReuse (test_badAddressReuse) where

import Lorentz qualified as L

import Test.Tasty (TestTree)

import Test.Cleveland
import TestSuite.Util

test_badAddressReuse :: TestTree
test_badAddressReuse =
  testScenario "Bad smart contract address reuse (#729 regression test)" $ scenario do
    _ <- originate "abcdefgh" () $ idContract @() @()

    addr <- newAddress "abcdefgh"

    target <- newAddress "target"
    withSender addr $ transfer target [L.tz|0.1|]
