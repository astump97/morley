-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.Entrypoints
  ( test_duplicateEntrypoints
  ) where

import Test.Tasty (TestTree)

import Morley.Michelson.Untyped

import Test.Cleveland.Michelson.Entrypoints
import TestSuite.Cleveland.Lorentz.Entrypoints (expectFailure)

test_duplicateEntrypoints :: TestTree
test_duplicateEntrypoints = testContractMatchesEntrypoints "Ill-typed contract fails the test"
  illTypedContract mempty
  & expectFailure "Duplicated entrypoint names: 'do1'"
  where
    n = unsafe $ mkAnnotation ""
    do1 = unsafe $ mkAnnotation "do1"
    illTypedContract =
      Contract
        { contractParameter = ParameterType (
            Ty (TOr do1 do1 (Ty TInt n)
                 (Ty (TPair n n n n (Ty TInt n) (Ty TInt n)) n)) n) n
        , contractStorage = Ty TUnit n
        , contractCode = []
        , entriesOrder = PSC
        , contractViews = []
        }
