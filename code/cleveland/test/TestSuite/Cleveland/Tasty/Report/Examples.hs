-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Cleveland.Tasty.Report.Examples
  ( reportExamples
  ) where

import System.FilePath ((</>))

import Morley.Tezos.Address
import Morley.Util.Interpolate
import Test.Cleveland







----------------------------------------------------------------------------
-- Examples
----------------------------------------------------------------------------

example1 :: EmulatedT PureM ()
example1 = void $ signBytes "" unknownAddr

example1ExpectedErr :: Text
example1ExpectedErr =
  [itu|
   ┏━━ #{reportExamplesPath} ━━━
24 ┃ example1 :: EmulatedT PureM ()
25 ┃ example1 = void $ signBytes "" unknownAddr
   ┃                   ^^^^^^^^^^^^^^^^^^^^^^^^
   ┃                   | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
26 ┃

CallStack (from HasCallStack):
  |]

----------------------------------------------------------------------------
-- Examples with helper functions
----------------------------------------------------------------------------

exampleWithHelperFunction1 :: EmulatedT PureM ()
exampleWithHelperFunction1 = exampleWithHelperFunction1Helper

exampleWithHelperFunction1Helper :: EmulatedT PureM ()
exampleWithHelperFunction1Helper = void $ signBytes "" unknownAddr

exampleWithHelperFunction1ExpectedErr :: Text
exampleWithHelperFunction1ExpectedErr =
  [itu|
     ┏━━ #{reportExamplesPath} ━━━
  47 ┃ exampleWithHelperFunction1Helper :: EmulatedT PureM ()
  48 ┃ exampleWithHelperFunction1Helper = void $ signBytes "" unknownAddr
     ┃                                           ^^^^^^^^^^^^^^^^^^^^^^^^
     ┃                                           | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  49 ┃

  CallStack (from HasCallStack):
  |]

exampleWithHelperFunction2 :: EmulatedT PureM ()
exampleWithHelperFunction2 = exampleWithHelperFunction2Helper

exampleWithHelperFunction2Helper :: HasCallStack => EmulatedT PureM ()
exampleWithHelperFunction2Helper = withFrozenCallStack $ void $ signBytes "" unknownAddr

exampleWithHelperFunction2ExpectedErr :: Text
exampleWithHelperFunction2ExpectedErr =
  [itu|
     ┏━━ #{reportExamplesPath} ━━━
  63 ┃ exampleWithHelperFunction2 :: EmulatedT PureM ()
  64 ┃ exampleWithHelperFunction2 = exampleWithHelperFunction2Helper
     ┃                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     ┃                              | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  65 ┃

  CallStack (from HasCallStack):
  |]

----------------------------------------------------------------------------
-- Example with no callstack frames
----------------------------------------------------------------------------

exampleNoCallStack1 :: EmulatedT PureM ()
exampleNoCallStack1 = withFrozenCallStack $ void $ signBytes "" unknownAddr

exampleNoCallStack1ExpectedErr :: Text
exampleNoCallStack1ExpectedErr =
  [i|Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z|]



----------------------------------------------------------------------------
-- Example with errors spanning across multiple lines
----------------------------------------------------------------------------

exampleMultipleLines1 :: EmulatedT PureM ()
exampleMultipleLines1 =
  void ( signBytes
           ""
           unknownAddr )

exampleMultipleLines1ExpectedErr :: Text
exampleMultipleLines1ExpectedErr =
  [itu|
      ┏━━ #{reportExamplesPath} ━━━
  100 ┃ exampleMultipleLines1 =
  101 ┃   void ( signBytes
  102 ┃            ""
  103 ┃            unknownAddr )
      ┃          ^^^^^^^^^^^^^
      ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  104 ┃

  CallStack (from HasCallStack):
  |]

exampleMultipleLines2 :: EmulatedT PureM ()
exampleMultipleLines2 =
  void (   signBytes
         ""
           unknownAddr )

exampleMultipleLines2ExpectedErr :: Text
exampleMultipleLines2ExpectedErr =
  [itu|
      ┏━━ #{reportExamplesPath} ━━━
  121 ┃ exampleMultipleLines2 =
  122 ┃   void (   signBytes
  123 ┃          ""
  124 ┃            unknownAddr )
      ┃          ^^^^^^^^^^^^^
      ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  125 ┃

  CallStack (from HasCallStack):
  |]

exampleMultipleLines3 :: EmulatedT PureM ()
exampleMultipleLines3 =
  void (   signBytes
           ""
         unknownAddr )

exampleMultipleLines3ExpectedErr :: Text
exampleMultipleLines3ExpectedErr =
  [itu|
      ┏━━ #{reportExamplesPath} ━━━
  142 ┃ exampleMultipleLines3 =
  143 ┃   void (   signBytes
  144 ┃            ""
  145 ┃          unknownAddr )
      ┃          ^^^^^^^^^^^
      ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  146 ┃

  CallStack (from HasCallStack):
  |]

exampleMultipleLines4 :: EmulatedT PureM ()
exampleMultipleLines4 =
  void (      signBytes
                   ""
         unknownAddr   )

exampleMultipleLines4ExpectedErr :: Text
exampleMultipleLines4ExpectedErr =
  [itu|
      ┏━━ #{reportExamplesPath} ━━━
  163 ┃ exampleMultipleLines4 =
  164 ┃   void (      signBytes
  165 ┃                    ""
  166 ┃          unknownAddr   )
      ┃          ^^^^^^^^^^^^^^
      ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  167 ┃

  CallStack (from HasCallStack):
  |]

exampleMultipleLines5 :: EmulatedT PureM ()
exampleMultipleLines5 =
  void (    signBytes
                     ""
         unknownAddr   )

exampleMultipleLines5ExpectedErr :: Text
exampleMultipleLines5ExpectedErr =
  [itu|
      ┏━━ #{reportExamplesPath} ━━━
  184 ┃ exampleMultipleLines5 =
  185 ┃   void (    signBytes
  186 ┃                      ""
  187 ┃          unknownAddr   )
      ┃          ^^^^^^^^^^^^^^
      ┃          | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  188 ┃

  CallStack (from HasCallStack):
  |]

exampleMultipleLines6 :: EmulatedT PureM ()
exampleMultipleLines6 =
  void (    signBytes
                   ""
           unknownAddr   )

exampleMultipleLines6ExpectedErr :: Text
exampleMultipleLines6ExpectedErr =
  [itu|
      ┏━━ #{reportExamplesPath} ━━━
  205 ┃ exampleMultipleLines6 =
  206 ┃   void (    signBytes
  207 ┃                    ""
  208 ┃            unknownAddr   )
      ┃            ^^^^^^^^^^^
      ┃            | Unknown address provided: tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z
  209 ┃

  CallStack (from HasCallStack):
  |]

unknownAddr :: ImplicitAddress
unknownAddr = [ta|tz1X59tp9P7sk8qdPwqqATVbURbN6o8sMZ7Z|]

reportExamples :: [(String, EmulatedT PureM (), Text)]
reportExamples =
  [ ("example1", example1, example1ExpectedErr)
  , ("exampleWithHelperFunction1", exampleWithHelperFunction1, exampleWithHelperFunction1ExpectedErr)
  , ("exampleWithHelperFunction2", exampleWithHelperFunction2, exampleWithHelperFunction2ExpectedErr)
  , ("exampleNoCallStack1", exampleNoCallStack1, exampleNoCallStack1ExpectedErr)
  , ("exampleMultipleLines1", exampleMultipleLines1, exampleMultipleLines1ExpectedErr)
  , ("exampleMultipleLines2", exampleMultipleLines2, exampleMultipleLines2ExpectedErr)
  , ("exampleMultipleLines3", exampleMultipleLines3, exampleMultipleLines3ExpectedErr)
  , ("exampleMultipleLines4", exampleMultipleLines4, exampleMultipleLines4ExpectedErr)
  , ("exampleMultipleLines5", exampleMultipleLines5, exampleMultipleLines5ExpectedErr)
  , ("exampleMultipleLines6", exampleMultipleLines6, exampleMultipleLines6ExpectedErr)
  ]

reportExamplesPath :: Text
reportExamplesPath = fromString $
  "test" </> "TestSuite" </> "Cleveland" </> "Tasty" </> "Report" </> "Examples.hs"
