<!-- Unreleased: append new entries here -->

* [!1258](https://gitlab.com/morley-framework/morley/-/merge_requests/1258)
  Support implicit contract delegates and setting delegates during origination
  + New function `setDelegate` for setting the delegate on implicit addresses.
  + `originate` now allows specifying optional delegate by passing a `KeyHash` value as argument.
  + `getDelegate` now accepts either implicit or contract addresses.
* [!1226](https://gitlab.com/morley-framework/morley/-/merge_requests/1226)
  Implement delegation operation via RPC
* [!1257](https://gitlab.com/morley-framework/morley/-/merge_requests/1257)
  Add ability to call on-chain views directly from test scenarios
* [!1244](https://gitlab.com/morley-framework/morley/-/merge_requests/1244)
  Carry verbosity setting to only-rpc env in `mkMorleyOnlyRpcEnvNetwork`
* [!1243](https://gitlab.com/morley-framework/morley/-/merge_requests/1243)
  Use Morley's `run_code` emulation
* [!1242](https://gitlab.com/morley-framework/morley/-/merge_requests/1242)
  Use `Constrained` utility existential
* [!1246](https://gitlab.com/morley-framework/morley/-/merge_requests/1246)
  Fix `MonadOriginate` blowing up with `Reduction stack overflow`
  + `MonadOriginate` constraint caused GHC to go loopy; now it doesn't, and a
    test was added to prevent this in the future.
* [!1247](https://gitlab.com/morley-framework/morley/-/merge_requests/1247)
  Refactor MorleyClientEnv
  + Removed `MorleyClientEnv'`, constructor now lives in `MorleyClientEnv`
  + Removed `Morley.Client.Env` module
  + `MorleyClientEnv`, `mkMorleyClientEnv` and `MorleyClientEnv` lenses moved to `Morley.Client.Full`
* [!1220](https://gitlab.com/morley-framework/morley/-/merge_requests/1220)
  Better `transfer` errors on bad address type
* [!1219](https://gitlab.com/morley-framework/morley/-/merge_requests/1219)
  Add resolving contract aliases in network tests
* [!1237](https://gitlab.com/morley-framework/morley/-/merge_requests/1237)
  Support delegate registration on the emulator
* [!1178](https://gitlab.com/morley-framework/morley/-/merge_requests/1178)
  Fix `contract t` typed value generator
  + `contract t` value generator where `t /= unit` could produce implicit
    addresses. This is invalid. Now it only produces smart contract addresses.
  + `contract unit` generator can still produce both implicit and smart contract
    addresses.
* [!1228](https://gitlab.com/morley-framework/morley/-/merge_requests/1228)
  Handle implicit default entrypoint in `testContractCoversEntrypoints` &c
  + Implicit default entrypoint is considered in entrypoint coverage tests iff
    the default entrypoint is defined in the spec. Otherwise, implicit default
    is ignored. Explicit default entrypoint is always considered.
* [!1211](https://gitlab.com/morley-framework/morley/-/merge_requests/1211)
  Unify handling of annotated exceptions

0.2.1
=====
* [!1199](https://gitlab.com/morley-framework/morley/-/merge_requests/1199)
  Replace removed HasTezosClient actions with TezosClient.Impl version.
* [!1212](https://gitlab.com/morley-framework/morley/-/merge_requests/1212)
  Support contract events
  + Untie cleveland operation results from morley-client.
  + Introduce `ContractEvent` type for contract events.
  + Introduce new `WithContractEvents` flag for `transfer`, which tells it to
    return `[ContractEvent]`.
* [!1191](https://gitlab.com/morley-framework/morley/-/merge_requests/1191)
  General cleanup
  + Display contract's aliases in error messages when a transfer fails (emulator-mode only).
* [!1177](https://gitlab.com/morley-framework/morley/-/merge_requests/1177)
  Distinguish implicit/contract aliases and addresses on the type level
* [!1187](https://gitlab.com/morley-framework/morley/-/merge_requests/1187)
  Access MorleyClientEnv in network tests
  + Add `NetworkCaps` carrying `NetworkEnv`.
  + Replace `ScenarioCleveland` with `ScenarioNetwork`.
  + Reimplement `scenario` as a function in `MonadScenario` typeclass.
  + Rename `Scenario` to `Scenario'`, add `Scenario` synonym that bakes in
    `MonadScenario` constraint.
  + Allow `ifEmulation` to access `NetworkCaps` in the on-network branch.
* [!1168](https://gitlab.com/morley-framework/morley/-/merge_requests/1168)
  Support a mix of RPC and non-RPC representations in `runCode`
* [!1137](https://gitlab.com/morley-framework/morley/-/merge_requests/1137)
  Refactor Cleveland's originate\* and `transfer`/`transferMoney`/`call`
  functions
  + All 9 `originate*` functions are replaced with single `originate`,
    optionally accepting initial balance (as `Mutez`) and whether it's a large
    origination (using `Large` argument).
  + `transfer`, `transferMoney` and `call` functions replaced by `transfer` and
    two helpers, `calling` and `unsafeCalling`.

0.2.0
=====
* [!1172](https://gitlab.com/morley-framework/morley/-/merge_requests/1172)
  Add `withChainId` and `withMinBlockTime` functions to set emulator constants.
* [!1123](https://gitlab.com/morley-framework/morley/-/merge_requests/1123)
  Remove deprecated exports
* [!1161](https://gitlab.com/morley-framework/morley/-/merge_requests/1161)
  Remove support for `AliasHint`
* [!1169](https://gitlab.com/morley-framework/morley/-/merge_requests/1169)
  Support MIN_BLOCK_TIME instruction
  + Add `getMinBlockTime` utility to get minimal block delay in seconds as
    `Natural`
* [!1164](https://gitlab.com/morley-framework/morley/-/merge_requests/1164)
  Add `now` and `level` params to the `/run_code`
* [!1162](https://gitlab.com/morley-framework/morley/-/merge_requests/1162)
  Implement instruction to batch-create accounts
* [!1114](https://gitlab.com/morley-framework/morley/-/merge_requests/1114)
  Update to ghc-9.0.2
* [!1108](https://gitlab.com/morley-framework/morley/-/merge_requests/1108)
  Remove support for the deprecated morley extensions
* [!1127](https://gitlab.com/morley-framework/morley/-/merge_requests/1127)
  Create tempdir in Cleveland if datadir is unspecified
  + Instead of mangling user's default tezos-client data directory, when
    a Cleveland test-suite invocation doesn't specify a datadir, use a
    new temporary directory by default.

0.1.2
=====
* [!1050](https://gitlab.com/morley-framework/morley/-/merge_requests/1050)
  Modify resolveAddress to accept AliasHints
* [!1039](https://gitlab.com/morley-framework/morley/-/merge_requests/1039)
  Parameterize generators with a Range
  + `minTimestamp`, `maxTimestamp` and `midTimestamp` are moved to `Hedgehog.Range.Tezos.Core.Timestamp`.
* [!1017](https://gitlab.com/morley-framework/morley/-/merge_requests/1017)
  Resolve some TODOs and link TODOs without issue id to the corresponding gitlab tickets.
* [!1082](https://gitlab.com/morley-framework/morley/-/merge_requests/1082)
  Fix/drop/comment noncanonical Show instances

0.1.1
=====
* [!1113](https://gitlab.com/morley-framework/morley/-/merge_requests/1113)
  Remove uses of `Parser LetEnv` due to redundant constraint.
* [!1094](https://gitlab.com/morley-framework/morley/-/merge_requests/1094)
  Deprecate morley language extensions
  + Functions for importing and testing Morley contracts are added: `Test.Cleveland.Michelson.Import.testTreesWithUntypedContractExt`, `Test.Cleveland.Michelson.Import.testTreesWithTypedContractExt`, `Test.Cleveland.Lorentz.Import.importContractExt`. All these functions are considered deprecated.
* [!965](https://gitlab.com/morley-framework/morley/-/merge_requests/965)
  Use Morley's fixed-size lists
  + Add Hedgehog generators for sized lists.
  + Introduce `enumAliasHints` for generating a sized list of similarly named alias hints.
* [!1072](https://gitlab.com/morley-framework/morley/-/merge_requests/1072)
  Add `runCode` to Cleveland
* [!1070](https://gitlab.com/morley-framework/morley/-/merge_requests/1070)
  Simplify cleveland's internals & public api
  + Relaced the `NiceUnpackedValue (AsRPC st)` constraint in `getStorage`
  + Removed the `NicePackedValue k` constraint from `getAllBigMapValuesMaybe`,
    `getAllBigMapValues`, `getBigMapSizeMaybe`, and `getBigMapSize`.
* [!1025](https://gitlab.com/morley-framework/morley/-/merge_requests/1025)
  Reduce the number of unsafe functions at the call site
  + Remove `unsafeFromHex` converter that is used as counter-part of safe `fromHex` and replaced with `unsafe . fromHex`.
  + Refactor `fromHex` to return `Either` instead of `Maybe`.
  + Add `parseAddressFromHex` helper to get rid of duplicated usages of `parseAddressRaw . unsafe . fromHex`.
* [!978](https://gitlab.com/morley-framework/morley/-/merge_requests/978)
  Make it difficult to misuse 'Show'
  + Use `Buildable` and `pretty` preferrentially.
  + Add `Buildable` instances for `FaluireReason`, `Either a b`.
  + Add `PrettyShow` instances for `TypeSpec` (in lorentz-test) and `Time _` (in cleveland itself)./
  + Avoid using `(+||)` and `(||+)`.
  + Use `Debug.show` when defining `Show` instances.
* [!1058](https://gitlab.com/morley-framework/morley/-/merge_requests/1058)
  Add `Buildable` instances for Unit, tuples up to 8 elements & bytestrings
* [!1057](https://gitlab.com/morley-framework/morley/-/merge_requests/1057)
  Test `Show` and `RenderDoc` instances of `Annotation t`, `AnnotationSet`, `Notes t`, `InstrAbstract op`
* [!997](https://gitlab.com/morley-framework/morley/-/merge_requests/997)
  Check necessity in initial `cast` automatically in Lorentz contracts
* [!1044](https://gitlab.com/morley-framework/morley/-/merge_requests/1044)
  Fix `getRunMode` and `whenEmulated`
  + `getRunMode` is replaced by `ifEmulation` helper.
  + A new helper `whenNetwork` is added.
  + One caveat is that functions passed to `ifEmulation` are universally quantified over the outer monad, so if additional constraints are required beyond `MonadCleveland` or `MonadEmulated`, those constraints have to go on the base monad, e.g. `(MonadCleveland caps m, MonadFail (ClevelandBaseMonad caps)) => m ()`.
ifEmulation.
* [!1043](https://gitlab.com/morley-framework/morley/-/merge_requests/1043)
  Extract actual + expected pattern into a datatype and show diff in type mismatch
* [!1037](https://gitlab.com/morley-framework/morley/-/merge_requests/1037)
  Add synonyms for `SomeConstrainedValue`
  + Reimplement `SomeConstant`, `SomeValue`, `SomeStorage` via `SomeConstrainedValue`.

0.1.0
=====
Initial release extracted from the [morley](https://hackage.haskell.org/package/morley)
and [lorentz](https://hackage.haskell.org/package/lorentz) packages.
Tezos testing framework for contract developed with Michelson, Morley or Lorentz.
