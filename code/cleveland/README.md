# Cleveland: A testing framework for Morley

This package provides an eDSL for testing contracts written in Michelson, Morley or Lorentz.

These tests can be run using the `Test.Cleveland` module:
* on an emulated environment
* on a real network (e.g. testnet).
  In addition to a testnet, one can also use a [local-chain](https://gitlab.com/morley-framework/local-chain)
  with short block periods in order to speed up testing process.

This interface is documented in detail [here](https://gitlab.com/morley-framework/morley/-/blob/master/code/cleveland/testingEDSL.md).

We also provide several Hedgehog generators for most of `morley`'s data types
in the `Hedgehog.Gen.*` modules.
