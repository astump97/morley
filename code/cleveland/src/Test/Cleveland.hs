-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Michelson contracts testing on a real Tezos network.
--
-- See the [documentation](https://gitlab.com/morley-framework/morley/-/blob/master/code/cleveland/testingEDSL.md)
-- for usage instructions.
module Test.Cleveland
  ( Alias
  , ContractAlias
  , ImplicitAlias
  , KindedAddress
  , ContractAddress
  , ImplicitAddress
  , ContractHandle (..)
  , TransferData (..)
  , TypedContract (..)
  , Scenario
  , PureM
  , ClientM

  -- * Validation
  , TransferFailure

  -- * Real network implementation based on @tezos-client@ and RPC.
  , testScenario
  , testScenarioOnNetwork
  , scenarioNetwork
  , MonadScenario(..)
  , getMorleyClientEnv

  -- * Emulation tests
  , testScenarioOnEmulator
  , scenarioEmulated
  , withInitialNow
  , withInitialLevel
  , withMinBlockTime
  , withChainId
  , collectLogs
  , logsForAddress

  -- * Cleveland actions
  , MonadCleveland
  , MonadOps
  , MonadEmulated
  , ClevelandT
  , EmulatedT
  , withSender
  , withMoneybag
  , runIO
  , resolveAddress
  , refillable
  , newAddress
  , newAddresses
  , newFreshAddress
  , enumAliases
  , signBytes
  , signBinary
  , originate
  , Large (..)
  , transfer
  , WithContractEvents (..)
  , ContractEvent (..)
  , calling
  , Default(def)
  , unsafeCalling
  , importUntypedContract
  , importContract
  , noViews
  , comment
  , getBalance
  , getDelegate
  , registerDelegate
  , setDelegate
  , getMorleyLogs
  , getMorleyLogs_
  , getStorage
  , getFullStorage
  , getSomeStorage
  , getAllBigMapValues
  , getAllBigMapValuesMaybe
  , getBigMapSize
  , getBigMapSizeMaybe
  , getBigMapValueMaybe
  , getBigMapValue
  , getPublicKey
  , getChainId
  , advanceTime
  , advanceLevel
  , advanceToLevel
  , getNow
  , getLevel
  , getApproximateBlockInterval
  , getMinBlockTime
  , RunCode(..)
  , runCode
  , branchout
  , offshoot
  , setVotingPowers
  , whenEmulation
  , whenNetwork
  , ifEmulation
  , inBatch

  -- * Address conversions
  , ToAddress(..)
  , ToL1Address(..)
  , ToL1TAddress(..)

  -- * Primitives re-exports
  , EntrypointRef (..)
  , VotingPowers
  , Mutez
  , tz
  , mkVotingPowers
  , mkVotingPowersFromMap

  -- * Assertions
  , failure
  , assert
  , (@==)
  , (@/=)
  , (@@==)
  , (@@/=)
  , checkCompares
  , checkComparesWith
  , evalJust
  , evalRight
  , Showing(..)

  -- * Calling views
  , callView
  , unsafeCallView

    -- * Exception handling
  , attempt
  , catchTransferFailure
  , checkTransferFailure
  , expectTransferFailure
  , expectFailedWith
  , expectError
  , expectCustomError
  , expectCustomError_
  , expectCustomErrorNoArg
  , expectNumericError
  , clarifyErrors

  -- ** Exception predicates
  , TransferFailurePredicate
  , shiftOverflow
  , emptyTransaction
  , badParameter
  , gasExhaustion
  , failedWith
  , addressIs
  -- ** @FAILWITH@ errors
  -- | Convert the many error formats available in @morley@ and @lorentz@ to t'Morley.Michelson.Typed.Existential.SomeConstant'.
  , constant
  , lerror
  , customError
  , customError_
  , customErrorNoArg
  , numericError

  -- * Helpers
  , auto
  , pattern DefEpName
  , ep
  , (?-)
  , (:!)
  , NamedF (..)

  -- * Integration with @hedgehog@
  , testScenarioProps

  -- * Config (reexports)
  , TezosClientEnv(..)
  , MorleyClientEnv(..)
  , NetworkEnv (..)
  , MorleyLogs
  -- ** Config utilities
  , mkMorleyOnlyRpcEnvNetwork

  -- * AsRPC
  , AsRPC.HasRPCRepr(AsRPC)
  , AsRPC.deriveRPC
  , AsRPC.deriveRPCWithStrategy
  , AsRPC.deriveManyRPC
  , AsRPC.deriveManyRPCWithStrategy

  -- * Utilities
  , mapEach
  , forEach
  ) where

import Data.Default (Default(def))

import Lorentz (EntrypointRef(..), Mutez, ToAddress(..), pattern DefEpName, tz)
import Morley.AsRPC qualified as AsRPC
import Morley.Client (MorleyClientEnv(..))
import Morley.Client.TezosClient.Types (TezosClientEnv(..))
import Morley.Michelson.Interpret (MorleyLogs)
import Morley.Michelson.Runtime (VotingPowers, mkVotingPowers, mkVotingPowersFromMap)
import Morley.Util.Named
import Test.Cleveland.Instances ()
import Test.Cleveland.Internal.Abstract
import Test.Cleveland.Internal.Actions
import Test.Cleveland.Internal.Client
import Test.Cleveland.Internal.Hedgehog
import Test.Cleveland.Internal.Pure
import Test.Cleveland.Internal.Scenario
import Test.Cleveland.Lorentz (ToL1Address(..), ToL1TAddress(..))
import Test.Cleveland.Tasty.Internal
import Test.Cleveland.Util
