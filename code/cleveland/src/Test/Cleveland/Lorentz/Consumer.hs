-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Contract which remembers all parameters it has been called with.
--
-- Useful to save return values of @View_@ entry points.
module Test.Cleveland.Lorentz.Consumer
  ( contractConsumer
  ) where

import Lorentz.Base
import Lorentz.Constraints
import Lorentz.Instr
import Lorentz.Run

-- | Remembers parameters it was called with, last goes first.
contractConsumer :: (NiceParameterFull cp, NiceStorageFull cp) => Contract cp [cp] ()
contractConsumer = defaultContract $
  unpair # cons # nil # pair
