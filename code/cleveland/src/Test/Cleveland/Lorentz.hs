-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Cleveland.Lorentz
  ( -- * Importing a contract
    importContract
  , embedContract
  , embedContractM

  -- * Importing a value
  , importValue
  , embedValue
  , embedValueM

  -- * Unit testing
  , testContractCoversEntrypointsT
  , testContractMatchesEntrypointsT
  , testContractCoversEntrypoints
  , testContractMatchesEntrypoints

  -- * Basic types
  , ContractHandle (..)

  , ToContractAddress (..)
  , ToL1Address (..)
  , ToL1TAddress (..)
  , ToStorageType (..)

  -- * Conversions
  , toAddress

  -- * Autodoc testing
  , runDocTests
  , testLorentzDoc
  , excludeDocTests

  -- * General utilities
  , failedTest
  , succeededTest
  , eitherIsLeft
  , eitherIsRight
  , meanTimeUpperBoundProp
  , meanTimeUpperBoundPropNF

  -- * Re-exports
  --
  -- | These functions from @Time@ are re-exported here to make it convenient to call
  -- 'meanTimeUpperBoundProp' and 'meanTimeUpperBoundPropNF'.
  , mcs, ms, sec, minute

  -- * Special contracts for testing
  , contractConsumer
  ) where

import Test.Cleveland.Doc.Lorentz
import Test.Cleveland.Lorentz.Consumer
import Test.Cleveland.Lorentz.Entrypoints
import Test.Cleveland.Lorentz.Import
import Test.Cleveland.Lorentz.Types
import Test.Cleveland.Util
