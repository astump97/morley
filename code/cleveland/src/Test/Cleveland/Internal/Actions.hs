-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | Cleveland actions.
module Test.Cleveland.Internal.Actions
  ( MonadOps
  , MonadTransfer
  , MonadOriginate
  , withSender
  , withMoneybag
  , runIO
  , resolveAddress
  , refillable
  , newAddress
  , newAddresses
  , newFreshAddress
  , enumAliases
  , signBytes
  , signBinary
  , originate
  , Large (..)
  , transfer
  , WithContractEvents (..)
  , calling
  , unsafeCalling
  , inBatch
  , importUntypedContract
  , importContract
  , noViews
  , comment
  , getBalance
  , getStorage
  , getFullStorage
  , getSomeStorage
  , getAllBigMapValues
  , getAllBigMapValuesMaybe
  , getBigMapSize
  , getBigMapSizeMaybe
  , getBigMapValueMaybe
  , getBigMapValue
  , getMorleyLogs
  , getMorleyLogs_
  , getPublicKey
  , getChainId
  , advanceTime
  , advanceLevel
  , advanceToLevel
  , getNow
  , getLevel
  , getApproximateBlockInterval
  , getMinBlockTime
  , runCode
  , branchout
  , offshoot
  , getDelegate
  , registerDelegate
  , setDelegate
  , setVotingPowers
  , whenEmulation
  , whenNetwork
  , ifEmulation
  , getMorleyClientEnv

  -- * Assertions
  , failure
  , assert
  , (@==)
  , (@/=)
  , (@@==)
  , (@@/=)
  , checkCompares
  , checkComparesWith
  , evalJust
  , evalRight

  -- * Calling views
  , callView
  , unsafeCallView

  -- * Exception handling
  , attempt
  , catchTransferFailure
  , checkTransferFailure
  , expectTransferFailure
  , expectFailedWith
  , expectError
  , expectCustomError
  , expectCustomError_
  , expectCustomErrorNoArg
  , expectNumericError
  , clarifyErrors
  -- ** TransferFailure predicates
  , TransferFailurePredicate(..)
  , shiftOverflow
  , emptyTransaction
  , badParameter
  , gasExhaustion
  , failedWith
  , addressIs
  -- ** @FAILWITH@ errors
  , constant
  , lerror
  , customError
  , customError_
  , customErrorNoArg
  , numericError
  ) where

import Test.Cleveland.Internal.Actions.Assertions
import Test.Cleveland.Internal.Actions.ExceptionHandling
import Test.Cleveland.Internal.Actions.Misc
import Test.Cleveland.Internal.Actions.Originate hiding (setDelegate)
import Test.Cleveland.Internal.Actions.Transfer
import Test.Cleveland.Internal.Actions.TransferFailurePredicate
import Test.Cleveland.Internal.Actions.View

-- | Synonym typeclass for monads where network operations can occur.
--
-- This has instances for @MonadCleveland@ and @ClevelandOpsBatch@ contexts.
--
-- Practically, if you want to use 'transfer' or 'originate' in a monad, add a
-- 'MonadOps' constraint on it, f. ex.:
--
-- > callEp1 :: MonadOps m => ContractHandle MyParam () () -> Integer -> m ()
-- > callEp1 ch = transfer ch . calling (ep @"Entrypoint1")
class (MonadTransfer m, MonadOriginate m) => MonadOps m
instance (MonadTransfer m, MonadOriginate m) => MonadOps m

-- Note: 'MonadOps' is a typeclass and not a constraint synonym because GHC (as
-- of 9.0) is being silly and complaining about impredicative polymorphism when
-- defining a constraint synonym with quantified constraints.
