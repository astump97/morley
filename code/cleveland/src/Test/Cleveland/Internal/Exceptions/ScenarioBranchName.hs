-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | This module defines 'ScenarioBranchName' and required instances to use it
-- with 'ExceptionAnnotation'.
module Test.Cleveland.Internal.Exceptions.ScenarioBranchName
  (ScenarioBranchName(..)
  ) where

import Fmt (Buildable(..), nameF, (+|), (|+))

import Test.Cleveland.Internal.Exceptions.Annotated

-- | When using 'Test.Cleveland.branchout' function for building test scenarios - names
-- of branches we are currently within.
newtype ScenarioBranchName = ScenarioBranchName { unTestBranch :: [Text] }
  deriving stock (Show, Eq)
  deriving newtype Semigroup

instance ExceptionAnnotation ScenarioBranchName where
  displayAnnotation ann = nameF ("In branch '" +| ann |+ "'")

instance Buildable ScenarioBranchName where
  build = mconcat . intersperse "/" . map build . unTestBranch
