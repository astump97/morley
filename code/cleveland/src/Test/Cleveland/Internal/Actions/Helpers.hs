-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | Helpers for working in @MonadCleveland@ context.
module Test.Cleveland.Internal.Actions.Helpers
  ( module Test.Cleveland.Internal.Actions.Helpers
  ) where

withCap :: Monad m => (caps -> cap) -> (cap -> m a) -> ReaderT caps m a
withCap getCap useCap = do
  cap <- asks getCap
  lift $ useCap cap
