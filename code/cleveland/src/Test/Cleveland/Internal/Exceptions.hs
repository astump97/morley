-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_HADDOCK not-home #-}

-- | This module defines common exception helpers.
module Test.Cleveland.Internal.Exceptions
  ( ExceptionAnnotation(..)
  , annotateExceptions
  , fromPossiblyAnnotatedException

  -- * Utilities
  , lookupAnnEx
  , insertAnnEx
  , removeAnnEx

  -- * Specific annotations
  , ErrorsClarification(..)
  , ScenarioBranchName(..)
  , CallStackAnnotation(..)
  , addCallStack
  ) where

import Test.Cleveland.Internal.Exceptions.Annotated
import Test.Cleveland.Internal.Exceptions.CallStack
import Test.Cleveland.Internal.Exceptions.ErrorsClarification
import Test.Cleveland.Internal.Exceptions.ScenarioBranchName
