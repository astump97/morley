-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Module declaring newtypes and 'Default' instances for 'Range'.
module Hedgehog.Range.Defaults
  ( Default(..)
  , Length(..)
  , SmallLength(..)
  , TinyLength(..)
  , TicketAmount(..)
  , ValueInt(..)
  , ExpressionInt(..)
  ) where

import Data.Default (Default(..))
import Hedgehog.Range (Range)
import Hedgehog.Range qualified as Range

import Hedgehog.Range.Defaults.Orphans ()

-- | Newtype for specifying lengths of various containers.
newtype Length = Length { unLength :: Int }
  deriving newtype (Eq, Ord, Integral, Enum, Bounded, Real, Num)

instance Default (Range Length) where
  def = Length <$> Range.linear 0 100

-- | Newtype for specifying lengths of various containers.
newtype SmallLength = SmallLength { unSmallLength :: Int }
  deriving newtype (Eq, Ord, Integral, Enum, Bounded, Real, Num)

instance Default (Range SmallLength) where
  def = SmallLength <$> Range.linear 0 10

-- | Newtype for specifying lengths of various containers.
newtype TinyLength = TinyLength { unTinyLength :: Int }
  deriving newtype (Eq, Ord, Integral, Enum, Bounded, Real, Num)

instance Default (Range TinyLength) where
  def = TinyLength <$> Range.linear 0 5

-- | Newtype for the range of @Ticket@ amount field.
newtype TicketAmount = MkTicketAmount { unTicketAmount :: Natural }
  deriving newtype (Eq, Ord, Enum, Integral, Real, Num)

instance Default (Range TicketAmount) where
  def = MkTicketAmount <$> Range.linearFrom 0 0 1000

-- | Newtype for the range of @ValueInt@ constructor of untyped @Value'@.
newtype ValueInt = MkValueInt { unValueInt :: Integer }
  deriving newtype (Eq, Ord, Enum, Real, Num)

instance Default (Range ValueInt) where
  def = MkValueInt <$>
    Range.linearFrom 0
      (fromIntegral $ minBound @Int64)
      (fromIntegral $ maxBound @Word64)

-- | Newtype for the range of @ExpressionInt@ constructor of @Expression@.
newtype ExpressionInt = MkExpressionInt { unExpressionInt :: Integer }
  deriving newtype (Eq, Ord, Enum, Real, Num)

instance Default (Range ExpressionInt) where
  def = MkExpressionInt <$> Range.linearFrom 0 -1000 1000
