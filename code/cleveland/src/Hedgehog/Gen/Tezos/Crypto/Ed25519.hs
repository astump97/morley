-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Tezos.Crypto.Ed25519
  ( genPublicKey
  , genSecretKey
  , genSignature
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Tezos.Crypto.Ed25519 (PublicKey, SecretKey, Signature, detSecretKey, sign, toPublic)

genPublicKey :: MonadGen m => m PublicKey
genPublicKey = toPublic <$> genSecretKey

genSecretKey :: MonadGen m => m SecretKey
genSecretKey = detSecretKey <$> Gen.bytes (Range.singleton 32)

genSignature :: MonadGen m => m Signature
genSignature = sign <$> genSecretKey <*> Gen.utf8 (Range.linear 0 100) Gen.unicode
