-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Address in Tezos.

module Hedgehog.Gen.Tezos.Address
  ( genAddress
  , genContractAddress
  , genKeyAddress
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Hedgehog.Gen.Tezos.Crypto (genKeyHash)
import Morley.Tezos.Address
import Morley.Tezos.Crypto

genAddress :: MonadGen m => m Address
genAddress = Gen.choice [MkAddress <$> genKeyAddress, MkAddress <$> genContractAddress]

genKeyAddress :: MonadGen m => m ImplicitAddress
genKeyAddress = ImplicitAddress <$> genKeyHash

genContractAddress :: MonadGen m => m ContractAddress
genContractAddress = ContractAddress . Hash HashContract <$> Gen.bytes (Range.singleton 20)
