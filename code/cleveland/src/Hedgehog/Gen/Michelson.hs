-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Hedgehog.Gen.Michelson
  ( genErrorSrcPos
  , genSrcPos
  , genPos
  , genMText
  ) where

import Hedgehog (MonadGen)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Michelson.ErrorPos (ErrorSrcPos(..), Pos(..), SrcPos(..))
import Morley.Michelson.Text (MText, maxBoundMChar, minBoundMChar, mkMText)

import Hedgehog.Range.Defaults

genErrorSrcPos :: MonadGen m => m ErrorSrcPos
genErrorSrcPos = ErrorSrcPos <$> genSrcPos

genSrcPos :: MonadGen m => m SrcPos
genSrcPos = SrcPos <$> genPos def <*> genPos def

genPos :: MonadGen m => Range.Range Pos -> m Pos
genPos range = Pos <$> Gen.word (unPos <$> range)

genMText :: MonadGen m => Range.Range Length -> m MText
genMText lenRange =
  unsafe . mkMText <$> Gen.text
    (unLength <$> lenRange)
    (Gen.enum (toEnum @Char minBoundMChar) (toEnum @Char maxBoundMChar))
