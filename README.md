<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Morley: Developer tools for the Michelson Language

[![pipeline status](https://gitlab.com/morley-framework/morley/badges/master/pipeline.svg)](https://gitlab.com/morley-framework/morley/-/commits/master)

Morley consists of a set of tools and libraries to make writing smart contracts
in Michelson pleasant and effective.

This repository is split into multiple Haskell packages for better granularity.
They are in the [code/](/code/) directory, please refer to the corresponding `README.md` files for more information.
We plan to move each package into its own repository at some point.
A brief overview:
* [`morley`](/code/morley/) is where it all started.
It contains:
  + A library with core Tezos and Michelson data types, and functions such as Michelson typechecker and interpreter.
  + An executable that can perform various operations with Michelson code.
* [`lorentz`](/code/lorentz) is a Haskel eDSL that provides a more convenient way to write Michelson contracts.
* [`cleveland`](/code/cleveland) is a testing framework that allows one to write tests for
Michelson/Morley/Lorentz contracts and run them in either an emulated environment or in a real network (usually testnet).
* [`morley-client`](/code/morley-client) provides bindings to Tezos RPC and [`tezos-client`](https://github.com/serokell/tezos-packaging).
* [`morley-prelude`](/code/morley-prelude) is an auxiliary package that we use for internal needs.

Note that this list may be incomplete, e.g. we don't mention internal packages here.

The name Morley is ambiguous, it may refer to:
1. This repository.
2. The `morley` package.

## Michelson version

`master` and `production` branches are maintained to be compatible with version of Michelson running on mainnet.
We use separate branches to support other versions.
More information about our branching strategy can be found [here](/docs/branching.md).

## Issue Tracker

We are using built-in issue tracker on GitLab.

We used to use [YouTrack](https://issues.serokell.io/issues/TM) as our primary issue tracker.
You may see that commit messages up to some date are prefixed with `[TM-X]`.
This prefix refers to an issue in our YouTrack.
YouTrack issues are public, so you can open any of them.

## For Contributors

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.
