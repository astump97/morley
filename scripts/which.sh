#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2021 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

IFS=":"
for p in $PATH; do
  [ -f "$p/$1" ] && [ -x "$p/$1" ] && echo "$p/$1" && exit 0
done
echo "No $1 found in $PATH"
exit 1
