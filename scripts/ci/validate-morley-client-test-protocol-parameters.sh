#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# This script checks that protocol parameters json in morley-client-test matches
# network's on our testnet node.
set -e

bot_name="Protocol Parameter Fetcher"
# Initializing git variables
our_branch="$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"

git config --local user.email "hi@serokell.io"
git config --local user.name "$bot_name"
git remote remove auth-origin 2> /dev/null || :
git remote add auth-origin "https://oath2:$GITLAB_PUSH_TOKEN@gitlab.com/morley-framework/morley.git"
git fetch
git checkout -B "$our_branch" --track "origin/$our_branch"

# If last commit was pushed by cabal generator we can leave, the files are already fixed
if [ "$(git show -s --format='%an' HEAD)" == "$bot_name" ]; then
    echo "Skipping duplicated generation step"
    exit 0
fi

# Updating protocol parameters
scripts/update-morley-client-test-protocol-parameters.sh

echo "Checking protocol parameters file"
# We add everything here to be able to see new files in the `git diff` below
git add --all

# If protocol parameters file was not changed, exit without errors
if [ -z "$(git diff --name-only --staged)" ]; then
    echo "Protocol parameters file is up-to-date"
    exit 0
fi

# If a file has changed push a chore commit
echo "Protocl parameters file is outdated, pushing chore commit"
git commit -m "[Chore] Update protocol parameters file in morley-client-test"
git push auth-origin "$our_branch"

# Because we don't want to run other steps in this pipeline.
# Since the file is updated in the new commit, all steps will run there
exit 1
