#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

selfdir="$(dirname "$0")"
cd "$selfdir/.." || exit 1

RPC_ENDPOINT=$(
  sed -rn '/^\s*TASTY_CLEVELAND_NODE_ENDPOINT:\s*"https:/ { s/^[^"]*"//; s/"[^:"]*$//; p}' \
    .gitlab-ci.yml
    )

curl "$RPC_ENDPOINT/chains/main/blocks/head/context/constants" \
  | awk '
      BEGIN { indent = 0; sep = ""; }
      {
        gsub(":", ": ");
        sub("{", sprintf("{\n%*s", indent+2, ""));
        sub("}", sprintf("\n%*s}", indent-2, ""));
        printf "%s%*s%s", sep, indent, "", $0
        sep=",\n"
      }
      /\{/ { indent+=2 }
      /\}/ { indent-=2 }
      ' \
    RS="," ORS="" \
  > "code/morley-client/test/data/constants.json"
