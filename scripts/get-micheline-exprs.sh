#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# This script uses `tezos-client` to get a list of all the micheline primitives,
# in the correct order.
# We use it to update our micheline serializer/deserializer in `Morley.Micheline.Expression`.

# NOTE: When a new protocol is released and this variable is updated,
# please update `scripts/ci/typecheck-tezos-client.sh` as well.
#
# Protocol hashes can be found in: https://gitlab.com/tezos/tezos/-/tree/
# In the file `src/proto_<version>/lib_protocol/TEZOS_PROTOCOL`
proto="PtKathmankSpLLDALzWw7CGD2j2MtyveTwboEYokqUCP4a1LxMg"

export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=YES

for n in {0..999}; do
  hex=$(printf "%02x" "$n");

  # Run tezos-client to get the primitive that corresponds to the binary code $hex.
  # We use `--mode mockup` to avoid needing a live node.
  # However, using `--mode mockup` might emit warnings like `Base directory /<dir>/.tezos-client is non empty.`,
  # so we pipe stderr to /dev/null.
  primitive=$(tezos-client --mode mockup --protocol $proto convert data "0x03$hex" from binary to michelson 2> /dev/null);

  # If `tezos-client` did not print a primitive to stdout, run the command again, this time printing
  # stderr to the console.
  if [ -z "$primitive" ] ; then
    set -euo pipefail
    tezos-client --mode mockup --protocol $proto convert data "0x03$hex" from binary to michelson
  fi

  echo -n "\"$primitive\", ";

  # print a newline every 8th item
  if ! (( (n + 1) % 8 )) ; then
    echo ""
  fi
done;
